﻿using Body2.Controladora;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista
{
    public partial class Mantenimiento_Piezas : FormGeneral
    {
        private List<Piezas> listaPiezas = new List<Piezas>();
        PiezaController piezas;
        public Mantenimiento_Piezas(string idUsuario)
        {
            InitializeComponent();
            piezas = new PiezaController(idUsuario);
        }

        private void ListarPiezas() {
            listaPiezas = piezas.ListarPiezas();
            dgvPiezas.DataSource = listaPiezas;
        }

        private void Mantenimiento_Piezas_Load(object sender, EventArgs e)
        {
            ListarPiezas();

            Estilo_AgregarBoton(btn_Agregar);
            Estilo_LimpiarBoton(btn_Cancelar);
            Estilo_ModificarBoton(btn_Modificar);
            Estilo_EliminarBoton(btn_Eliminar);
        }

        private void dgvPiezas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LimpiarInputs(grbPiezas, txtId);
            Dictionary<Control, int> textBoxes = new Dictionary<Control, int>();
            textBoxes.Add(txtId,0);
            textBoxes.Add(txtPieza,1);
            LlenarDataGrid(dgvPiezas, sender, e, textBoxes);
            txtId.Enabled = false;
        }

        private void btn_Eliminar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            //verifica que se ha colocado un registro existente
            if (!RegistroExistente())
            {
                MessageBox.Show("No hemos encontrado esta pieza en la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DialogResult dialog = MessageBox.Show("¿Deseas eliminar esta pieza? Pueden existir muchos problemas", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grbPiezas, txtId);
                return;
            }
            string idPieza = txtId.Text.Trim();
            string resultado = piezas.EliminarPieza(idPieza);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Pieza eliminada exitosamente", "Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbPiezas, txtId);
            ListarPiezas();
        }

        private void btn_Agregar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            Piezas prod = new Piezas
            {
                idPieza = txtId.Text.Trim(),
                NombrePieza = txtPieza.Text.Trim()
            };
            string resultado = piezas.AgregarPieza(prod);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Pieza agregada exitosamente", "Agregado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbPiezas, txtId);
            ListarPiezas();
        }
        private bool RegistroExistente()
        {
            if (listaPiezas.Where(x => x.idPieza.Equals(txtId.Text)).FirstOrDefault() == null)
            {
                return false;
            }
            return true;
        }
        private void btn_Modificar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            //verifica que se ha colocado un registro existente
            if (!RegistroExistente())
            {
                MessageBox.Show("No hemos encontrado esta pieza en la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DialogResult dialog = MessageBox.Show("¿Deseas actualizar esta pieza?", "Actualización", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grbPiezas, txtId);
                return;
            }
            Piezas prod = new Piezas
            {
                idPieza = txtId.Text.Trim(),
                NombrePieza = txtPieza.Text.Trim()
            };
            string resultado = piezas.ActualizarPieza(prod);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Pieza actualizada exitosamente", "Actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbPiezas, txtId);
            ListarPiezas();
        }

        private void btn_Cancelar_Click(object sender, EventArgs e)
        {
            LimpiarInputs(grbPiezas, txtId);
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox meTxt = (TextBox)sender;
            string search = meTxt.Text.ToUpper();
            List<Piezas> listaParcialBusqueda = new List<Piezas>();
            List<Piezas> listaParcialBusquedaFinal = new List<Piezas>();
            if (meTxt.Text.Length == 0)
            {
                dgvPiezas.DataSource = listaPiezas;
            }
            if (listaPiezas.Count > 0)
            {
                listaParcialBusquedaFinal = new List<Piezas>();
                dgvPiezas.DataSource = listaParcialBusquedaFinal;
                listaParcialBusqueda = listaPiezas.Where(x => x.NombrePieza.ToUpper().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaPiezas.Where(x => x.idPieza.ToUpper().ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                
                listaParcialBusquedaFinal = listaParcialBusquedaFinal.Distinct().ToList();
                dgvPiezas.DataSource = listaParcialBusquedaFinal;
            }
        }
        private bool Validaciones()
        {
            List<Control> controls = new List<Control>();
            controls.Add(txtPieza);
            controls.Add(txtId);
            if (!ValidacionInputs(controls))
            {
                MessageBox.Show("Por favor llena toda la información requerida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        private void dgvPiezas_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            LimpiarInputs(grbPiezas, txtId);
            Dictionary<Control, int> textBoxes = new Dictionary<Control, int>();
            textBoxes.Add(txtId, 0);
            textBoxes.Add(txtPieza, 1);
            LlenarDataGrid(dgvPiezas, sender, e, textBoxes);
            txtId.Enabled = false;
        }

    }
}
