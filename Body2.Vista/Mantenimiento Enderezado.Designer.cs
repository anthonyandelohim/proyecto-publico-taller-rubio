﻿namespace Body2.Vista
{
    partial class frmMantenimientoEnderezado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMantenimientoEnderezado));
            this.timHoraFecha = new System.Windows.Forms.Timer(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dgvmep = new System.Windows.Forms.DataGridView();
            this.dgvProductos = new System.Windows.Forms.DataGridView();
            this.grbEnderezado = new System.Windows.Forms.GroupBox();
            this.cmbProductos = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtMontoPrecio = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblMontoExtra = new System.Windows.Forms.Label();
            this.lblExtra = new System.Windows.Forms.Label();
            this.lblMontoTotal = new System.Windows.Forms.Label();
            this.lblMontoIva = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblPrecio = new System.Windows.Forms.Label();
            this.lblIva = new System.Windows.Forms.Label();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnAgregaProd = new System.Windows.Forms.Button();
            this.grbDescuento = new System.Windows.Forms.GroupBox();
            this.rdNoDescuento = new System.Windows.Forms.RadioButton();
            this.rdDescuento = new System.Windows.Forms.RadioButton();
            this.lblPrecioIndividual = new System.Windows.Forms.Label();
            this.txtMontoExtra = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbServicios = new System.Windows.Forms.ComboBox();
            this.cmbClientes = new System.Windows.Forms.ComboBox();
            this.txtReferencia = new System.Windows.Forms.TextBox();
            this.txtObservacion = new System.Windows.Forms.TextBox();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.lblObservaciones = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.lblPlaca = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.clbPiezas = new System.Windows.Forms.CheckedListBox();
            this.lblHora = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPiezas = new System.Windows.Forms.Label();
            this.lblEnderezado = new System.Windows.Forms.Label();
            this.ep2 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvmep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).BeginInit();
            this.grbEnderezado.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grbDescuento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep2)).BeginInit();
            this.SuspendLayout();
            // 
            // timHoraFecha
            // 
            this.timHoraFecha.Enabled = true;
            this.timHoraFecha.Tick += new System.EventHandler(this.timHoraFecha_Tick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(913, 250);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 14);
            this.label8.TabIndex = 107;
            this.label8.Text = "SERVICIOS";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.dgvmep);
            this.groupBox1.Location = new System.Drawing.Point(9, 269);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(890, 210);
            this.groupBox1.TabIndex = 106;
            this.groupBox1.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(396, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 14);
            this.label10.TabIndex = 130;
            this.label10.Text = "COTIZACIONES";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(234, 15);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 125;
            this.label9.Text = "Buscar";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(209, 10);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(21, 23);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 120;
            this.pictureBox1.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(5, 11);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(200, 20);
            this.textBox1.TabIndex = 119;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // dgvmep
            // 
            this.dgvmep.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvmep.Location = new System.Drawing.Point(0, 36);
            this.dgvmep.Name = "dgvmep";
            this.dgvmep.ReadOnly = true;
            this.dgvmep.RowHeadersWidth = 51;
            this.dgvmep.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvmep.Size = new System.Drawing.Size(890, 175);
            this.dgvmep.TabIndex = 89;
            this.dgvmep.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvmep_CellDoubleClick);
            this.dgvmep.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvmep_KeyDown);
            // 
            // dgvProductos
            // 
            this.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProductos.Location = new System.Drawing.Point(916, 269);
            this.dgvProductos.Margin = new System.Windows.Forms.Padding(2);
            this.dgvProductos.Name = "dgvProductos";
            this.dgvProductos.ReadOnly = true;
            this.dgvProductos.RowHeadersWidth = 51;
            this.dgvProductos.RowTemplate.Height = 24;
            this.dgvProductos.Size = new System.Drawing.Size(302, 210);
            this.dgvProductos.TabIndex = 105;
            this.dgvProductos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvProductos_KeyDown);
            // 
            // grbEnderezado
            // 
            this.grbEnderezado.Controls.Add(this.cmbProductos);
            this.grbEnderezado.Controls.Add(this.label7);
            this.grbEnderezado.Controls.Add(this.panel1);
            this.grbEnderezado.Controls.Add(this.btnLimpiar);
            this.grbEnderezado.Controls.Add(this.btnAgregaProd);
            this.grbEnderezado.Controls.Add(this.grbDescuento);
            this.grbEnderezado.Controls.Add(this.lblPrecioIndividual);
            this.grbEnderezado.Controls.Add(this.txtMontoExtra);
            this.grbEnderezado.Controls.Add(this.label2);
            this.grbEnderezado.Controls.Add(this.cmbServicios);
            this.grbEnderezado.Controls.Add(this.cmbClientes);
            this.grbEnderezado.Controls.Add(this.txtReferencia);
            this.grbEnderezado.Controls.Add(this.txtObservacion);
            this.grbEnderezado.Controls.Add(this.dtpFecha);
            this.grbEnderezado.Controls.Add(this.lblObservaciones);
            this.grbEnderezado.Controls.Add(this.btnSalir);
            this.grbEnderezado.Controls.Add(this.btnActualizar);
            this.grbEnderezado.Controls.Add(this.btnAgregar);
            this.grbEnderezado.Controls.Add(this.lblPlaca);
            this.grbEnderezado.Controls.Add(this.lblFecha);
            this.grbEnderezado.Controls.Add(this.lblCliente);
            this.grbEnderezado.Controls.Add(this.lblNumero);
            this.grbEnderezado.Location = new System.Drawing.Point(9, 34);
            this.grbEnderezado.Margin = new System.Windows.Forms.Padding(2);
            this.grbEnderezado.Name = "grbEnderezado";
            this.grbEnderezado.Padding = new System.Windows.Forms.Padding(2);
            this.grbEnderezado.Size = new System.Drawing.Size(890, 230);
            this.grbEnderezado.TabIndex = 104;
            this.grbEnderezado.TabStop = false;
            this.grbEnderezado.Text = "Información";
            // 
            // cmbProductos
            // 
            this.cmbProductos.FormattingEnabled = true;
            this.cmbProductos.Location = new System.Drawing.Point(442, 81);
            this.cmbProductos.Margin = new System.Windows.Forms.Padding(2);
            this.cmbProductos.Name = "cmbProductos";
            this.cmbProductos.Size = new System.Drawing.Size(170, 21);
            this.cmbProductos.TabIndex = 129;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(377, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 14);
            this.label7.TabIndex = 128;
            this.label7.Text = "VEHÍCULO";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtMontoPrecio);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lblMontoExtra);
            this.panel1.Controls.Add(this.lblExtra);
            this.panel1.Controls.Add(this.lblMontoTotal);
            this.panel1.Controls.Add(this.lblMontoIva);
            this.panel1.Controls.Add(this.lblTotal);
            this.panel1.Controls.Add(this.lblPrecio);
            this.panel1.Controls.Add(this.lblIva);
            this.panel1.Location = new System.Drawing.Point(667, 128);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(199, 84);
            this.panel1.TabIndex = 127;
            // 
            // txtMontoPrecio
            // 
            this.txtMontoPrecio.Location = new System.Drawing.Point(89, 6);
            this.txtMontoPrecio.Margin = new System.Windows.Forms.Padding(2);
            this.txtMontoPrecio.Name = "txtMontoPrecio";
            this.txtMontoPrecio.Size = new System.Drawing.Size(101, 20);
            this.txtMontoPrecio.TabIndex = 136;
            this.txtMontoPrecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMontoPrecio_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bahnschrift Condensed", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(63, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 13);
            this.label3.TabIndex = 135;
            this.label3.Text = "₡";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bahnschrift Condensed", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(63, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(12, 13);
            this.label4.TabIndex = 134;
            this.label4.Text = "₡";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bahnschrift Condensed", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(63, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 13);
            this.label5.TabIndex = 133;
            this.label5.Text = "₡";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bahnschrift Condensed", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(63, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 13);
            this.label6.TabIndex = 132;
            this.label6.Text = "₡";
            // 
            // lblMontoExtra
            // 
            this.lblMontoExtra.AutoSize = true;
            this.lblMontoExtra.Font = new System.Drawing.Font("Bahnschrift Condensed", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMontoExtra.Location = new System.Drawing.Point(87, 44);
            this.lblMontoExtra.Name = "lblMontoExtra";
            this.lblMontoExtra.Size = new System.Drawing.Size(0, 13);
            this.lblMontoExtra.TabIndex = 131;
            // 
            // lblExtra
            // 
            this.lblExtra.AutoSize = true;
            this.lblExtra.Font = new System.Drawing.Font("Bahnschrift Condensed", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExtra.Location = new System.Drawing.Point(6, 44);
            this.lblExtra.Name = "lblExtra";
            this.lblExtra.Size = new System.Drawing.Size(29, 13);
            this.lblExtra.TabIndex = 130;
            this.lblExtra.Text = "EXTRA";
            // 
            // lblMontoTotal
            // 
            this.lblMontoTotal.AutoSize = true;
            this.lblMontoTotal.Font = new System.Drawing.Font("Bahnschrift Condensed", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMontoTotal.Location = new System.Drawing.Point(87, 63);
            this.lblMontoTotal.Name = "lblMontoTotal";
            this.lblMontoTotal.Size = new System.Drawing.Size(0, 13);
            this.lblMontoTotal.TabIndex = 129;
            // 
            // lblMontoIva
            // 
            this.lblMontoIva.AutoSize = true;
            this.lblMontoIva.Font = new System.Drawing.Font("Bahnschrift Condensed", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMontoIva.Location = new System.Drawing.Point(87, 26);
            this.lblMontoIva.Name = "lblMontoIva";
            this.lblMontoIva.Size = new System.Drawing.Size(0, 13);
            this.lblMontoIva.TabIndex = 128;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Bahnschrift Condensed", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(7, 63);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(29, 13);
            this.lblTotal.TabIndex = 127;
            this.lblTotal.Text = "TOTAL";
            // 
            // lblPrecio
            // 
            this.lblPrecio.AutoSize = true;
            this.lblPrecio.Font = new System.Drawing.Font("Bahnschrift Condensed", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecio.Location = new System.Drawing.Point(3, 9);
            this.lblPrecio.Name = "lblPrecio";
            this.lblPrecio.Size = new System.Drawing.Size(33, 13);
            this.lblPrecio.TabIndex = 126;
            this.lblPrecio.Text = "PRECIO";
            // 
            // lblIva
            // 
            this.lblIva.AutoSize = true;
            this.lblIva.Font = new System.Drawing.Font("Bahnschrift Condensed", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIva.Location = new System.Drawing.Point(17, 26);
            this.lblIva.Name = "lblIva";
            this.lblIva.Size = new System.Drawing.Size(18, 13);
            this.lblIva.TabIndex = 125;
            this.lblIva.Text = "IVA";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpiar.Location = new System.Drawing.Point(128, 197);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(113, 23);
            this.btnLimpiar.TabIndex = 126;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAgregaProd
            // 
            this.btnAgregaProd.Location = new System.Drawing.Point(442, 45);
            this.btnAgregaProd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAgregaProd.Name = "btnAgregaProd";
            this.btnAgregaProd.Size = new System.Drawing.Size(108, 20);
            this.btnAgregaProd.TabIndex = 125;
            this.btnAgregaProd.Text = "Agregar servicio";
            this.btnAgregaProd.UseVisualStyleBackColor = true;
            this.btnAgregaProd.Click += new System.EventHandler(this.btnAgregaProd_Click);
            // 
            // grbDescuento
            // 
            this.grbDescuento.BackColor = System.Drawing.Color.Transparent;
            this.grbDescuento.Controls.Add(this.rdNoDescuento);
            this.grbDescuento.Controls.Add(this.rdDescuento);
            this.grbDescuento.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDescuento.ForeColor = System.Drawing.Color.Black;
            this.grbDescuento.Location = new System.Drawing.Point(692, 19);
            this.grbDescuento.Name = "grbDescuento";
            this.grbDescuento.Size = new System.Drawing.Size(154, 65);
            this.grbDescuento.TabIndex = 119;
            this.grbDescuento.TabStop = false;
            this.grbDescuento.Text = "APLICA DESCUENTO";
            // 
            // rdNoDescuento
            // 
            this.rdNoDescuento.AutoSize = true;
            this.rdNoDescuento.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdNoDescuento.Location = new System.Drawing.Point(11, 48);
            this.rdNoDescuento.Name = "rdNoDescuento";
            this.rdNoDescuento.Size = new System.Drawing.Size(40, 18);
            this.rdNoDescuento.TabIndex = 1;
            this.rdNoDescuento.TabStop = true;
            this.rdNoDescuento.Text = "NO";
            this.rdNoDescuento.UseVisualStyleBackColor = true;
            this.rdNoDescuento.Click += new System.EventHandler(this.rdNoDescuento_Click);
            // 
            // rdDescuento
            // 
            this.rdDescuento.AutoSize = true;
            this.rdDescuento.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdDescuento.Location = new System.Drawing.Point(11, 23);
            this.rdDescuento.Name = "rdDescuento";
            this.rdDescuento.Size = new System.Drawing.Size(35, 18);
            this.rdDescuento.TabIndex = 0;
            this.rdDescuento.TabStop = true;
            this.rdDescuento.Text = "SI";
            this.rdDescuento.UseVisualStyleBackColor = true;
            this.rdDescuento.Click += new System.EventHandler(this.rdDescuento_Click);
            // 
            // lblPrecioIndividual
            // 
            this.lblPrecioIndividual.AutoSize = true;
            this.lblPrecioIndividual.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioIndividual.Location = new System.Drawing.Point(616, 24);
            this.lblPrecioIndividual.Name = "lblPrecioIndividual";
            this.lblPrecioIndividual.Size = new System.Drawing.Size(15, 14);
            this.lblPrecioIndividual.TabIndex = 118;
            this.lblPrecioIndividual.Text = "₡";
            this.lblPrecioIndividual.Visible = false;
            // 
            // txtMontoExtra
            // 
            this.txtMontoExtra.Location = new System.Drawing.Point(442, 120);
            this.txtMontoExtra.Name = "txtMontoExtra";
            this.txtMontoExtra.Size = new System.Drawing.Size(169, 20);
            this.txtMontoExtra.TabIndex = 117;
            this.txtMontoExtra.TextChanged += new System.EventHandler(this.txtMontoExtra_TextChanged);
            this.txtMontoExtra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMontoExtra_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(354, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 14);
            this.label2.TabIndex = 116;
            this.label2.Text = "MONTO EXTRA";
            // 
            // cmbServicios
            // 
            this.cmbServicios.FormattingEnabled = true;
            this.cmbServicios.Location = new System.Drawing.Point(442, 21);
            this.cmbServicios.Margin = new System.Windows.Forms.Padding(2);
            this.cmbServicios.Name = "cmbServicios";
            this.cmbServicios.Size = new System.Drawing.Size(170, 21);
            this.cmbServicios.TabIndex = 115;
            this.cmbServicios.SelectedIndexChanged += new System.EventHandler(this.cmbProductos_SelectedIndexChanged);
            this.cmbServicios.TextUpdate += new System.EventHandler(this.cmbProductos_TextUpdate);
            // 
            // cmbClientes
            // 
            this.cmbClientes.FormattingEnabled = true;
            this.cmbClientes.Location = new System.Drawing.Point(111, 93);
            this.cmbClientes.Margin = new System.Windows.Forms.Padding(2);
            this.cmbClientes.Name = "cmbClientes";
            this.cmbClientes.Size = new System.Drawing.Size(170, 21);
            this.cmbClientes.TabIndex = 114;
            this.cmbClientes.SelectedIndexChanged += new System.EventHandler(this.cmbClientes_SelectedIndexChanged_1);
            // 
            // txtReferencia
            // 
            this.txtReferencia.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReferencia.Location = new System.Drawing.Point(111, 19);
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.ReadOnly = true;
            this.txtReferencia.Size = new System.Drawing.Size(60, 20);
            this.txtReferencia.TabIndex = 107;
            // 
            // txtObservacion
            // 
            this.txtObservacion.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtObservacion.Location = new System.Drawing.Point(442, 156);
            this.txtObservacion.Multiline = true;
            this.txtObservacion.Name = "txtObservacion";
            this.txtObservacion.Size = new System.Drawing.Size(169, 56);
            this.txtObservacion.TabIndex = 106;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(111, 54);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(170, 20);
            this.dtpFecha.TabIndex = 105;
            // 
            // lblObservaciones
            // 
            this.lblObservaciones.AutoSize = true;
            this.lblObservaciones.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObservaciones.Location = new System.Drawing.Point(342, 158);
            this.lblObservaciones.Name = "lblObservaciones";
            this.lblObservaciones.Size = new System.Drawing.Size(96, 14);
            this.lblObservaciones.TabIndex = 112;
            this.lblObservaciones.Text = "OBSERVACIONES";
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalir.Location = new System.Drawing.Point(5, 197);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(98, 23);
            this.btnSalir.TabIndex = 14;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActualizar.Location = new System.Drawing.Point(128, 156);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(113, 23);
            this.btnActualizar.TabIndex = 13;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregar.Location = new System.Drawing.Point(5, 156);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(98, 23);
            this.btnAgregar.TabIndex = 11;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // lblPlaca
            // 
            this.lblPlaca.AutoSize = true;
            this.lblPlaca.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlaca.Location = new System.Drawing.Point(375, 24);
            this.lblPlaca.Name = "lblPlaca";
            this.lblPlaca.Size = new System.Drawing.Size(64, 14);
            this.lblPlaca.TabIndex = 111;
            this.lblPlaca.Text = "SERVICIOS";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.Location = new System.Drawing.Point(9, 54);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(42, 14);
            this.lblFecha.TabIndex = 110;
            this.lblFecha.Text = "FECHA";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(9, 95);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(51, 14);
            this.lblCliente.TabIndex = 109;
            this.lblCliente.Text = "CLIENTE";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(9, 22);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(97, 14);
            this.lblNumero.TabIndex = 108;
            this.lblNumero.Text = "# DE RERERENCIA";
            // 
            // clbPiezas
            // 
            this.clbPiezas.BackColor = System.Drawing.Color.Silver;
            this.clbPiezas.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbPiezas.FormattingEnabled = true;
            this.clbPiezas.Location = new System.Drawing.Point(916, 39);
            this.clbPiezas.MultiColumn = true;
            this.clbPiezas.Name = "clbPiezas";
            this.clbPiezas.ScrollAlwaysVisible = true;
            this.clbPiezas.Size = new System.Drawing.Size(302, 199);
            this.clbPiezas.TabIndex = 94;
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.BackColor = System.Drawing.Color.DimGray;
            this.lblHora.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.ForeColor = System.Drawing.Color.White;
            this.lblHora.Location = new System.Drawing.Point(712, 7);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(39, 14);
            this.lblHora.TabIndex = 93;
            this.lblHora.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DimGray;
            this.label1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(617, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 14);
            this.label1.TabIndex = 92;
            this.label1.Text = "label1";
            // 
            // lblPiezas
            // 
            this.lblPiezas.AutoSize = true;
            this.lblPiezas.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPiezas.Location = new System.Drawing.Point(913, 15);
            this.lblPiezas.Name = "lblPiezas";
            this.lblPiezas.Size = new System.Drawing.Size(100, 14);
            this.lblPiezas.TabIndex = 51;
            this.lblPiezas.Text = "PIEZAS DAÑADAS";
            // 
            // lblEnderezado
            // 
            this.lblEnderezado.AutoSize = true;
            this.lblEnderezado.BackColor = System.Drawing.Color.White;
            this.lblEnderezado.Font = new System.Drawing.Font("Arial Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnderezado.ForeColor = System.Drawing.Color.Black;
            this.lblEnderezado.Location = new System.Drawing.Point(132, 5);
            this.lblEnderezado.Name = "lblEnderezado";
            this.lblEnderezado.Size = new System.Drawing.Size(474, 27);
            this.lblEnderezado.TabIndex = 31;
            this.lblEnderezado.Text = "MANTENIMIENTO ENDEREZADO Y PINTURA";
            // 
            // ep2
            // 
            this.ep2.ContainerControl = this;
            // 
            // frmMantenimientoEnderezado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1232, 495);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvProductos);
            this.Controls.Add(this.grbEnderezado);
            this.Controls.Add(this.clbPiezas);
            this.Controls.Add(this.lblHora);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblPiezas);
            this.Controls.Add(this.lblEnderezado);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3);
            this.MaximumSize = new System.Drawing.Size(1248, 534);
            this.MinimumSize = new System.Drawing.Size(1248, 534);
            this.Name = "frmMantenimientoEnderezado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MANTENIMIENTO ENDEREZADO Y PINTURA";
            this.Load += new System.EventHandler(this.frmMantenimientoEnderezado_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvmep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).EndInit();
            this.grbEnderezado.ResumeLayout(false);
            this.grbEnderezado.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.grbDescuento.ResumeLayout(false);
            this.grbDescuento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ep2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEnderezado;
        private System.Windows.Forms.Label lblPiezas;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ErrorProvider ep2;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timHoraFecha;
        private System.Windows.Forms.CheckedListBox clbPiezas;
        private System.Windows.Forms.GroupBox grbEnderezado;
        private System.Windows.Forms.Label lblPrecioIndividual;
        private System.Windows.Forms.TextBox txtMontoExtra;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbServicios;
        private System.Windows.Forms.ComboBox cmbClientes;
        private System.Windows.Forms.TextBox txtReferencia;
        private System.Windows.Forms.TextBox txtObservacion;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label lblPlaca;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.GroupBox grbDescuento;
        private System.Windows.Forms.RadioButton rdNoDescuento;
        private System.Windows.Forms.RadioButton rdDescuento;
        private System.Windows.Forms.Button btnAgregaProd;
        private System.Windows.Forms.DataGridView dgvProductos;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtMontoPrecio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblMontoExtra;
        private System.Windows.Forms.Label lblExtra;
        private System.Windows.Forms.Label lblMontoTotal;
        private System.Windows.Forms.Label lblMontoIva;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblPrecio;
        private System.Windows.Forms.Label lblIva;
        private System.Windows.Forms.Label lblObservaciones;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvmep;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cmbProductos;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
    }
}