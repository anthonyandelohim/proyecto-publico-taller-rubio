﻿namespace Body2.Vista
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.rEGISTROToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cOTIZACIONToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eNDEREZADOYPINTURAToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pRODUCTOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pIEZASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sERVICIOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mANTENIMIENTOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cLIENTEToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rOLYPERMISOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uSUARIOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rEPORTEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmCotizaciones = new System.Windows.Forms.ToolStripMenuItem();
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sALIRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tIPODESERVICIOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eNDEREZADOYPINTURAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tRABAJOSFIBRADEVIDRIOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pULIDODECARROCERIAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMantenimientos = new System.Windows.Forms.ToolStripMenuItem();
            this.uSUARIOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rOLESYPERMISOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cLIENTESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rEPARACIONESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tIPODESERVICIOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eNDEREZADOYPINTURAToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tRABAJOSFIBRADEVIDRIOToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pULIDODECARROCERIAToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmReportes = new System.Windows.Forms.ToolStripMenuItem();
            this.iNGRESOSYSALIDASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mOVIMIENTOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cOTIZACIONESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cLIENTESToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vEHICULOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAcercade = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.iNICIOToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.oTROUSUARIOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sALIRToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.aYUDAToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.aCERCADEToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.iNICIOToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.sALIRToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.aYUDAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timHoraFecha = new System.Windows.Forms.Timer(this.components);
            this.lblHora = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.cAMBIARCONTRASEÑAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iNICIOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rEGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sALIRToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aYUDAToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aCERCADEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rEGISTROSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mANTENIMIENTOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rEPORTESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iNICIOToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sALIRToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.iNICIOToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.sALIRToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.lblLoggueado = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rEGISTROToolStripMenuItem,
            this.mANTENIMIENTOToolStripMenuItem,
            this.rEPORTEToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 30);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(708, 30);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // rEGISTROToolStripMenuItem
            // 
            this.rEGISTROToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cOTIZACIONToolStripMenuItem,
            this.pRODUCTOSToolStripMenuItem,
            this.pIEZASToolStripMenuItem,
            this.sERVICIOSToolStripMenuItem});
            this.rEGISTROToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rEGISTROToolStripMenuItem.Image")));
            this.rEGISTROToolStripMenuItem.Name = "rEGISTROToolStripMenuItem";
            this.rEGISTROToolStripMenuItem.Size = new System.Drawing.Size(110, 26);
            this.rEGISTROToolStripMenuItem.Text = "REGISTRO";
            // 
            // cOTIZACIONToolStripMenuItem
            // 
            this.cOTIZACIONToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eNDEREZADOYPINTURAToolStripMenuItem2});
            this.cOTIZACIONToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cOTIZACIONToolStripMenuItem.Image")));
            this.cOTIZACIONToolStripMenuItem.Name = "cOTIZACIONToolStripMenuItem";
            this.cOTIZACIONToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.cOTIZACIONToolStripMenuItem.Text = "COTIZACION";
            // 
            // eNDEREZADOYPINTURAToolStripMenuItem2
            // 
            this.eNDEREZADOYPINTURAToolStripMenuItem2.Checked = true;
            this.eNDEREZADOYPINTURAToolStripMenuItem2.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.eNDEREZADOYPINTURAToolStripMenuItem2.Image = global::Body2.Vista.Properties.Resources.painting;
            this.eNDEREZADOYPINTURAToolStripMenuItem2.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.eNDEREZADOYPINTURAToolStripMenuItem2.Name = "eNDEREZADOYPINTURAToolStripMenuItem2";
            this.eNDEREZADOYPINTURAToolStripMenuItem2.Size = new System.Drawing.Size(264, 26);
            this.eNDEREZADOYPINTURAToolStripMenuItem2.Text = "ENDEREZADO Y PINTURA";
            this.eNDEREZADOYPINTURAToolStripMenuItem2.Click += new System.EventHandler(this.eNDEREZADOYPINTURAToolStripMenuItem2_Click);
            // 
            // pRODUCTOSToolStripMenuItem
            // 
            this.pRODUCTOSToolStripMenuItem.Image = global::Body2.Vista.Properties.Resources.license_plate;
            this.pRODUCTOSToolStripMenuItem.Name = "pRODUCTOSToolStripMenuItem";
            this.pRODUCTOSToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.pRODUCTOSToolStripMenuItem.Text = "PRODUCTOS";
            this.pRODUCTOSToolStripMenuItem.Click += new System.EventHandler(this.pRODUCTOSToolStripMenuItem_Click);
            // 
            // pIEZASToolStripMenuItem
            // 
            this.pIEZASToolStripMenuItem.Image = global::Body2.Vista.Properties.Resources.shock_absorber__1_;
            this.pIEZASToolStripMenuItem.Name = "pIEZASToolStripMenuItem";
            this.pIEZASToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.pIEZASToolStripMenuItem.Text = "PIEZAS";
            this.pIEZASToolStripMenuItem.Click += new System.EventHandler(this.pIEZASToolStripMenuItem_Click);
            // 
            // sERVICIOSToolStripMenuItem
            // 
            this.sERVICIOSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sERVICIOSToolStripMenuItem.Image")));
            this.sERVICIOSToolStripMenuItem.Name = "sERVICIOSToolStripMenuItem";
            this.sERVICIOSToolStripMenuItem.Size = new System.Drawing.Size(177, 26);
            this.sERVICIOSToolStripMenuItem.Text = "SERVICIOS";
            this.sERVICIOSToolStripMenuItem.Click += new System.EventHandler(this.sERVICIOSToolStripMenuItem_Click);
            // 
            // mANTENIMIENTOToolStripMenuItem
            // 
            this.mANTENIMIENTOToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cLIENTEToolStripMenuItem1,
            this.rOLYPERMISOToolStripMenuItem,
            this.uSUARIOToolStripMenuItem});
            this.mANTENIMIENTOToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("mANTENIMIENTOToolStripMenuItem.Image")));
            this.mANTENIMIENTOToolStripMenuItem.Name = "mANTENIMIENTOToolStripMenuItem";
            this.mANTENIMIENTOToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.mANTENIMIENTOToolStripMenuItem.Text = "MANTENIMIENTO";
            // 
            // cLIENTEToolStripMenuItem1
            // 
            this.cLIENTEToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("cLIENTEToolStripMenuItem1.Image")));
            this.cLIENTEToolStripMenuItem1.Name = "cLIENTEToolStripMenuItem1";
            this.cLIENTEToolStripMenuItem1.Size = new System.Drawing.Size(196, 26);
            this.cLIENTEToolStripMenuItem1.Text = "CLIENTE";
            this.cLIENTEToolStripMenuItem1.Click += new System.EventHandler(this.cLIENTEToolStripMenuItem1_Click);
            // 
            // rOLYPERMISOToolStripMenuItem
            // 
            this.rOLYPERMISOToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rOLYPERMISOToolStripMenuItem.Image")));
            this.rOLYPERMISOToolStripMenuItem.Name = "rOLYPERMISOToolStripMenuItem";
            this.rOLYPERMISOToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.rOLYPERMISOToolStripMenuItem.Text = "ROL Y PERMISO";
            this.rOLYPERMISOToolStripMenuItem.Click += new System.EventHandler(this.rOLYPERMISOToolStripMenuItem_Click);
            // 
            // uSUARIOToolStripMenuItem
            // 
            this.uSUARIOToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("uSUARIOToolStripMenuItem.Image")));
            this.uSUARIOToolStripMenuItem.Name = "uSUARIOToolStripMenuItem";
            this.uSUARIOToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.uSUARIOToolStripMenuItem.Text = "USUARIO";
            this.uSUARIOToolStripMenuItem.Click += new System.EventHandler(this.uSUARIOToolStripMenuItem_Click);
            // 
            // rEPORTEToolStripMenuItem
            // 
            this.rEPORTEToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rEPORTEToolStripMenuItem.Image")));
            this.rEPORTEToolStripMenuItem.Name = "rEPORTEToolStripMenuItem";
            this.rEPORTEToolStripMenuItem.Size = new System.Drawing.Size(103, 26);
            this.rEPORTEToolStripMenuItem.Text = "REPORTE";
            this.rEPORTEToolStripMenuItem.Click += new System.EventHandler(this.rEPORTEToolStripMenuItem_Click);
            // 
            // tsmCotizaciones
            // 
            this.tsmCotizaciones.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem,
            this.sALIRToolStripMenuItem});
            this.tsmCotizaciones.Image = ((System.Drawing.Image)(resources.GetObject("tsmCotizaciones.Image")));
            this.tsmCotizaciones.Name = "tsmCotizaciones";
            this.tsmCotizaciones.Size = new System.Drawing.Size(93, 20);
            this.tsmCotizaciones.Text = "REGISTROS";
            // 
            // iNGRESARCONOTROUSUARIOToolStripMenuItem
            // 
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("iNGRESARCONOTROUSUARIOToolStripMenuItem.Image")));
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem.Name = "iNGRESARCONOTROUSUARIOToolStripMenuItem";
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem.Size = new System.Drawing.Size(256, 26);
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem.Text = "REGISTRAR CLIENTE";
            // 
            // sALIRToolStripMenuItem
            // 
            this.sALIRToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tIPODESERVICIOSToolStripMenuItem});
            this.sALIRToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sALIRToolStripMenuItem.Image")));
            this.sALIRToolStripMenuItem.Name = "sALIRToolStripMenuItem";
            this.sALIRToolStripMenuItem.Size = new System.Drawing.Size(256, 26);
            this.sALIRToolStripMenuItem.Text = "REGISTRAR COTIZACION";
            // 
            // tIPODESERVICIOSToolStripMenuItem
            // 
            this.tIPODESERVICIOSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eNDEREZADOYPINTURAToolStripMenuItem,
            this.tRABAJOSFIBRADEVIDRIOToolStripMenuItem,
            this.pULIDODECARROCERIAToolStripMenuItem});
            this.tIPODESERVICIOSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("tIPODESERVICIOSToolStripMenuItem.Image")));
            this.tIPODESERVICIOSToolStripMenuItem.Name = "tIPODESERVICIOSToolStripMenuItem";
            this.tIPODESERVICIOSToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
            this.tIPODESERVICIOSToolStripMenuItem.Text = "TIPO DE SERVICIOS";
            // 
            // eNDEREZADOYPINTURAToolStripMenuItem
            // 
            this.eNDEREZADOYPINTURAToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("eNDEREZADOYPINTURAToolStripMenuItem.Image")));
            this.eNDEREZADOYPINTURAToolStripMenuItem.Name = "eNDEREZADOYPINTURAToolStripMenuItem";
            this.eNDEREZADOYPINTURAToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
            this.eNDEREZADOYPINTURAToolStripMenuItem.Text = "ENDEREZADO Y PINTURA";
            // 
            // tRABAJOSFIBRADEVIDRIOToolStripMenuItem
            // 
            this.tRABAJOSFIBRADEVIDRIOToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("tRABAJOSFIBRADEVIDRIOToolStripMenuItem.Image")));
            this.tRABAJOSFIBRADEVIDRIOToolStripMenuItem.Name = "tRABAJOSFIBRADEVIDRIOToolStripMenuItem";
            this.tRABAJOSFIBRADEVIDRIOToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
            this.tRABAJOSFIBRADEVIDRIOToolStripMenuItem.Text = "TRABAJOS FIBRA DE VIDRIO";
            // 
            // pULIDODECARROCERIAToolStripMenuItem
            // 
            this.pULIDODECARROCERIAToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pULIDODECARROCERIAToolStripMenuItem.Image")));
            this.pULIDODECARROCERIAToolStripMenuItem.Name = "pULIDODECARROCERIAToolStripMenuItem";
            this.pULIDODECARROCERIAToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
            this.pULIDODECARROCERIAToolStripMenuItem.Text = "PULIDO DE CARROCERIA";
            // 
            // tsmMantenimientos
            // 
            this.tsmMantenimientos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uSUARIOSToolStripMenuItem,
            this.rOLESYPERMISOSToolStripMenuItem,
            this.cLIENTESToolStripMenuItem,
            this.rEPARACIONESToolStripMenuItem});
            this.tsmMantenimientos.Image = ((System.Drawing.Image)(resources.GetObject("tsmMantenimientos.Image")));
            this.tsmMantenimientos.Name = "tsmMantenimientos";
            this.tsmMantenimientos.Size = new System.Drawing.Size(136, 20);
            this.tsmMantenimientos.Text = "MANTENIMIENTOS";
            // 
            // uSUARIOSToolStripMenuItem
            // 
            this.uSUARIOSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("uSUARIOSToolStripMenuItem.Image")));
            this.uSUARIOSToolStripMenuItem.Name = "uSUARIOSToolStripMenuItem";
            this.uSUARIOSToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
            this.uSUARIOSToolStripMenuItem.Text = "USUARIOS";
            this.uSUARIOSToolStripMenuItem.Click += new System.EventHandler(this.uSUARIOSToolStripMenuItem_Click);
            // 
            // rOLESYPERMISOSToolStripMenuItem
            // 
            this.rOLESYPERMISOSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rOLESYPERMISOSToolStripMenuItem.Image")));
            this.rOLESYPERMISOSToolStripMenuItem.Name = "rOLESYPERMISOSToolStripMenuItem";
            this.rOLESYPERMISOSToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
            this.rOLESYPERMISOSToolStripMenuItem.Text = "ROLES Y PERMISOS";
            this.rOLESYPERMISOSToolStripMenuItem.Click += new System.EventHandler(this.rOLESYPERMISOSToolStripMenuItem_Click);
            // 
            // cLIENTESToolStripMenuItem
            // 
            this.cLIENTESToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cLIENTESToolStripMenuItem.Image")));
            this.cLIENTESToolStripMenuItem.Name = "cLIENTESToolStripMenuItem";
            this.cLIENTESToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
            this.cLIENTESToolStripMenuItem.Text = "CLIENTES";
            // 
            // rEPARACIONESToolStripMenuItem
            // 
            this.rEPARACIONESToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tIPODESERVICIOToolStripMenuItem});
            this.rEPARACIONESToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rEPARACIONESToolStripMenuItem.Image")));
            this.rEPARACIONESToolStripMenuItem.Name = "rEPARACIONESToolStripMenuItem";
            this.rEPARACIONESToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
            this.rEPARACIONESToolStripMenuItem.Text = "COTIZACIONES";
            // 
            // tIPODESERVICIOToolStripMenuItem
            // 
            this.tIPODESERVICIOToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eNDEREZADOYPINTURAToolStripMenuItem1,
            this.tRABAJOSFIBRADEVIDRIOToolStripMenuItem1,
            this.pULIDODECARROCERIAToolStripMenuItem1});
            this.tIPODESERVICIOToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("tIPODESERVICIOToolStripMenuItem.Image")));
            this.tIPODESERVICIOToolStripMenuItem.Name = "tIPODESERVICIOToolStripMenuItem";
            this.tIPODESERVICIOToolStripMenuItem.Size = new System.Drawing.Size(212, 26);
            this.tIPODESERVICIOToolStripMenuItem.Text = "TIPO DE SERVICIO";
            // 
            // eNDEREZADOYPINTURAToolStripMenuItem1
            // 
            this.eNDEREZADOYPINTURAToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("eNDEREZADOYPINTURAToolStripMenuItem1.Image")));
            this.eNDEREZADOYPINTURAToolStripMenuItem1.Name = "eNDEREZADOYPINTURAToolStripMenuItem1";
            this.eNDEREZADOYPINTURAToolStripMenuItem1.Size = new System.Drawing.Size(264, 26);
            this.eNDEREZADOYPINTURAToolStripMenuItem1.Text = "ENDEREZADO Y PINTURA";
            // 
            // tRABAJOSFIBRADEVIDRIOToolStripMenuItem1
            // 
            this.tRABAJOSFIBRADEVIDRIOToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("tRABAJOSFIBRADEVIDRIOToolStripMenuItem1.Image")));
            this.tRABAJOSFIBRADEVIDRIOToolStripMenuItem1.Name = "tRABAJOSFIBRADEVIDRIOToolStripMenuItem1";
            this.tRABAJOSFIBRADEVIDRIOToolStripMenuItem1.Size = new System.Drawing.Size(264, 26);
            // 
            // pULIDODECARROCERIAToolStripMenuItem1
            // 
            this.pULIDODECARROCERIAToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("pULIDODECARROCERIAToolStripMenuItem1.Image")));
            this.pULIDODECARROCERIAToolStripMenuItem1.Name = "pULIDODECARROCERIAToolStripMenuItem1";
            this.pULIDODECARROCERIAToolStripMenuItem1.Size = new System.Drawing.Size(264, 26);
            this.pULIDODECARROCERIAToolStripMenuItem1.Text = "PULIDO DE CARROCERIA";
            // 
            // tsmReportes
            // 
            this.tsmReportes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iNGRESOSYSALIDASToolStripMenuItem,
            this.mOVIMIENTOSToolStripMenuItem,
            this.cOTIZACIONESToolStripMenuItem,
            this.cLIENTESToolStripMenuItem1,
            this.vEHICULOSToolStripMenuItem});
            this.tsmReportes.Image = ((System.Drawing.Image)(resources.GetObject("tsmReportes.Image")));
            this.tsmReportes.Name = "tsmReportes";
            this.tsmReportes.Size = new System.Drawing.Size(88, 20);
            this.tsmReportes.Text = "REPORTES";
            // 
            // iNGRESOSYSALIDASToolStripMenuItem
            // 
            this.iNGRESOSYSALIDASToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("iNGRESOSYSALIDASToolStripMenuItem.Image")));
            this.iNGRESOSYSALIDASToolStripMenuItem.Name = "iNGRESOSYSALIDASToolStripMenuItem";
            this.iNGRESOSYSALIDASToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.iNGRESOSYSALIDASToolStripMenuItem.Text = "INGRESOS Y SALIDAS";
            // 
            // mOVIMIENTOSToolStripMenuItem
            // 
            this.mOVIMIENTOSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("mOVIMIENTOSToolStripMenuItem.Image")));
            this.mOVIMIENTOSToolStripMenuItem.Name = "mOVIMIENTOSToolStripMenuItem";
            this.mOVIMIENTOSToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.mOVIMIENTOSToolStripMenuItem.Text = "MOVIMIENTOS";
            // 
            // cOTIZACIONESToolStripMenuItem
            // 
            this.cOTIZACIONESToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cOTIZACIONESToolStripMenuItem.Image")));
            this.cOTIZACIONESToolStripMenuItem.Name = "cOTIZACIONESToolStripMenuItem";
            this.cOTIZACIONESToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.cOTIZACIONESToolStripMenuItem.Text = "COTIZACIONES";
            // 
            // cLIENTESToolStripMenuItem1
            // 
            this.cLIENTESToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("cLIENTESToolStripMenuItem1.Image")));
            this.cLIENTESToolStripMenuItem1.Name = "cLIENTESToolStripMenuItem1";
            this.cLIENTESToolStripMenuItem1.Size = new System.Drawing.Size(235, 26);
            this.cLIENTESToolStripMenuItem1.Text = "CLIENTES";
            // 
            // vEHICULOSToolStripMenuItem
            // 
            this.vEHICULOSToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("vEHICULOSToolStripMenuItem.Image")));
            this.vEHICULOSToolStripMenuItem.Name = "vEHICULOSToolStripMenuItem";
            this.vEHICULOSToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.vEHICULOSToolStripMenuItem.Text = "VEHICULOS";
            // 
            // tsmAcercade
            // 
            this.tsmAcercade.Image = ((System.Drawing.Image)(resources.GetObject("tsmAcercade.Image")));
            this.tsmAcercade.Name = "tsmAcercade";
            this.tsmAcercade.Size = new System.Drawing.Size(97, 20);
            this.tsmAcercade.Text = "ACERCA DE";
            // 
            // menuStrip2
            // 
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iNICIOToolStripMenuItem4,
            this.aYUDAToolStripMenuItem2,
            this.aCERCADEToolStripMenuItem1});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip2.Size = new System.Drawing.Size(708, 30);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // iNICIOToolStripMenuItem4
            // 
            this.iNICIOToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oTROUSUARIOToolStripMenuItem,
            this.sALIRToolStripMenuItem5});
            this.iNICIOToolStripMenuItem4.Image = ((System.Drawing.Image)(resources.GetObject("iNICIOToolStripMenuItem4.Image")));
            this.iNICIOToolStripMenuItem4.Name = "iNICIOToolStripMenuItem4";
            this.iNICIOToolStripMenuItem4.Size = new System.Drawing.Size(86, 26);
            this.iNICIOToolStripMenuItem4.Text = "INICIO";
            // 
            // oTROUSUARIOToolStripMenuItem
            // 
            this.oTROUSUARIOToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("oTROUSUARIOToolStripMenuItem.Image")));
            this.oTROUSUARIOToolStripMenuItem.Name = "oTROUSUARIOToolStripMenuItem";
            this.oTROUSUARIOToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
            this.oTROUSUARIOToolStripMenuItem.Text = "OTRO USUARIO";
            this.oTROUSUARIOToolStripMenuItem.Click += new System.EventHandler(this.oTROUSUARIOToolStripMenuItem_Click);
            // 
            // sALIRToolStripMenuItem5
            // 
            this.sALIRToolStripMenuItem5.Image = ((System.Drawing.Image)(resources.GetObject("sALIRToolStripMenuItem5.Image")));
            this.sALIRToolStripMenuItem5.Name = "sALIRToolStripMenuItem5";
            this.sALIRToolStripMenuItem5.Size = new System.Drawing.Size(196, 26);
            this.sALIRToolStripMenuItem5.Text = "SALIR";
            this.sALIRToolStripMenuItem5.Click += new System.EventHandler(this.sALIRToolStripMenuItem5_Click);
            // 
            // aYUDAToolStripMenuItem2
            // 
            this.aYUDAToolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("aYUDAToolStripMenuItem2.Image")));
            this.aYUDAToolStripMenuItem2.Name = "aYUDAToolStripMenuItem2";
            this.aYUDAToolStripMenuItem2.Size = new System.Drawing.Size(91, 26);
            this.aYUDAToolStripMenuItem2.Text = "AYUDA";
            this.aYUDAToolStripMenuItem2.Click += new System.EventHandler(this.aYUDAToolStripMenuItem2_Click);
            // 
            // aCERCADEToolStripMenuItem1
            // 
            this.aCERCADEToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("aCERCADEToolStripMenuItem1.Image")));
            this.aCERCADEToolStripMenuItem1.Name = "aCERCADEToolStripMenuItem1";
            this.aCERCADEToolStripMenuItem1.Size = new System.Drawing.Size(121, 26);
            this.aCERCADEToolStripMenuItem1.Text = "ACERCA DE";
            this.aCERCADEToolStripMenuItem1.Click += new System.EventHandler(this.aCERCADEToolStripMenuItem1_Click);
            // 
            // iNICIOToolStripMenuItem1
            // 
            this.iNICIOToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem2,
            this.sALIRToolStripMenuItem2});
            this.iNICIOToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("iNICIOToolStripMenuItem1.Image")));
            this.iNICIOToolStripMenuItem1.Name = "iNICIOToolStripMenuItem1";
            this.iNICIOToolStripMenuItem1.Size = new System.Drawing.Size(70, 20);
            this.iNICIOToolStripMenuItem1.Text = "INICIO";
            // 
            // iNGRESARCONOTROUSUARIOToolStripMenuItem2
            // 
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("iNGRESARCONOTROUSUARIOToolStripMenuItem2.Image")));
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem2.Name = "iNGRESARCONOTROUSUARIOToolStripMenuItem2";
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem2.Size = new System.Drawing.Size(304, 26);
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem2.Text = "INGRESAR CON OTRO USUARIO";
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem2.Click += new System.EventHandler(this.iNGRESARCONOTROUSUARIOToolStripMenuItem2_Click);
            // 
            // sALIRToolStripMenuItem2
            // 
            this.sALIRToolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("sALIRToolStripMenuItem2.Image")));
            this.sALIRToolStripMenuItem2.Name = "sALIRToolStripMenuItem2";
            this.sALIRToolStripMenuItem2.Size = new System.Drawing.Size(304, 26);
            this.sALIRToolStripMenuItem2.Text = "SALIR";
            // 
            // aYUDAToolStripMenuItem
            // 
            this.aYUDAToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("aYUDAToolStripMenuItem.Image")));
            this.aYUDAToolStripMenuItem.Name = "aYUDAToolStripMenuItem";
            this.aYUDAToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.aYUDAToolStripMenuItem.Text = "AYUDA";
            // 
            // timHoraFecha
            // 
            this.timHoraFecha.Enabled = true;
            this.timHoraFecha.Tick += new System.EventHandler(this.timHoraFecha_Tick);
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.BackColor = System.Drawing.Color.Red;
            this.lblHora.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.ForeColor = System.Drawing.Color.White;
            this.lblHora.Location = new System.Drawing.Point(13, 565);
            this.lblHora.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(85, 29);
            this.lblHora.TabIndex = 24;
            this.lblHora.Text = "label1";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.BackColor = System.Drawing.Color.Red;
            this.lblFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.ForeColor = System.Drawing.Color.White;
            this.lblFecha.Location = new System.Drawing.Point(521, 565);
            this.lblFecha.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(85, 29);
            this.lblFecha.TabIndex = 23;
            this.lblFecha.Text = "label1";
            // 
            // cAMBIARCONTRASEÑAToolStripMenuItem
            // 
            this.cAMBIARCONTRASEÑAToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cAMBIARCONTRASEÑAToolStripMenuItem.Image")));
            this.cAMBIARCONTRASEÑAToolStripMenuItem.Name = "cAMBIARCONTRASEÑAToolStripMenuItem";
            this.cAMBIARCONTRASEÑAToolStripMenuItem.Size = new System.Drawing.Size(166, 20);
            this.cAMBIARCONTRASEÑAToolStripMenuItem.Text = "CAMBIAR CONTRASEÑA";
            // 
            // iNICIOToolStripMenuItem
            // 
            this.iNICIOToolStripMenuItem.Name = "iNICIOToolStripMenuItem";
            this.iNICIOToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.iNICIOToolStripMenuItem.Text = "INICIO";
            // 
            // rEGToolStripMenuItem
            // 
            this.rEGToolStripMenuItem.Name = "rEGToolStripMenuItem";
            this.rEGToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.rEGToolStripMenuItem.Text = "INGRESAR CON OTRO USUARIO";
            // 
            // sALIRToolStripMenuItem1
            // 
            this.sALIRToolStripMenuItem1.Name = "sALIRToolStripMenuItem1";
            this.sALIRToolStripMenuItem1.Size = new System.Drawing.Size(242, 22);
            this.sALIRToolStripMenuItem1.Text = "SALIR";
            // 
            // aYUDAToolStripMenuItem1
            // 
            this.aYUDAToolStripMenuItem1.Name = "aYUDAToolStripMenuItem1";
            this.aYUDAToolStripMenuItem1.Size = new System.Drawing.Size(57, 20);
            this.aYUDAToolStripMenuItem1.Text = "AYUDA";
            // 
            // aCERCADEToolStripMenuItem
            // 
            this.aCERCADEToolStripMenuItem.Name = "aCERCADEToolStripMenuItem";
            this.aCERCADEToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.aCERCADEToolStripMenuItem.Text = "ACERCA DE";
            // 
            // rEGISTROSToolStripMenuItem
            // 
            this.rEGISTROSToolStripMenuItem.Name = "rEGISTROSToolStripMenuItem";
            this.rEGISTROSToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.rEGISTROSToolStripMenuItem.Text = "REGISTROS";
            // 
            // mANTENIMIENTOSToolStripMenuItem
            // 
            this.mANTENIMIENTOSToolStripMenuItem.Name = "mANTENIMIENTOSToolStripMenuItem";
            this.mANTENIMIENTOSToolStripMenuItem.Size = new System.Drawing.Size(120, 20);
            this.mANTENIMIENTOSToolStripMenuItem.Text = "MANTENIMIENTOS";
            // 
            // rEPORTESToolStripMenuItem
            // 
            this.rEPORTESToolStripMenuItem.Name = "rEPORTESToolStripMenuItem";
            this.rEPORTESToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.rEPORTESToolStripMenuItem.Text = "REPORTES";
            // 
            // iNICIOToolStripMenuItem2
            // 
            this.iNICIOToolStripMenuItem2.Name = "iNICIOToolStripMenuItem2";
            this.iNICIOToolStripMenuItem2.Size = new System.Drawing.Size(94, 20);
            this.iNICIOToolStripMenuItem2.Text = "INICIO";
            // 
            // iNGRESARCONOTROUSUARIOToolStripMenuItem1
            // 
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem1.Name = "iNGRESARCONOTROUSUARIOToolStripMenuItem1";
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem1.Size = new System.Drawing.Size(245, 22);
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem1.Text = "INGRESAR CON  OTRO USUARIO";
            // 
            // sALIRToolStripMenuItem3
            // 
            this.sALIRToolStripMenuItem3.Name = "sALIRToolStripMenuItem3";
            this.sALIRToolStripMenuItem3.Size = new System.Drawing.Size(245, 22);
            this.sALIRToolStripMenuItem3.Text = "SALIR";
            // 
            // iNICIOToolStripMenuItem3
            // 
            this.iNICIOToolStripMenuItem3.Name = "iNICIOToolStripMenuItem3";
            this.iNICIOToolStripMenuItem3.Size = new System.Drawing.Size(54, 20);
            this.iNICIOToolStripMenuItem3.Text = "INICIO";
            // 
            // iNGRESARCONOTROUSUARIOToolStripMenuItem3
            // 
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem3.Name = "iNGRESARCONOTROUSUARIOToolStripMenuItem3";
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem3.Size = new System.Drawing.Size(242, 22);
            this.iNGRESARCONOTROUSUARIOToolStripMenuItem3.Text = "INGRESAR CON OTRO USUARIO";
            // 
            // sALIRToolStripMenuItem4
            // 
            this.sALIRToolStripMenuItem4.Name = "sALIRToolStripMenuItem4";
            this.sALIRToolStripMenuItem4.Size = new System.Drawing.Size(242, 22);
            this.sALIRToolStripMenuItem4.Text = "SALIR";
            // 
            // lblLoggueado
            // 
            this.lblLoggueado.AutoSize = true;
            this.lblLoggueado.BackColor = System.Drawing.Color.Transparent;
            this.lblLoggueado.Font = new System.Drawing.Font("Baskerville Old Face", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoggueado.Location = new System.Drawing.Point(13, 70);
            this.lblLoggueado.Name = "lblLoggueado";
            this.lblLoggueado.Size = new System.Drawing.Size(16, 22);
            this.lblLoggueado.TabIndex = 25;
            this.lblLoggueado.Text = "l";
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(708, 601);
            this.Controls.Add(this.lblLoggueado);
            this.Controls.Add(this.lblHora);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(726, 648);
            this.MinimumSize = new System.Drawing.Size(726, 648);
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MENU PRINCIPAL PCTR";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmCotizaciones;
        private System.Windows.Forms.ToolStripMenuItem iNGRESARCONOTROUSUARIOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sALIRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmMantenimientos;
        private System.Windows.Forms.ToolStripMenuItem tsmReportes;
        private System.Windows.Forms.ToolStripMenuItem tsmAcercade;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem aYUDAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tIPODESERVICIOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eNDEREZADOYPINTURAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tRABAJOSFIBRADEVIDRIOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pULIDODECARROCERIAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uSUARIOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rOLESYPERMISOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cLIENTESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rEPARACIONESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iNGRESOSYSALIDASToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mOVIMIENTOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cOTIZACIONESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cLIENTESToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vEHICULOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iNICIOToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem iNGRESARCONOTROUSUARIOToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem sALIRToolStripMenuItem2;
        private System.Windows.Forms.Timer timHoraFecha;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.ToolStripMenuItem tIPODESERVICIOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eNDEREZADOYPINTURAToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tRABAJOSFIBRADEVIDRIOToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pULIDODECARROCERIAToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cAMBIARCONTRASEÑAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rEGISTROSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mANTENIMIENTOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rEPORTESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iNICIOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rEGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sALIRToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aYUDAToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aCERCADEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iNICIOToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem iNGRESARCONOTROUSUARIOToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sALIRToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem iNICIOToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem iNGRESARCONOTROUSUARIOToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem sALIRToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem iNICIOToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem oTROUSUARIOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sALIRToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem aYUDAToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem aCERCADEToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rEGISTROToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cOTIZACIONToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eNDEREZADOYPINTURAToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mANTENIMIENTOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cLIENTEToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rEPORTEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rOLYPERMISOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uSUARIOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pRODUCTOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pIEZASToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sERVICIOSToolStripMenuItem;
        private System.Windows.Forms.Label lblLoggueado;
    }
}

