﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista.Reportes.Cotizaciones
{
    public partial class CotizacionesReportes : Form
    {
        private string tipoDato = "";
        private string valor = "";
        public CotizacionesReportes(string tipoDato, string valor)
        {
            InitializeComponent();
            this.tipoDato = tipoDato;
            this.valor = valor;
        }

        private void CotizacionesReportes_Load(object sender, EventArgs e)
        {
            switch (tipoDato)
            {
                case "Filtrar por usuario":
                    this.sP_SeleccionarCotizacionesReportTableAdapter.Fill(this.dsReportes.SP_SeleccionarCotizacionesReport, valor, null, null);
                    break;
                case "Filtrar por fecha":
                    valor = valor.Replace('/', '-'); 
                    this.sP_SeleccionarCotizacionesReportTableAdapter.Fill(this.dsReportes.SP_SeleccionarCotizacionesReport, null, null, valor);
                    break;
                case "Filtrar por clientes":
                    this.sP_SeleccionarCotizacionesReportTableAdapter.Fill(this.dsReportes.SP_SeleccionarCotizacionesReport, null, valor, null);
                    break;
                default:
                    break;
            }

            this.reportViewer1.RefreshReport();
        }
    }
}
