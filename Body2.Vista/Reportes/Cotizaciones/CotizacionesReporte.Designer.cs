﻿namespace Body2.Vista.Reportes.Cotizaciones
{
    partial class CotizacionesReporte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dSCotizaciones = new Body2.Vista.Reportes.Cotizaciones.DSCotizaciones();
            this.dSCotizacionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dSCotizaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSCotizacionesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.dSCotizacionesBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Body2.Vista.Reportes.Cotizaciones.RptCotizaciones.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(396, 246);
            this.reportViewer1.TabIndex = 0;
            // 
            // dSCotizaciones
            // 
            this.dSCotizaciones.DataSetName = "DSCotizaciones";
            this.dSCotizaciones.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dSCotizacionesBindingSource
            // 
            this.dSCotizacionesBindingSource.DataSource = this.dSCotizaciones;
            this.dSCotizacionesBindingSource.Position = 0;
            // 
            // CotizacionesReporte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1342, 659);
            this.Controls.Add(this.reportViewer1);
            this.Name = "CotizacionesReporte";
            this.Text = "CotizacionesReporte";
            this.Load += new System.EventHandler(this.CotizacionesReporte_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dSCotizaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSCotizacionesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource dSCotizacionesBindingSource;
        private DSCotizaciones dSCotizaciones;
    }
}