﻿using System;
using Body2.Vista.Reportes.DsReportesTableAdapters;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista.Reportes.EntradaSalida
{
    public partial class LogInOutReportes : FormGeneral
    {
        private string tipoDato = "";
        private string valor = "";
        public LogInOutReportes(string tipoDato, string valor)
        {
            InitializeComponent();
            this.tipoDato = tipoDato;
            this.valor = valor;
        }

        private void LogInOutReportes_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'dsReportes.SP_SeleccionarIngresoSalida' Puede moverla o quitarla según sea necesario.
            switch (tipoDato)
            {
                case "Filtrar por fecha":
                    valor = valor.Replace('/', '-');
                    this.sP_SeleccionarIngresoSalidaTableAdapter.Fill(this.dsReportes.SP_SeleccionarIngresoSalida, null, valor);
                    break;
                case "Filtrar por usuario":
                    this.sP_SeleccionarIngresoSalidaTableAdapter.Fill(this.dsReportes.SP_SeleccionarIngresoSalida, valor, null);
                    break;
                default:
                    break;
            }

            this.reportViewer1.RefreshReport();
            this.reportViewer1.RefreshReport();
        }
    }
}
