﻿namespace Body2.Vista.Reportes.EntradaSalida
{
    partial class LogInOutReportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogInOutReportes));
            this.sPSeleccionarIngresoSalidaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsReportes = new Body2.Vista.Reportes.DsReportes();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.sP_SeleccionarIngresoSalidaTableAdapter = new Body2.Vista.Reportes.DsReportesTableAdapters.SP_SeleccionarIngresoSalidaTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.sPSeleccionarIngresoSalidaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsReportes)).BeginInit();
            this.SuspendLayout();
            // 
            // sPSeleccionarIngresoSalidaBindingSource
            // 
            this.sPSeleccionarIngresoSalidaBindingSource.DataMember = "SP_SeleccionarIngresoSalida";
            this.sPSeleccionarIngresoSalidaBindingSource.DataSource = this.dsReportes;
            // 
            // dsReportes
            // 
            this.dsReportes.DataSetName = "DsReportes";
            this.dsReportes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DSLogInOut";
            reportDataSource1.Value = this.sPSeleccionarIngresoSalidaBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Body2.Vista.Reportes.EntradaSalida.RptLogInOut.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(904, 574);
            this.reportViewer1.TabIndex = 0;
            // 
            // sP_SeleccionarIngresoSalidaTableAdapter
            // 
            this.sP_SeleccionarIngresoSalidaTableAdapter.ClearBeforeFill = true;
            // 
            // LogInOutReportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 574);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "LogInOutReportes";
            this.Text = "LogInOutReportes";
            this.Load += new System.EventHandler(this.LogInOutReportes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sPSeleccionarIngresoSalidaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsReportes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private DsReportes dsReportes;
        private System.Windows.Forms.BindingSource sPSeleccionarIngresoSalidaBindingSource;
        private DsReportesTableAdapters.SP_SeleccionarIngresoSalidaTableAdapter sP_SeleccionarIngresoSalidaTableAdapter;
    }
}