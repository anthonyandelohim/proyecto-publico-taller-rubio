﻿namespace Body2.Vista.Reportes
{
    partial class Mantenimiento_Reportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mantenimiento_Reportes));
            this.label1 = new System.Windows.Forms.Label();
            this.plReportes = new System.Windows.Forms.Panel();
            this.txtMaximo = new System.Windows.Forms.TextBox();
            this.txtMinimo = new System.Windows.Forms.TextBox();
            this.lblMaximo = new System.Windows.Forms.Label();
            this.lblMinimo = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.cmbOpciones = new System.Windows.Forms.ComboBox();
            this.lbOpciones = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbTipoDatos = new System.Windows.Forms.ComboBox();
            this.cmbTipoReporte = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.plReportes.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(244, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "REPORTES";
            // 
            // plReportes
            // 
            this.plReportes.Controls.Add(this.txtMaximo);
            this.plReportes.Controls.Add(this.txtMinimo);
            this.plReportes.Controls.Add(this.lblMaximo);
            this.plReportes.Controls.Add(this.lblMinimo);
            this.plReportes.Controls.Add(this.dtpFecha);
            this.plReportes.Controls.Add(this.cmbOpciones);
            this.plReportes.Controls.Add(this.lbOpciones);
            this.plReportes.Controls.Add(this.panel2);
            this.plReportes.Controls.Add(this.cmbTipoDatos);
            this.plReportes.Controls.Add(this.cmbTipoReporte);
            this.plReportes.Controls.Add(this.label3);
            this.plReportes.Controls.Add(this.label2);
            this.plReportes.Location = new System.Drawing.Point(7, 81);
            this.plReportes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plReportes.Name = "plReportes";
            this.plReportes.Size = new System.Drawing.Size(598, 354);
            this.plReportes.TabIndex = 1;
            // 
            // txtMaximo
            // 
            this.txtMaximo.Location = new System.Drawing.Point(332, 125);
            this.txtMaximo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtMaximo.Name = "txtMaximo";
            this.txtMaximo.Size = new System.Drawing.Size(76, 20);
            this.txtMaximo.TabIndex = 12;
            this.txtMaximo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            // 
            // txtMinimo
            // 
            this.txtMinimo.Location = new System.Drawing.Point(241, 125);
            this.txtMinimo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtMinimo.Name = "txtMinimo";
            this.txtMinimo.Size = new System.Drawing.Size(76, 20);
            this.txtMinimo.TabIndex = 11;
            this.txtMinimo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // lblMaximo
            // 
            this.lblMaximo.AutoSize = true;
            this.lblMaximo.BackColor = System.Drawing.Color.Transparent;
            this.lblMaximo.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblMaximo.Location = new System.Drawing.Point(350, 109);
            this.lblMaximo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMaximo.Name = "lblMaximo";
            this.lblMaximo.Size = new System.Drawing.Size(35, 13);
            this.lblMaximo.TabIndex = 10;
            this.lblMaximo.Text = "Hasta";
            // 
            // lblMinimo
            // 
            this.lblMinimo.AutoSize = true;
            this.lblMinimo.BackColor = System.Drawing.Color.Transparent;
            this.lblMinimo.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblMinimo.Location = new System.Drawing.Point(258, 109);
            this.lblMinimo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMinimo.Name = "lblMinimo";
            this.lblMinimo.Size = new System.Drawing.Size(38, 13);
            this.lblMinimo.TabIndex = 9;
            this.lblMinimo.Text = "Desde";
            // 
            // dtpFecha
            // 
            this.dtpFecha.Location = new System.Drawing.Point(241, 135);
            this.dtpFecha.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(151, 20);
            this.dtpFecha.TabIndex = 2;
            this.dtpFecha.Visible = false;
            // 
            // cmbOpciones
            // 
            this.cmbOpciones.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOpciones.FormattingEnabled = true;
            this.cmbOpciones.Location = new System.Drawing.Point(241, 125);
            this.cmbOpciones.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbOpciones.Name = "cmbOpciones";
            this.cmbOpciones.Size = new System.Drawing.Size(312, 21);
            this.cmbOpciones.TabIndex = 7;
            this.cmbOpciones.SelectedIndexChanged += new System.EventHandler(this.cmbOpciones_SelectedIndexChanged);
            // 
            // lbOpciones
            // 
            this.lbOpciones.AutoSize = true;
            this.lbOpciones.BackColor = System.Drawing.Color.Transparent;
            this.lbOpciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOpciones.ForeColor = System.Drawing.Color.Gainsboro;
            this.lbOpciones.Location = new System.Drawing.Point(19, 126);
            this.lbOpciones.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbOpciones.Name = "lbOpciones";
            this.lbOpciones.Size = new System.Drawing.Size(158, 15);
            this.lbOpciones.TabIndex = 6;
            this.lbOpciones.Text = "Elige la opción que deseas:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Location = new System.Drawing.Point(220, 258);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(331, 81);
            this.panel2.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.Location = new System.Drawing.Point(185, 28);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 33);
            this.button2.TabIndex = 1;
            this.button2.Text = "Cancelar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(38, 28);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 33);
            this.button1.TabIndex = 0;
            this.button1.Text = "Generar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbTipoDatos
            // 
            this.cmbTipoDatos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoDatos.FormattingEnabled = true;
            this.cmbTipoDatos.Location = new System.Drawing.Point(241, 72);
            this.cmbTipoDatos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbTipoDatos.Name = "cmbTipoDatos";
            this.cmbTipoDatos.Size = new System.Drawing.Size(312, 21);
            this.cmbTipoDatos.TabIndex = 4;
            this.cmbTipoDatos.SelectedIndexChanged += new System.EventHandler(this.cmbTipoDatos_SelectedIndexChanged);
            // 
            // cmbTipoReporte
            // 
            this.cmbTipoReporte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoReporte.FormattingEnabled = true;
            this.cmbTipoReporte.Location = new System.Drawing.Point(241, 24);
            this.cmbTipoReporte.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbTipoReporte.Name = "cmbTipoReporte";
            this.cmbTipoReporte.Size = new System.Drawing.Size(312, 21);
            this.cmbTipoReporte.TabIndex = 3;
            this.cmbTipoReporte.SelectedIndexChanged += new System.EventHandler(this.cmbTipoReporte_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gainsboro;
            this.label3.Location = new System.Drawing.Point(19, 73);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(203, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Elige los datos que deseas generar:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gainsboro;
            this.label2.Location = new System.Drawing.Point(19, 24);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Elige el tipo de reporte a generar: ";
            // 
            // Mantenimiento_Reportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(616, 496);
            this.Controls.Add(this.plReportes);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(632, 535);
            this.MinimumSize = new System.Drawing.Size(632, 535);
            this.Name = "Mantenimiento_Reportes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mantenimiento_Reportes";
            this.Load += new System.EventHandler(this.Mantenimiento_Reportes_Load);
            this.plReportes.ResumeLayout(false);
            this.plReportes.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel plReportes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cmbTipoDatos;
        private System.Windows.Forms.ComboBox cmbTipoReporte;
        private System.Windows.Forms.ComboBox cmbOpciones;
        private System.Windows.Forms.Label lbOpciones;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.TextBox txtMaximo;
        private System.Windows.Forms.TextBox txtMinimo;
        private System.Windows.Forms.Label lblMaximo;
        private System.Windows.Forms.Label lblMinimo;
    }
}