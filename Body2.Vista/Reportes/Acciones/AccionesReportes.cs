﻿using Body2.Vista.Reportes.DsReportesTableAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista.Reportes.Acciones
{
    public partial class AccionesReportes : FormGeneral
    {
        private string tipoDato = "";
        private string valor = "";
        public AccionesReportes(string tipoDato, string valor)
        {
            InitializeComponent();
            this.tipoDato = tipoDato;
            this.valor = valor;
        }

        private void AccionesReportes_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'dsReportes.SeleccionarAcciones' Puede moverla o quitarla según sea necesario.
            switch (tipoDato)
            {
                case "Filtrar por usuario":
                    this.seleccionarAccionesTableAdapter.Fill(this.dsReportes.SeleccionarAcciones, valor, null, null);
                    break;
                case "Filtrar por fecha":
                    valor = valor.Replace('/', '-');
                    this.seleccionarAccionesTableAdapter.Fill(this.dsReportes.SeleccionarAcciones, null, valor, null);
                    break;
                case "Filtrar por pantalla":
                    this.seleccionarAccionesTableAdapter.Fill(this.dsReportes.SeleccionarAcciones, null, null, Convert.ToInt32(valor));
                    break;
                default:
                    break;
            }
            
            // TODO: esta línea de código carga datos en la tabla 'dsReportes.SeleccionarAcciones' Puede moverla o quitarla según sea necesario.
            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            
        }
    }
}
