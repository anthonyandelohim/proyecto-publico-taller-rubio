﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista.Reportes.EntradaSalida
{
    public partial class IngresoSalidaEx : Form
    {
        public IngresoSalidaEx()
        {
            InitializeComponent();
        }

        private void IngresoSalidaEx_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'dsReportes.SP_SeleccionarIngresoSalidaError' Puede moverla o quitarla según sea necesario.
            this.sP_SeleccionarIngresoSalidaErrorTableAdapter.Fill(this.dsReportes.SP_SeleccionarIngresoSalidaError);

            this.reportViewer1.RefreshReport();
        }
    }
}
