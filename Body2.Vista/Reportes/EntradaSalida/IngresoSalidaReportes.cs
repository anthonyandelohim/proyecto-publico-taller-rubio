﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista.Reportes.EntradaSalida
{
    public partial class IngresoSalidaReportes : Form
    {
        public IngresoSalidaReportes()
        {
            InitializeComponent();
        }

        private void IngresoSalidaErrorReportes_Load(object sender, EventArgs e)
        {
            this.sP_SeleccionarIngresoSalidaTableAdapter.Fill(this.dsReportes.SP_SeleccionarIngresoSalida, "error", null);
            this.reportViewer1.RefreshReport();
        }
    }
}
