﻿namespace Body2.Vista.Reportes.EntradaSalida
{
    partial class IngresoSalidaEx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IngresoSalidaEx));
            this.sPSeleccionarIngresoSalidaErrorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsReportes = new Body2.Vista.Reportes.DsReportes();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.sP_SeleccionarIngresoSalidaErrorTableAdapter = new Body2.Vista.Reportes.DsReportesTableAdapters.SP_SeleccionarIngresoSalidaErrorTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.sPSeleccionarIngresoSalidaErrorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsReportes)).BeginInit();
            this.SuspendLayout();
            // 
            // sPSeleccionarIngresoSalidaErrorBindingSource
            // 
            this.sPSeleccionarIngresoSalidaErrorBindingSource.DataMember = "SP_SeleccionarIngresoSalidaError";
            this.sPSeleccionarIngresoSalidaErrorBindingSource.DataSource = this.dsReportes;
            // 
            // dsReportes
            // 
            this.dsReportes.DataSetName = "DsReportes";
            this.dsReportes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.sPSeleccionarIngresoSalidaErrorBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Body2.Vista.Reportes.EntradaSalida.RptIngresoSalidaEx.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Margin = new System.Windows.Forms.Padding(2);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(730, 466);
            this.reportViewer1.TabIndex = 0;
            // 
            // sP_SeleccionarIngresoSalidaErrorTableAdapter
            // 
            this.sP_SeleccionarIngresoSalidaErrorTableAdapter.ClearBeforeFill = true;
            // 
            // IngresoSalidaEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 466);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "IngresoSalidaEx";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IngresoSalidaEx";
            this.Load += new System.EventHandler(this.IngresoSalidaEx_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sPSeleccionarIngresoSalidaErrorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsReportes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private DsReportes dsReportes;
        private System.Windows.Forms.BindingSource sPSeleccionarIngresoSalidaErrorBindingSource;
        private DsReportesTableAdapters.SP_SeleccionarIngresoSalidaErrorTableAdapter sP_SeleccionarIngresoSalidaErrorTableAdapter;
    }
}