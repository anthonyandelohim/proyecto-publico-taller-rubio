﻿using Body2.Controladora;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Body2.Vista.Reportes.Cotizaciones
{
    public class CotizacionesTransformacion
    {
        public string ProductosTransform(string productosJson)
        {
            string productos = "[{idProducto:AUT01,NombreProducto:Audi,PrecioProducto:50000.00,Color:Blue,Marca:Audi,Placa:ADI677,Cliente:102340567}]";
            if (productos != "")
            {
                string jsonTransform = productosJson;
                productos = productos.Replace("[{idProducto:", "Producto");
                productos = productos.Replace("Producto:", String.Empty);
                productos = productos.Replace("{:", String.Empty);
                productos = productos.Replace("}:", String.Empty);
                productos = productos.Replace("{", String.Empty);
                productos = productos.Replace("}", String.Empty);
                productos = productos.Replace("}]", String.Empty);
                productos = productos.Replace("]", String.Empty);
                productos = productos.Replace(":", "=>");
                
            }
            return productos;

        }
        public string PiezasTransform(string productosJson)
        {
            string productos = "[{idPieza:Mtr800,NombrePieza:Motor Volvo}]";
            string jsonTransform = productosJson;
            productos = productos.Replace("[{idPieza:", String.Empty);
            productos = productos.Replace("idPieza:", String.Empty);
            productos = productos.Replace("NombrePieza:", String.Empty);
            productos = productos.Replace("{:", String.Empty);
            productos = productos.Replace("}:", String.Empty);
            productos = productos.Replace("{", String.Empty);
            productos = productos.Replace("}", String.Empty);
            productos = productos.Replace("}]", String.Empty);
            productos = productos.Replace("]", String.Empty);
            productos = productos.Replace('"', ' ');
            return productos;
        }
    }
}
