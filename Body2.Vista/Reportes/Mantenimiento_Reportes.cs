﻿using Body2.Controladora;
using Body2.Vista.Reportes.Acciones;
using Body2.Vista.Reportes.Cotizaciones;
using Body2.Vista.Reportes.EntradaSalida;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista.Reportes
{
    public partial class Mantenimiento_Reportes : FormGeneral
    {

        List<string> tiposReportes = new List<string>();
        List<string> tiposDatos = new List<string>(); 
        ReportesController accionReportes;
        private string idUsuario = "";
        private string valor = "";
        public Mantenimiento_Reportes(string idUsuario)
        {
            InitializeComponent();
            plReportes.BackColor = Color.FromArgb(125, Color.Black);
            this.idUsuario = idUsuario;
        }

        private void Mantenimiento_Reportes_Load(object sender, EventArgs e)
        {
            lbOpciones.Visible = false;
            OcultaInputsLoad();
            LlenarReportes();

            Estilo_AgregarBoton(button1);
            Estilo_CancelarBoton(button2);
        }
        private void seleccion(string seleccion)
        {
            List<string> lista = new List<string>();
            if (seleccion.Equals("Acciones"))
            {
                lista = LlenarAcciones();
            }
            else if (seleccion.Equals("Ingreso/Salida"))
            {
                lista = LlenarEntradaSalida();
            }
            else if(seleccion.Equals("Cotizaciones"))
            {
                lista = LlenarCotizaciones();
            }
            else if (seleccion.Equals("Productos"))
            {
                lista = LlenarProducto();
            }
            cmbTipoDatos.DataSource = lista;
        }
        private void LlenarReportes()
        {
            tiposReportes.Add("-- Selecciona una opción --");
            tiposReportes.Add("Acciones");
            tiposReportes.Add("Cotizaciones");
            tiposReportes.Add("Ingreso/Salida");
            //tiposReportes.Add("Productos");

            cmbTipoReporte.DataSource = tiposReportes;
        }
        private List<string> LlenarAcciones()
        {
            tiposDatos.Add("-- Selecciona una opción --");
            tiposDatos.Add("Filtrar por fecha");
            tiposDatos.Add("Filtrar por usuario");
            tiposDatos.Add("Filtrar por pantalla");

            return tiposDatos;
        }
        private List<string> LlenarEntradaSalida()
        {
            tiposDatos.Add("-- Selecciona una opción --");
            tiposDatos.Add("Filtrar por fecha");
            tiposDatos.Add("Filtrar por usuario");
            tiposDatos.Add("Filtrar por errores de ingreso");

            return tiposDatos;
        }

        private List<string> LlenarCotizaciones()
        {
            tiposDatos.Add("-- Selecciona una opción --");
            tiposDatos.Add("Filtrar por fecha");
            //tiposDatos.Add("Filtrar por montos");
            tiposDatos.Add("Filtrar por clientes");
            tiposDatos.Add("Filtrar por usuario");
            return tiposDatos;
        }
        private List<string> LlenarProducto()
        {
            tiposDatos.Add("-- Selecciona una opción --");
            tiposDatos.Add("Filtrar por clientes/productos");
            tiposDatos.Add("Filtrar por productos system");
            return tiposDatos;
        }
        private void cmbTipoReporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            tiposDatos = new List<string>();
            seleccion(cmbTipoReporte.SelectedItem.ToString());
            cmbOpciones.Visible = false;
            lbOpciones.Visible = false;
            dtpFecha.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string reporte = cmbTipoReporte.SelectedItem.ToString();
                if (!cmbTipoDatos.SelectedItem.ToString().Equals("Filtrar por errores de ingreso"))
                {
                    if (cmbTipoDatos.SelectedItem == null || reporte.Equals("-- Selecciona una opción --") || (valor.Equals("-- Selecciona una opción --") && cmbOpciones.Visible))
                    {
                        MessageBox.Show("Por favor elige alguna opción para el reporte", "Sin datos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    valor = "Errores de ingreso";
                }
                
                string tipo = cmbTipoDatos.SelectedItem.ToString();
                if (dtpFecha.Visible)
                {
                    valor = dtpFecha.Value.ToString("yyyy/MM/dd");
                }
                if (valor == "")
                {
                    MessageBox.Show("Por favor elige alguna opción para el reporte", "Sin datos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                
                switch (reporte)
                {
                    case "Acciones":
                        AccionesReportes acciones = new AccionesReportes(tipo, valor);
                        acciones.ShowDialog();
                        break;
                    case "Ingreso/Salida":
                        if (valor.Equals("Errores de ingreso"))
                        {
                            IngresoSalidaEx logInOutError = new IngresoSalidaEx();
                            logInOutError.ShowDialog();
                        }
                        else
                        {
                            LogInOutReportes logInOut = new LogInOutReportes(tipo, valor);
                            logInOut.ShowDialog();
                        }
                        
                        break;
                    case "Cotizaciones":
                        CotizacionesReportes cotizaciones = new CotizacionesReportes(tipo, valor);
                        cotizaciones.ShowDialog();
                        break;
                    default:
                        break;
                }
                valor = "";
                accionReportes = new ReportesController(idUsuario, "Generando reporte tipo " + tipo + " usando el valor(es) de " + valor);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Sin datos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        private void ListarComboBoxOpciones(string param)
        {
            switch (param)
            {
                case "Filtrar por usuario":
                    cmbOpciones.Visible = true;
                    UsuarioHelper usuario = new UsuarioHelper(idUsuario);
                    List<Usuario> lista = new List<Usuario>();
                    lista = usuario.ListarUsuario();
                    lista.Add(new Usuario
                    {
                        Nombre = "-- Selecciona una opción --",
                        User = "-- Selecciona una opción --"
                    });
                    lista = lista.OrderBy(x => x.User).ToList();
                    cmbOpciones.DataSource = lista;
                    cmbOpciones.DisplayMember = "Nombre";
                    cmbOpciones.ValueMember = "User";
                    break;
                case "Filtrar por fecha":
                    dtpFecha.Visible = true;
                    break;
                case "Filtrar por clientes":
                    cmbOpciones.Visible = true;
                    lbOpciones.Visible = true;
                    ClienteHelper cliente = new ClienteHelper(idUsuario);
                    List<Cliente> listaCliente = new List<Cliente>();
                    listaCliente = cliente.ListarCliente();
                    listaCliente.Add(new Cliente { 
                        Nombre = "-- Selecciona una opción --",
                        Cedula = "00000"
                    });
                    listaCliente = listaCliente.OrderBy(x => x.Cedula).ToList();
                    cmbOpciones.DataSource = listaCliente;
                    cmbOpciones.DisplayMember = "Nombre";
                    cmbOpciones.ValueMember = "Cedula";
                    break;
                case "Filtrar por pantalla":
                    cmbOpciones.Visible = true;
                    lbOpciones.Visible = true;
                    PermisoHelper pantalla = new PermisoHelper();
                    List<Pantalla> listaPantalla = new List<Pantalla>();
                    listaPantalla = pantalla.ListaPantallas();
                    listaPantalla.Add(new Pantalla
                    {
                        idPantalla = 10000,
                        NombrePantalla = "-- Selecciona una opción --"
                    });
                    listaPantalla = listaPantalla.OrderByDescending(x => x.idPantalla).ToList();
                    cmbOpciones.DataSource = listaPantalla;
                    cmbOpciones.DisplayMember = "NombrePantalla";
                    cmbOpciones.ValueMember = "idPantalla";
                    break;
                case "Filtrar por montos":
                    txtMaximo.Visible = true;
                    txtMinimo.Visible = true; 
                    lblMaximo.Visible = true;
                    lblMinimo.Visible = true;
                    break;
                case "Filtrar por errores de ingreso":
                    lbOpciones.Visible = false;
                    break;
                default:
                    break;
            }
        }
        private void OcultaInputsLoad()
        {
            cmbOpciones.Visible = false;
            dtpFecha.Visible = false;
            txtMaximo.Visible = false;
            txtMinimo.Visible = false;
            lblMaximo.Visible = false;
            lblMinimo.Visible = false;
        }
        private void cmbTipoDatos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OcultaInputsLoad();
                lbOpciones.Visible = true;
                ComboBox thisCmb = (ComboBox)sender;
                if (thisCmb.SelectedItem != null)
                {
                    ListarComboBoxOpciones(thisCmb.SelectedItem.ToString());
                }
                
            }
            catch (Exception ex)
            {

            }
            
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPress_SoloNumeros(sender, e);
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPress_SoloNumeros(sender, e);
        }

        private void cmbOpciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox thisCombo = (ComboBox)sender;
            valor = thisCombo.SelectedValue.ToString();

            if (valor.Contains("Controladora"))
            {
                valor = "-- Selecciona una opción --";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("¿Deseas salir de la pantalla de reportes?", "Salida", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
                this.Close();
        }
    }
}
