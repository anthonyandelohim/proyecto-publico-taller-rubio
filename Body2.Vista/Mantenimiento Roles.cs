﻿using Body2.Controladora;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista
{
    public partial class Mantenimiento_Roles : FormGeneral
    {
        PermisoHelper permisos = new PermisoHelper();
        List<Rol> listaRoles = new List<Rol>();
        RolHelper roles;
        public Mantenimiento_Roles(string idUsuario)
        {
            InitializeComponent();
            roles = new RolHelper(idUsuario);
        }

        private void Mantenimiento_Roles_Load(object sender, EventArgs e)
        {
            ListarPantallas();
            ListarRoles();

            Estilo_AgregarBoton(btnAgregar);
            Estilo_EliminarBoton(btnEliminar);
            Estilo_ModificarBoton(btnActualizar);
            Estilo_LimpiarBoton(button1);
        }
        private void ListarPantallas()
        {
            List<Pantalla> pantallas = new List<Pantalla>();
            pantallas = permisos.ListaPantallas();
            if (clbPantallas.Items.Count > 0)
            {
                clbPantallas.Items.Clear();
            }
            
            clbPantallas.DataSource = pantallas;
            clbPantallas.DisplayMember = "NombrePantalla";
            clbPantallas.ValueMember = "idPantalla";
        }
        private bool Validaciones()
        {
            List<Control> controls = new List<Control>();
            controls.Add(txtDescripcion);
            controls.Add(txtNombre);
            controls.Add(cmbEstado);
            if (!ValidacionInputs(controls))
            {
                MessageBox.Show("Por favor llena toda la información requerida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            //estado 1 habilitado - estado 0 deshabilitado
            string estadoRol = "1";
            if (cmbEstado.Text.Equals("Rol Deshabilitado"))
            {
                estadoRol = "0";
            }

            List<Pantalla> idPantallas = new List<Pantalla>();
            foreach (var p in clbPantallas.CheckedItems)
            {
                var row = (Pantalla)p;
                idPantallas.Add(row);
            }

            //toma la lista de todos las pantallas asociadas a este rol y los convierte en un formato Json
            //esto me ayuda a ahorrar muchisimo espacio en la base de datos y mejora el rendimiento, en lugar de tener muchas tablas y registros relacionados
            //la relacion en lugar de hacerse en la base de datos, se hace mediante objetos Json en el codigo
            string pantallasJson = JsonConvert.SerializeObject(idPantallas);

            Rol prod = new Rol
            {
                NombreRol = txtNombre.Text.Trim(),
                Estado = estadoRol,
                Descripcion = txtDescripcion.Text,
                Pantallas = pantallasJson
            };
            string resultado = roles.AgregarRol(prod);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Rol agregado exitosamente", "Agregado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbRoles, null);
            ListarRoles();
        }
        private void ListarRoles()
        {
            
            listaRoles = roles.ListarRol();
            dgvRolesPermisos.DataSource = listaRoles;

            //cambia el 1 - 0 por rol habilitado - rol deshabilitado
            if (listaRoles == null)
            {
                return;
            }
            foreach (var u in listaRoles)
            {
                if (u.Estado.Equals("1"))
                {
                    u.Estado = "Rol Habilitado";
                }
                else
                {
                    u.Estado = "Rol Deshabilitado";
                }
            }

            List<string> controlesNombre = new List<string>();
            controlesNombre.Add("idRol");
            OcultarCeldasDataGrid(dgvRolesPermisos, controlesNombre);
        }

        private void dgvRolesPermisos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            for (int i = 0; i < clbPantallas.Items.Count; i++)
            {
                clbPantallas.SetItemChecked(i, false);
            }
            //LlenarInputsDoubleClick(sender, e);

            LimpiarInputs(grbRoles, txtNombre);
            Dictionary<Control, int> textBoxes = new Dictionary<Control, int>();
            textBoxes.Add(txtNombre,1);
            textBoxes.Add(txtDescripcion,2);
            textBoxes.Add(cmbEstado,3);
            textBoxes.Add(clbPantallas,4);
            LlenarDataGrid(dgvRolesPermisos, sender, e, textBoxes);
            txtNombre.Enabled = false;
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            DialogResult dialog = MessageBox.Show("¿Realmente deseas eliminar este rol? Pueden existir muchos problemas", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grbRoles, txtNombre);
                return;
            }
            if (!RegistroExistente())
            {
                return;
            }
            string idRol = txtNombre.Text.Trim();
            List<Usuario> usuarios = new List<Usuario>();
            UsuarioHelper usuarioHelper = new UsuarioHelper("");
            usuarios = usuarioHelper.ListarUsuario();
            string resultado = roles.EliminarRol(idRol, usuarios);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Rol eliminado exitosamente", "Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbRoles, txtNombre);
            ListarRoles();
        }
        private bool RegistroExistente()
        {
            if (listaRoles.Where(x => x.NombreRol.Equals(txtNombre.Text)).FirstOrDefault() == null)
            {
                return false;
            }
            return true;
        }
        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            DialogResult dialog = MessageBox.Show("¿Deseas actualizar este rol?", "Actualización", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grbRoles, txtNombre);
                return;
            }
            if (!RegistroExistente())
            {
                return;
            }

            //estado 1 habilitado - estado 0 deshabilitado
            string estadoRol = "1";
            if (cmbEstado.Text.Equals("Rol Deshabilitado"))
            {
                estadoRol = "0";
            }

            List<Pantalla> idPantallas = new List<Pantalla>();
            foreach (var p in clbPantallas.CheckedItems)
            {
                var row = (Pantalla)p;
                idPantallas.Add(row);
            }

            //toma la lista de todos las pantallas asociadas a este rol y los convierte en un formato Json
            //esto me ayuda a ahorrar muchisimo espacio en la base de datos y mejora el rendimiento, en lugar de tener muchas tablas y registros relacionados
            //la relacion en lugar de hacerse en la base de datos, se hace mediante objetos Json en el codigo
            string pantallasJson = JsonConvert.SerializeObject(idPantallas);
            Rol prod = new Rol
            {
                NombreRol = txtNombre.Text.Trim(),
                Descripcion = txtDescripcion.Text.Trim(),
                Estado = estadoRol,
                Pantallas = pantallasJson
            };
            //tomo el idRol desde aqui
            List<Rol> nuevaListaRol = roles.ListarRol();

            string idRol = nuevaListaRol.Where(x => x.NombreRol == prod.NombreRol).Select(x => x.idRol).FirstOrDefault().ToString();
            prod.idRol = Convert.ToInt32(idRol);
            string resultado = roles.ActualizarRol(prod);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Rol actualizdo exitosamente", "Actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbRoles, txtNombre);
            for (int i = 0; i < clbPantallas.Items.Count; i++)
            {
                clbPantallas.SetItemChecked(i, false);
            }
            
            ListarRoles();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LimpiarInputs(grbRoles, txtNombre);
            for (int i = 0; i < clbPantallas.Items.Count; i++)
            {
                clbPantallas.SetItemChecked(i, false);
            }
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox meTxt = (TextBox)sender;
            string search = meTxt.Text.ToUpper();
            List<Rol> listaParcialBusqueda = new List<Rol>();
            List<Rol> listaParcialBusquedaFinal = new List<Rol>();
            if (meTxt.Text.Length == 0)
            {
                dgvRolesPermisos.DataSource = listaRoles;
            }
            if (listaRoles.Count > 0)
            {
                listaParcialBusquedaFinal = new List<Rol>();
                dgvRolesPermisos.DataSource = listaParcialBusquedaFinal;
                listaParcialBusqueda = listaRoles.Where(x => x.NombreRol.ToUpper().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaRoles.Where(x => x.idRol.ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaRoles.Where(x => x.Estado.ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusquedaFinal = listaParcialBusquedaFinal.Distinct().ToList();
                dgvRolesPermisos.DataSource = listaParcialBusquedaFinal;
            }
        }
    }
}
