﻿using Body2.Controladora;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista
{
    public partial class Mantenimiento_Servicios : FormGeneral
    {
        List<Servicios> listaServicios = new List<Servicios>();
        ServiciosHelper servicios;
        public Mantenimiento_Servicios(string idUsuario)
        {
            InitializeComponent();
            servicios = new ServiciosHelper(idUsuario);
        }

        private void Mantenimiento_Servicios_Load(object sender, EventArgs e)
        {
            ListarServicio();

            Estilo_AgregarBoton(btnAceptar);
            Estilo_LimpiarBoton(btnLimpiar);
            Estilo_EliminarBoton(btnEliminar);
            Estilo_ModificarBoton(btnModificar);
        }
        private void ListarServicio()
        {
            listaServicios = servicios.ListarServicios();
            dgvServicios.DataSource = listaServicios;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarInputs(grbControles, txtId);
        }
        private bool Validaciones()
        {
            List<Control> controls = new List<Control>();
            controls.Add(txtId);
            controls.Add(txtNombre);
            controls.Add(txtCosto);
            if (!ValidacionInputs(controls))
            {
                MessageBox.Show("Por favor llena toda la información requerida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (RegistroExistente())
            {
                return;
            }
            if (!Validaciones())
            {
                return;
            }
            Servicios prod = new Servicios
            {
                idServicio = txtId.Text.Trim(),
                Nombre = txtNombre.Text,
                CostoBase = Convert.ToDecimal(txtCosto.Text),
            };
            string resultado = servicios.AgregarServicios(prod);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Servicio agregado Exitosamente", "Agregado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbControles, null);
            ListarServicio();
        }

        private void txtCosto_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPress_SoloNumeros(sender, e);
        }

        private void dgvServicios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LimpiarInputs(grbControles, txtId);
            Dictionary<Control, int> textBoxes = new Dictionary<Control, int>();
            textBoxes.Add(txtId, 0);
            textBoxes.Add(txtNombre, 1);
            textBoxes.Add(txtCosto, 2);
            LlenarDataGrid(dgvServicios, sender, e, textBoxes);
            txtId.Enabled = false;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            if (!RegistroExistente())
            {
                return;
            }
            
            DialogResult dialog = MessageBox.Show("¿Deseas actualizar este servicio?", "Actualización", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grbControles, txtId);
                return;
            }
            Servicios prod = new Servicios
            {
                idServicio = txtId.Text.Trim(),
                Nombre = txtNombre.Text,
                CostoBase = Convert.ToDecimal(txtCosto.Text),
            };
            string resultado = servicios.ActualizarServicios(prod);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Servicio actualizado Exitosamente", "Actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbControles, txtId);
            ListarServicio();
        }
        private bool RegistroExistente()
        {
            if (listaServicios.Where(x => x.idServicio.Equals(txtId.Text)).FirstOrDefault() == null)
            {
                return false;
            }
            return true;
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            DialogResult dialog = MessageBox.Show("¿Realmente deseas eliminar este servicio? Pueden existir muchos problemas", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grbControles, txtId);
                return;
            }
            string idServicio = txtId.Text.Trim();
            string resultado = servicios.EliminarServicios(idServicio);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Servicio eliminado Exitosamente", "Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbControles, txtId);
            ListarServicio();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox meTxt = (TextBox)sender;
            string search = meTxt.Text.ToUpper();
            List<Servicios> listaParcialBusqueda = new List<Servicios>();
            List<Servicios> listaParcialBusquedaFinal = new List<Servicios>();
            if (meTxt.Text.Length == 0)
            {
                dgvServicios.DataSource = listaServicios;
            }
            if (listaServicios.Count > 0)
            {
                listaParcialBusquedaFinal = new List<Servicios>();
                dgvServicios.DataSource = listaParcialBusquedaFinal;
                listaParcialBusqueda = listaServicios.Where(x => x.Nombre.ToUpper().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaServicios.Where(x => x.idServicio.ToUpper().ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaServicios.Where(x => x.CostoBase.ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusquedaFinal = listaParcialBusquedaFinal.Distinct().ToList();
                dgvServicios.DataSource = listaParcialBusquedaFinal;
            }
        }
    }
}
