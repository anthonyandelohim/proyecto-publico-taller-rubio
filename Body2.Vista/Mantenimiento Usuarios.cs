﻿using Body2.Controladora;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Body2.Vista
{
    public partial class frmMantenimientoUsuarios : FormGeneral
    {
        private bool dobleClick = false;
        List<Usuario> listaUsuarios = new List<Usuario>();
        List<Rol> listaRoles = new List<Rol>();
        UsuarioHelper usuarios;
        RolHelper roles;
        public frmMantenimientoUsuarios(string idUsuario)
        {
            InitializeComponent();
            usuarios = new UsuarioHelper(idUsuario);
            roles = new RolHelper(idUsuario);
            ListarUsuarios();
            ListarRoles();

        }
        private void ListarUsuarios()
        {
            listaUsuarios = usuarios.ListarUsuario();

            foreach (var u in listaUsuarios)
            {
                u.idRol = listaRoles.Where(x => x.idRol == Convert.ToInt32(u.idRol)).Select(x => x.NombreRol).FirstOrDefault();
            }

            dgmMUsuarios.DataSource = listaUsuarios;
        }
        private bool Validaciones()
        {
            List<Control> controls = new List<Control>();
            controls.Add(txtClave);
            controls.Add(txtNombre);
            controls.Add(txtUsuario);
            if (!ValidacionInputs(controls))
            {
                MessageBox.Show("Por favor llena toda la información requerida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        private void btnAgregar_Click(object sender, System.EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            //cuando la base de datos tiene el primary key como autoincremental, se le pasa como parametro a este objeto un 0
            Usuario usuario = new Usuario { 
                Nombre = txtNombre.Text,
                User = txtUsuario.Text,
                Clave = txtClave.Text,
                idRol = cmbRol.SelectedValue.ToString()
            };

            string resultado = usuarios.AgregarUsuario(usuario);

            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Usuario agregado exitosamente", "Agregado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbUsuarios, null);
            ListarUsuarios();
            dobleClick = false;
        }
        private void ListarRoles()
        {
            listaRoles = roles.ListarRol();
            cmbRol.DataSource = listaRoles;
            cmbRol.DisplayMember = "NombreRol";
            cmbRol.ValueMember = "idRol";
        }

        private void dgmMUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            dobleClick = true;
            LimpiarInputs(grbUsuarios, txtUsuario);
            Dictionary<Control, int> textBoxes = new Dictionary<Control, int>();
            textBoxes.Add(txtNombre,0);
            textBoxes.Add(txtUsuario,1);
            textBoxes.Add(txtClave,2);
            textBoxes.Add(cmbRol,3);
            LlenarDataGrid(dgmMUsuarios, sender, e, textBoxes);
            txtUsuario.Enabled = false;
        }
        private bool RegistroExistente()
        {
            if (listaUsuarios.Where(x => x.User.Equals(txtUsuario.Text.Replace("-", String.Empty))).FirstOrDefault() == null)
            {
                MessageBox.Show("No hemos encontrado este usuario en la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            DialogResult dialog = MessageBox.Show("¿Deseas actualizar este usuarioo?", "Actualización", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grbUsuarios, txtUsuario);
                return;
            }
            if (!RegistroExistente())
            {
                return;
            }
            Usuario prod = new Usuario
            {
                User = txtUsuario.Text.Trim(),
                Nombre = txtNombre.Text.Trim(),
                idRol = cmbRol.SelectedValue.ToString().Trim(),
                Clave = txtClave.Text.Trim()
            };
            string resultado = usuarios.ActualizarUsuario(prod);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Usuario actualizado exitosamente", "Actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbUsuarios, txtUsuario);
            ListarUsuarios();
            dobleClick = false;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            DialogResult dialog = MessageBox.Show("¿Deseas eliminar este usuario?", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grbUsuarios, txtUsuario);
                return;
            }
            if (!RegistroExistente())
            {
                return;
            }
            string idPieza = txtUsuario.Text.Trim();
            string resultado = usuarios.EliminarUsuario(idPieza);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Usuario eliminado exitosamente", "Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbUsuarios, txtUsuario);
            ListarUsuarios();
            dobleClick = false;
        }

        private void frmMantenimientoUsuarios_Load(object sender, EventArgs e)
        {
            ListarUsuarios();

            Estilo_AgregarBoton(btnAgregar);
            Estilo_EliminarBoton(btnEliminar);
            Estilo_ModificarBoton(btnActualizar);
            Estilo_LimpiarBoton(btnSalir);
        }
        private void LimpiarTodosControles()
        {
            LimpiarInputs(grbUsuarios, txtUsuario);
        }
        private void btnSalir_Click(object sender, EventArgs e)
        {
            LimpiarTodosControles();
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPress_SoloLetras(sender,e);
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPress_SoloNumeros(sender, e);
            TextBox textBox = (TextBox)sender;
            if (textBox.Text.Length >= 11 && e.KeyChar != 8)
            {
                e.Handled = true;
            }
            if ((textBox.Text.Length == 1 || textBox.Text.Length == 6) && e.KeyChar != 8)
            {
                textBox.Text = textBox.Text + "-";
                textBox.SelectionStart = textBox.Text.Length;
            }
        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            string nuevoTexto = "";
            if (textBox.Text.Length == 9 && dobleClick)
            {
                for (int c = 0; c < textBox.Text.Length; c++)
                {
                    if (c == 1 || c == 5)
                    {
                        nuevoTexto = nuevoTexto + "-";
                        nuevoTexto = nuevoTexto + textBox.Text[c];
                    }
                    else
                    {
                        nuevoTexto = nuevoTexto + textBox.Text[c];
                    }
                }
                textBox.Text = nuevoTexto;
            }
        }
    }
}
