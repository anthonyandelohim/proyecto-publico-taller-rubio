﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Body2.Controladora;

namespace Body2.Vista
{
    public partial class frmLogin : Form
    {
        private Usuario user;
        private UsuarioHelper userH;
        private DataTable datos;

        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            
            try
            {
              
                if (this.txtUsuario.Text != "" && this.txtContrasena.Text != "")
                {

                    user = new Usuario();
                    user.NombreU = this.txtUsuario.Text;
                    user.Contrasena = this.txtContrasena.Text;
                    user.opc = 5;

                    userH = new UsuarioHelper(user);
                    datos = userH.ValidaLogin();

                    if (datos.Rows.Count > 0)
                    {
                        DataRow fila = datos.Rows[0];
                        user.Nombre = fila["nombre"].ToString();
                        user.Genero = fila["genero"].ToString();
                        user.Fecha = (DateTime)fila["fecha"];
                        user.Correo =fila["correo"].ToString();
                        user.Perfil = fila["perfil"].ToString();
                        user.Confirmacion = fila["confirmacion"].ToString();

                        frmPrincipal inicio = new frmPrincipal();
                        inicio.Show();
                        this.Hide();
                    }
                    else MessageBox.Show("Datos de inico de sesión incorrectos");
                }
                else MessageBox.Show("Debe llenar todos los campos");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblUsuario_Click(object sender, EventArgs e)
        {

        }

        private void lblContrasena_Click(object sender, EventArgs e)
        {

        }
    }
}
