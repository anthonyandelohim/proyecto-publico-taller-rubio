﻿using Body2.Controladora;
using Body2.Vista.Properties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista
{
    public partial class FormGeneral : Form
    {
        public FormGeneral()
        {
            InitializeComponent();
        }

        private void FormGeneral_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataGrid"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="controles">El orden de como se llena debe de ser el mismo orden de como se encuentra en el form</param>
        /// 

        //llena los grids
        public void LlenarDataGrid(DataGridView dataGrid, object sender, DataGridViewCellEventArgs e, Dictionary<Control, int> controles)
        {
            try
            {
                //verifica que la celda venga visible, si es oculta no entra poque provocaria un error

                bool visble = dataGrid.Columns[e.ColumnIndex].Visible;
                string name = dataGrid.Columns[e.ColumnIndex].Name;
                string value = dataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                if (visble)
                {
                    if (dataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                    {
                        dataGrid.CurrentRow.Selected = true;
                        //cuando se le da doble click a una fila, se llenan los inputs. Se llena con las posiciones del datagrid
                        int contadorDataGrid = 0;
                        foreach (var c in controles)
                        {
                            string txt = c.Key.GetType().Name;
                            string nameCrr = c.Key.Text.ToString();

                            for (int i = contadorDataGrid; i < dataGrid.ColumnCount; i++)
                            {
                                string columnaNombre = dataGrid.Columns[i].Name;
                                bool columnaVisible = dataGrid.Columns[i].Visible;
                                if (!columnaVisible)
                                {
                                    contadorDataGrid += 1;
                                    //break;
                                }
                                else
                                {
                                    switch (txt)
                                    {
                                        case "TextBox":
                                            string texto = dataGrid.Rows[e.RowIndex].Cells[c.Value].FormattedValue.ToString();
                                            c.Key.Text = texto;
                                            contadorDataGrid += 1;
                                            break;

                                        case "CheckedListBox":
                                            string valores = dataGrid.Rows[e.RowIndex].Cells[c.Value].FormattedValue.ToString();
                                            //convierto mis valores que ahorita son un string, en un array por medio de un split
                                            bool tieneVariosValores = false;
                                            string[] arrayValores = null;
                                            if (valores.Contains("-"))
                                            {
                                                arrayValores = valores.Split('-');
                                                tieneVariosValores = true;
                                            }
                                            if (valores.Contains("/"))
                                            {
                                                arrayValores = valores.Split('/');
                                                tieneVariosValores = true;
                                            }
                                            if (!tieneVariosValores)
                                            {
                                                arrayValores = new string[1];
                                                arrayValores[0] = valores;
                                            }
                                            CheckedListBox checkList = c.Key as CheckedListBox;

                                            //recorro todos las opciones que vienen dentro del checkboxlist
                                            for (int b = 0; b < checkList.Items.Count; b++)
                                            {
                                                object objetos = checkList.Items[b];
                                                //como pueden venir opciones de todo tipo (porque esta funcion es general), puedo tener 
                                                //checkboxlist de tipo pantallas ,piezas, etc. Debo de convertirlo en Json format, para poder
                                                //trabajar con ellos
                                                string json = JsonConvert.SerializeObject(objetos, Formatting.Indented);
                                                //una vez convertido en Json, que de hecho es un string, verifico cuales opciones tiene el 
                                                //checkbox list de mi opcion que deseo modificar, para checkearlo para cuando se muestren
                                                string[] arrayJson = null;
                                                if (json.Contains(","))
                                                {
                                                    arrayJson = json.Split(',');
                                                }

                                                //sabemos que el segundo item en el arrayJson es el nombre del item
                                                string nombre = "";
                                                if (arrayJson == null)
                                                {
                                                    nombre = json;
                                                }
                                                else
                                                {
                                                    nombre = arrayJson[1];
                                                }

                                                //lo divido de nuevo ahora por medio de dos puntos (:)
                                                string[] nombreArray = null;
                                                if (nombre.Contains(":"))
                                                {
                                                    nombreArray = nombre.Split(':');
                                                }
                                                //de nuevo sabemos que el segundo item es el nombre separado del item
                                                string nombreSeparado = null;
                                                if (nombreArray != null)
                                                {
                                                    nombreSeparado = nombreArray[1].TrimEnd('}').ToString();
                                                }
                                                else
                                                {
                                                    nombreSeparado = nombre;
                                                }
                                                nombreSeparado = nombreSeparado.Replace('"', ' ').Replace(" ", String.Empty).Replace("\r\n", String.Empty);
                                                if (arrayValores != null)
                                                {
                                                    foreach (var s in arrayValores)
                                                    {
                                                        if (nombreSeparado.ToUpper().Equals(s.Replace(" ", String.Empty).ToUpper()))
                                                        {
                                                            //si existe en la lista, checkeo la opcion actual que esta en el for [posicion]
                                                            int posicion = b;
                                                            checkList.SetItemChecked(posicion, true);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            break;

                                        case "ComboBox":
                                            string valor = dataGrid.Rows[e.RowIndex].Cells[c.Value].FormattedValue.ToString();
                                            ComboBox comboBox = c.Key as ComboBox;
                                            //verifica que el valor fue encontrado. Se hara la verificacion sobre el combbox por medio del value y del item
                                            bool encontrado = false;
                                            foreach (var b in comboBox.Items)
                                            {
                                                if (b.ToString().Equals(valor))
                                                {
                                                    comboBox.SelectedItem = valor;
                                                    encontrado = true;
                                                    break;
                                                }
                                            }
                                            if (!encontrado)
                                            {
                                                int cont = 0;
                                                foreach (var b in comboBox.Items)
                                                {
                                                    object cmbValues = comboBox.Items[cont];
                                                    
                                                    if (b.ToString().Equals(valor))
                                                    {
                                                        comboBox.SelectedItem = valor;
                                                        encontrado = true;
                                                        break;
                                                    }
                                                    cont += 1;
                                                }
                                            }
                                            if (!encontrado)
                                            {
                                                comboBox.Text = valor;
                                            }
                                            contadorDataGrid += 1;
                                            break;
                                        
                                        default:
                                            break;
                                    }
                                    break;
                                }

                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {

            }
        }

        //le da estilo al boton agregar
        public void Estilo_AgregarBoton(Button btn)
        {
            btn.Image = ((Image)(Resources.plus));
            btn.ImageAlign = ContentAlignment.MiddleLeft;
            //btn.Font = new Font("Marlett", 9.5f);
            btn.Size = new Size(110, 28);
            btn.UseVisualStyleBackColor = true;
            btn.Text = btn.Text.ToUpper();
        }

        //le da estilo al boton modificar
        public void Estilo_ModificarBoton(Button btn)
        {
            btn.Image = ((Image)(Resources.arrows));
            btn.ImageAlign = ContentAlignment.MiddleLeft;
            //btn.Font = new Font("Marlett", 9.5f);
            btn.Size = new Size(110, 28);
            btn.Text = btn.Text.ToUpper();
        }
        public void Estilo_EliminarBoton(Button btn)
        {
            btn.Image = ((Image)(Resources.delete));
            btn.ImageAlign = ContentAlignment.MiddleLeft;
            //btn.Font = new Font("Marlett", 9.5f);
            btn.Size = new Size(110, 28);
            btn.Text = btn.Text.ToUpper();
        }
        public void Estilo_CancelarBoton(Button btn)
        {
            btn.Image = ((Image)(Resources.cancel));
            btn.ImageAlign = ContentAlignment.MiddleLeft;
            //btn.Font = new Font("Marlett", 9.5f);
            btn.Size = new Size(110, 28);
            btn.Text = btn.Text.ToUpper();
        }
        public void Estilo_LimpiarBoton(Button btn)
        {
            btn.Image = ((Image)(Resources.clean));
            btn.ImageAlign = ContentAlignment.MiddleLeft;
           // btn.Font = new Font("Marlett", 9.5f);
            btn.Size = new Size(110, 28);
            btn.Text = btn.Text.ToUpper();
        }

        //da un string random
        public string Random()
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 5)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public void LimpiarInputs(GroupBox groupBox, TextBox textBox)
        {
            foreach (Control c in groupBox.Controls)
            {
                if (c is TextBox)
                    c.Text = "";
            }
            if (textBox != null)
                textBox.Enabled = true;
        }

        //permite solo letras a los textbox
        public void KeyPress_SoloLetras(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }

        //permite solo numeros a los textbox
        public void KeyPress_SoloNumeros(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != 8))
            {
                e.Handled = true;
            }

            // If you want, you can allow decimal (float) numbers
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        //calcula los montos
        public void CalcularMontos(decimal precio, decimal montoExtra, TextBox precioUnitario, bool descuento, Label lblTotal, Label lblIva, Label lblDescuento = null)
        {

            try
            {
                decimal iva = Math.Round(precio * 0.13M);
                decimal total = iva + precio;
                if (montoExtra > 0)
                {
                    if (descuento)
                    {
                        total = total - montoExtra;
                    }
                    else
                    {
                        total = total + montoExtra;
                    }
                }
                precioUnitario.Text = precio.ToString();
                lblTotal.Text = total.ToString();
                lblIva.Text = iva.ToString();
                if (lblDescuento != null)
                {
                    if (descuento)
                    {
                        lblDescuento.Text = "-" + montoExtra.ToString();
                    }
                    else
                    {
                        lblDescuento.Text = montoExtra.ToString();
                    }
                    
                }
                
            }
            catch (Exception)
            {

            }

        }

        //oculta columnas que no quiero que aparezcan
        public void OcultarCeldasDataGrid(DataGridView dataGrid, List<string> celdas) {
            try
            {
                //el metodo oculta celdas del datagridview que no es necesario mostrar
                //solo se pasa el nombre de cada celda que se desea ocultar dentro del parametro tipo List<string> llamado celdas
                //los nombres de las celdas son las mismas que el nombre de las propiedades
                foreach (var i in celdas)
                {
                    dataGrid.Columns[i].Visible = false;
                }
            }
            catch (Exception)
            {

            }
        }

        //valida que venga toda la info completa en los textbox
        public bool ValidacionInputs(List<Control> controls) {
            try
            {
                foreach (var c in controls)
                {
                    if (c.Text.Equals(""))
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool TelefonoFormato(TextBox txt, bool esElimnacion)
        {
            try
            {
                string s = txt.Text;
                if (s.Length == 4 && !esElimnacion)
                {
                    double sAsD = double.Parse(s);
                    txt.Text += '-';
                }
                if (txt.Text.Length > 1)
                    txt.SelectionStart = txt.Text.Length;
                txt.SelectionLength = 0;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CorreoFormato(string correo) {
            bool flag = false;
            try
            {
                Regex regex = new Regex(@"\w+@\w+\.\w{2,3}(?<extension>\.\w+)?");
                Match match = regex.Match(correo);
                if (match.Success)
                {
                    string extension = match.Groups["extension"].Value;
                    if (extension.Length == 0)
                    {
                        flag = true;
                    }
                    else
                    {
                        Regex regexExtension = new Regex(@"\.\w{2,3}(?<mas>\w+)?");
                        Match matchExtension = regexExtension.Match(extension);
                        if (matchExtension.Success)
                        {
                            string mas = matchExtension.Groups["mas"].Value;
                            if (mas.Length == 0)
                            {
                                flag = true;
                            }
                            else
                            { flag = false; }
                        }
                        else
                        {
                            flag = false;
                        }
                     }
                }
            }
            catch (Exception)
            {
                flag = false;
            }
            return flag;
        }
    }
}
