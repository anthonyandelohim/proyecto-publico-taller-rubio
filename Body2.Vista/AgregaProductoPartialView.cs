﻿using Body2.Controladora;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista
{
    public partial class AgregaProductoPartialView : FormGeneral
    {
        private bool _creado = false;

        public bool Creado
        {
            get { return _creado; }
            set { _creado = value; }
        }

        readonly string placa = "";
        readonly string cliente = "";
        readonly string idUsuario = "";
        readonly ProductosController productos;
        public AgregaProductoPartialView(string cliente, string placa, string idUsuario)
        {
            InitializeComponent();
            this.idUsuario = idUsuario;
            this.cliente = cliente;
            this.placa = placa;

            productos = new ProductosController(idUsuario);
        }

        private void AgregaProductoPartialView_Load(object sender, EventArgs e)
        {
            txtPlaca.Text = placa;
            txtCliente.Text = cliente;

            Estilo_AgregarBoton(btnAceptar);
            Estilo_LimpiarBoton(btnLimpiar);
            Estilo_CancelarBoton(btnCancelar);
        }
        private bool ValidarInputs()
        {
            List<Control> controls = new List<Control>();
            controls.Add(txtColor);
            controls.Add(txtId);
            controls.Add(txtMarca);
            controls.Add(txtModelo);
            controls.Add(txtNombre);
            return ValidacionInputs(controls);
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarInputs())
            {
                Productos productoObj = new Productos
                {
                    idProducto = txtId.Text.Trim(),
                    NombreProducto = txtNombre.Text.Trim(),
                    Placa = txtPlaca.Text.Trim(),
                    Marca = txtMarca.Text.Trim(),
                    Color = txtColor.Text.Trim(),
                    Cliente = txtCliente.Text.Trim(),
                    Modelo = txtModelo.Text
                };

                string resultado = productos.AgregarProducto(productoObj);
                if (resultado.Contains("Error"))
                {
                    MessageBox.Show("Error. Lo sentimos, hemos tenido un error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Creado = true;
                    MessageBox.Show(resultado, "Creado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Hide();
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Por favor ingresa todos los datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtId.Text = "";
            txtColor.Text = "";
            txtMarca.Text = "";
            txtModelo.Text = "";
            txtNombre.Text = "";
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }
    }
}
