﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Body2.Controladora;
using System.Configuration;
using System.Data.SqlClient;



namespace Body2.Vista
{
    public partial class frmMantenimientoClientes : FormGeneral
    {
        ClienteHelper cliente;
        bool esElimacion = false;
        List<Cliente> listaClientes = new List<Cliente>();
        public frmMantenimientoClientes(string idUsuario)
        {
            InitializeComponent();
            cliente = new ClienteHelper(idUsuario);

        }
        private void frmMantenimientoClientes_Load(object sender, EventArgs e)
        {
            ListarClientes();

            Estilo_AgregarBoton(btnAgregar);
            Estilo_EliminarBoton(btnEliminar);
            Estilo_LimpiarBoton(btnSalir);
            Estilo_ModificarBoton(btnActualizar);
        }
        private void LimpiarTodosControles()
        {
            LimpiarInputs(grbClientes, txtCedula);
        }
        private void btnSalir_Click(object sender, EventArgs e)
        {
            LimpiarTodosControles();
        }


        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsLetter(e.KeyChar) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
            }
        }
        private void ListarClientes() {
            listaClientes = cliente.ListarCliente();
            dgmMClientes.DataSource = listaClientes;
        }
        private bool Validaciones()
        {
            List<Control> controls = new List<Control>();
            controls.Add(txtCedula);
            controls.Add(txtCorreo);
            controls.Add(txtDomicilio);
            controls.Add(txtNombre);
            controls.Add(txtTelefono);
            if (!ValidacionInputs(controls))
            {
                MessageBox.Show("Por favor llena toda la información requerida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (!CorreoFormato(txtCorreo.Text)) {
                MessageBox.Show("Por favor digita un correo valido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (txtTelefono.Text.Length != 9)
            {
                MessageBox.Show("Por favor digita un teléfono valido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            
            return true;
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            Cliente prod = new Cliente
            {
                Cedula = txtCedula.Text,
                Nombre = txtNombre.Text,
                Telefono = txtTelefono.Text,
                Correo = txtCorreo.Text,
                Domicilio = txtDomicilio.Text,
            };
            string resultado = cliente.AgregarCliente(prod);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Cliente agregado exitosamente", "Agregado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //LimpiarInputs(grbClientes, txtCedula);
            ListarClientes();
            LimpiarTodosControles();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            DialogResult dialog = MessageBox.Show("¿Deseas eliminar este cliente?", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grbClientes, txtCedula);
                return;
            }
            string cedula = txtCedula.Text.Trim();
            string resultado = cliente.EliminarCliente(cedula);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Cliente eliminado exitosamente", "Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbClientes, txtCedula);
            ListarClientes();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            DialogResult dialog = MessageBox.Show("¿Deseas actualizar este cliente?", "Actualización", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grbClientes, txtCedula);
                return;
            }
            Cliente prod = new Cliente
            {
                Nombre = txtNombre.Text,
                Cedula = txtCedula.Text,
                Telefono = txtTelefono.Text,
                Correo = txtCorreo.Text,
                Domicilio = txtDomicilio.Text,
            };
            string resultado = cliente.ActualizarCliente(prod);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Cliente actualizado exitosamente", "Actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbClientes, txtCedula);
            ListarClientes();
        }

        private void dgmMClientes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LimpiarInputs(grbClientes, txtCedula);
            Dictionary<Control, int> textBoxes = new Dictionary<Control, int>();
            textBoxes.Add(txtNombre,0);
            textBoxes.Add(txtCedula,1);
            textBoxes.Add(txtTelefono,2);
            textBoxes.Add(txtCorreo,3);
            textBoxes.Add(txtDomicilio,4);
            LlenarDataGrid(dgmMClientes, sender, e, textBoxes);
            txtCedula.Enabled = false;
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox meTxt = (TextBox)sender;
            string search = meTxt.Text.ToUpper();
            List<Cliente> listaParcialBusqueda = new List<Cliente>();
            List<Cliente> listaParcialBusquedaFinal = new List<Cliente>();
            if (meTxt.Text.Length == 0)
            {
                dgmMClientes.DataSource = dgmMClientes;
            }
            if (listaClientes.Count > 0)
            {
                listaParcialBusquedaFinal = new List<Cliente>();
                dgmMClientes.DataSource = listaParcialBusquedaFinal;
                listaParcialBusqueda = listaClientes.Where(x => x.Cedula.ToUpper().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaClientes.Where(x => x.Correo.ToUpper().ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaClientes.Where(x => x.Nombre.ToUpper().ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaClientes.Where(x => x.Telefono.ToUpper().ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaClientes.Where(x => x.Domicilio.ToUpper().ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusquedaFinal = listaParcialBusquedaFinal.Distinct().ToList();
                dgmMClientes.DataSource = listaParcialBusquedaFinal;
            }
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPress_SoloNumeros(sender, e);
        }

        private void txtNombre_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            KeyPress_SoloLetras(sender, e);
        }

        private void txtTelefono_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            TelefonoFormato(textBox, esElimacion);
        }

        private void txtTelefono_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
            {
                esElimacion = true;
            }
            else
            {
                esElimacion = false;
            }
        }
    }
}
