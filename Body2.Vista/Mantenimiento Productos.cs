﻿using Body2.Controladora;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Body2.Vista
{
    public partial class frm_Mantenimiento_Productos : FormGeneral
    {
        List<Productos> listaProductos = new List<Productos>();
        ProductosController productos;
        ClienteHelper clientes;
        public frm_Mantenimiento_Productos(string idUsuario)
        {
            InitializeComponent();
            productos = new ProductosController(idUsuario);
            clientes = new ClienteHelper(idUsuario);
        }
        private void ListarProducto()
        {
            List<Cliente> listaCliente = new List<Cliente>();
            listaCliente = clientes.ListarCliente();
            listaCliente = listaCliente.OrderBy(x => x.Cedula).ToList();

            listaProductos = productos.ListarProductos();

            //Cuando se agrega un nuevo produto y se le coloca un cliente, la base de datos toma la primary key y lo guarda de esa
            //forma, en este caso el primary key en la cedula. Pero es mas representativo tener el nombre del cliente. Este
            //foreach toma la cedula del cliente que esta en la tabla producto, y la reemplaza por el nombre en esta lista que
            //se mostrara en el grid
            foreach (var c in listaCliente)
            {
                foreach (var p in listaProductos)
                {
                    if (p.Cliente.Equals(c.Cedula))
                    {
                        p.Cliente = c.Nombre;
                    }
                }
            }
            dgvProductos.DataSource = listaProductos;
        }

        private void txt_Precio_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPress_SoloNumeros(sender,e);
        }

        private void frm_Mantenimiento_Productos_Load(object sender, EventArgs e)
        {
            ListarProducto();
            ListarClientes();

            Estilo_AgregarBoton(btn_Agregar);
            Estilo_ModificarBoton(btn_Modificar);
            Estilo_LimpiarBoton(btn_Cancelar);
            Estilo_EliminarBoton(btn_Eliminar);
        }

        private SysProductos BuscarCoincidencia(string placa)
        {
            SysProductos sysProducto = new SysProductos();
            try
            {
                sysProducto = productos.BuscarSysProductos(placa);
            }
            catch (Exception)
            {

            }
            return sysProducto;
        }
        private bool Validaciones()
        {
            List<Control> controls = new List<Control>();
            controls.Add(txt_Color);
            controls.Add(txt_Id);
            controls.Add(txt_Marca);
            controls.Add(txt_Nombre);
            controls.Add(txt_Placa);
            controls.Add(txtModelo);
            if (!ValidacionInputs(controls))
            {
                MessageBox.Show("Por favor llena toda la información requerida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        private bool RegistroExistente()
        {
            if (listaProductos.Where(x => x.idProducto.Equals(txt_Id.Text)).FirstOrDefault() == null)
            {
                MessageBox.Show("No hemos encontrado este vehiculo en la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        private void btn_Agregar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            SysProductos sysProducto = new SysProductos();
            sysProducto = BuscarCoincidencia(txt_Id.Text);
            if (sysProducto.Placa != null)
            {
                DialogResult dialogResult = MessageBox.Show("Este producto se encuentra en la lista de productos creados tipo System. " +
                    "¿Deseas agregarlo directamente a tu lista de productos del sistema?", "Existente", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("Por favor utiliza otro identificador para este producto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    productos.EliminarSysProducto(txt_Id.Text);
                }
            }

            Productos prod = new Productos { 
                idProducto = txt_Id.Text.Trim(),
                NombreProducto = txt_Nombre.Text.Trim(),
                Placa = txt_Placa.Text.Trim(),
                Marca = txt_Marca.Text.Trim(),
                Color = txt_Color.Text.Trim(),
                Cliente = cmbClientes.SelectedValue.ToString(),
                Modelo = txtModelo.Text
            };
            string resultado = productos.AgregarProducto(prod);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Vehiculo agregado exitosamente", "Agregado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grpProductos, txt_Id);
            ListarProducto();
        }

        private void btn_Modificar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            if (!RegistroExistente())
            {
                return;
            }
            DialogResult dialog = MessageBox.Show("¿Deseas actualizar este vehiculo?", "Actualización", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grpProductos, txt_Id);
                return;
            }
            if (cmbClientes.SelectedValue == null)
            {
                cmbClientes.SelectedIndex = 0;
            }
            Productos prod = new Productos
            {
                idProducto = txt_Id.Text.Trim(),
                NombreProducto = txt_Nombre.Text.Trim(),
                Placa = txt_Placa.Text.Trim(),
                Marca = txt_Marca.Text.Trim(),
                Color = txt_Color.Text.Trim(),
                Cliente = cmbClientes.SelectedValue.ToString(),
                Modelo = txtModelo.Text
            };
            string resultado = productos.ActualizarProducto(prod);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Vehiculo actualizado exitosamente", "Actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grpProductos, txt_Id);
            ListarProducto();
        }

        private void btn_Eliminar_Click(object sender, EventArgs e)
        {
            if (!Validaciones())
            {
                return;
            }
            if (!RegistroExistente())
            {
                return;
            }
            DialogResult dialog = MessageBox.Show("¿Deseas eliminar este vehiculo?? Pueden existir muchos problemas", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialog == DialogResult.No)
            {
                LimpiarInputs(grpProductos, txt_Id);
                return;
            }
            string idProducto = txt_Id.Text.Trim();
            string resultado = productos.EliminarProducto(idProducto);
            if (resultado.Contains("Error"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Vehiculo eliminado exitosamente", "Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grpProductos, txt_Id);
            ListarProducto();
        }

        private void btn_Cancelar_Click(object sender, EventArgs e)
        {
            LimpiarInputs(grpProductos, txt_Id);
        }

        private void ListarClientes()
        {
            //llena la lista con las piezas de la base de datos
            List<Cliente> lista = new List<Cliente>();
            lista = clientes.ListarCliente();
            lista.Add(new Cliente { 
                Cedula = "0",
                Correo = "",
                Domicilio = "",
                Nombre = "Sin Cliente",
                Telefono = ""
            });
            lista = lista.OrderBy(x => x.Cedula).ToList();
            cmbClientes.DataSource = lista;
            cmbClientes.DisplayMember = "Nombre";
            cmbClientes.ValueMember = "Cedula";
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox meTxt = (TextBox)sender;
            string search = meTxt.Text.ToUpper();
            List<Productos> listaParcialBusqueda = new List<Productos>();
            List<Productos> listaParcialBusquedaFinal = new List<Productos>();
            if (meTxt.Text.Length == 0)
            {
                dgvProductos.DataSource = listaProductos;
            }
            if (listaProductos.Count > 0)
            {
                listaParcialBusquedaFinal = new List<Productos>();
                dgvProductos.DataSource = listaParcialBusquedaFinal;
                listaParcialBusqueda = listaProductos.Where(x => x.NombreProducto.ToUpper().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaProductos.Where(x => x.idProducto.ToUpper().ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaProductos.Where(x => x.Color.ToUpper().ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaProductos.Where(x => x.Marca.ToUpper().ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaProductos.Where(x => x.Placa.ToUpper().ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaProductos.Where(x => x.Cliente.ToUpper().ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusquedaFinal = listaParcialBusquedaFinal.Distinct().ToList();
                dgvProductos.DataSource = listaParcialBusquedaFinal;
            }
        }

        private void dgvProductos_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            LimpiarInputs(grpProductos, txt_Id);
            Dictionary<Control, int> textBoxes = new Dictionary<Control, int>();
            textBoxes.Add(txt_Id, 0);
            textBoxes.Add(txt_Nombre, 1);
            textBoxes.Add(txt_Color, 2);
            textBoxes.Add(txt_Marca, 3);
            textBoxes.Add(txt_Placa, 4);
            textBoxes.Add(cmbClientes, 5);
            textBoxes.Add(txtModelo, 6);
            LlenarDataGrid(dgvProductos, sender, e, textBoxes);
            txt_Id.Enabled = false;
        }


    }
}
