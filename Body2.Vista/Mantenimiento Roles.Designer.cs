﻿namespace Body2.Vista
{
    partial class Mantenimiento_Roles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mantenimiento_Roles));
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.grbCckList = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.clbPantallas = new System.Windows.Forms.CheckedListBox();
            this.dgvRolesPermisos = new System.Windows.Forms.DataGridView();
            this.lblEnderezado = new System.Windows.Forms.Label();
            this.grbRoles = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbEstado = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grbCckList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRolesPermisos)).BeginInit();
            this.grbRoles.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(208, 262);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 131;
            this.label4.Text = "Buscar";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(187, 258);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(17, 23);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 130;
            this.pictureBox1.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(9, 259);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(174, 20);
            this.textBox1.TabIndex = 36;
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // grbCckList
            // 
            this.grbCckList.BackColor = System.Drawing.Color.Transparent;
            this.grbCckList.Controls.Add(this.label3);
            this.grbCckList.Controls.Add(this.clbPantallas);
            this.grbCckList.Location = new System.Drawing.Point(452, 40);
            this.grbCckList.Margin = new System.Windows.Forms.Padding(2);
            this.grbCckList.Name = "grbCckList";
            this.grbCckList.Padding = new System.Windows.Forms.Padding(2);
            this.grbCckList.Size = new System.Drawing.Size(236, 215);
            this.grbCckList.TabIndex = 35;
            this.grbCckList.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 55;
            this.label3.Text = "Pantallas:";
            // 
            // clbPantallas
            // 
            this.clbPantallas.FormattingEnabled = true;
            this.clbPantallas.Location = new System.Drawing.Point(4, 32);
            this.clbPantallas.Margin = new System.Windows.Forms.Padding(2);
            this.clbPantallas.Name = "clbPantallas";
            this.clbPantallas.Size = new System.Drawing.Size(218, 184);
            this.clbPantallas.TabIndex = 34;
            // 
            // dgvRolesPermisos
            // 
            this.dgvRolesPermisos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRolesPermisos.Location = new System.Drawing.Point(9, 284);
            this.dgvRolesPermisos.Margin = new System.Windows.Forms.Padding(2);
            this.dgvRolesPermisos.Name = "dgvRolesPermisos";
            this.dgvRolesPermisos.ReadOnly = true;
            this.dgvRolesPermisos.RowHeadersWidth = 51;
            this.dgvRolesPermisos.RowTemplate.Height = 24;
            this.dgvRolesPermisos.Size = new System.Drawing.Size(679, 202);
            this.dgvRolesPermisos.TabIndex = 34;
            this.dgvRolesPermisos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRolesPermisos_CellDoubleClick);
            // 
            // lblEnderezado
            // 
            this.lblEnderezado.AutoSize = true;
            this.lblEnderezado.BackColor = System.Drawing.Color.White;
            this.lblEnderezado.Font = new System.Drawing.Font("Arial Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnderezado.ForeColor = System.Drawing.Color.Black;
            this.lblEnderezado.Location = new System.Drawing.Point(134, 11);
            this.lblEnderezado.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEnderezado.Name = "lblEnderezado";
            this.lblEnderezado.Size = new System.Drawing.Size(274, 27);
            this.lblEnderezado.TabIndex = 32;
            this.lblEnderezado.Text = "MANTENIMIENTO ROLES";
            // 
            // grbRoles
            // 
            this.grbRoles.BackColor = System.Drawing.Color.Transparent;
            this.grbRoles.Controls.Add(this.button1);
            this.grbRoles.Controls.Add(this.txtDescripcion);
            this.grbRoles.Controls.Add(this.label2);
            this.grbRoles.Controls.Add(this.cmbEstado);
            this.grbRoles.Controls.Add(this.label1);
            this.grbRoles.Controls.Add(this.btnAgregar);
            this.grbRoles.Controls.Add(this.btnEliminar);
            this.grbRoles.Controls.Add(this.btnActualizar);
            this.grbRoles.Controls.Add(this.txtNombre);
            this.grbRoles.Controls.Add(this.lblNombre);
            this.grbRoles.Location = new System.Drawing.Point(9, 40);
            this.grbRoles.Margin = new System.Windows.Forms.Padding(2);
            this.grbRoles.Name = "grbRoles";
            this.grbRoles.Padding = new System.Windows.Forms.Padding(2);
            this.grbRoles.Size = new System.Drawing.Size(443, 215);
            this.grbRoles.TabIndex = 0;
            this.grbRoles.TabStop = false;
            this.grbRoles.Text = "Información";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(340, 182);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 28);
            this.button1.TabIndex = 54;
            this.button1.Text = "Limpiar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(100, 98);
            this.txtDescripcion.Margin = new System.Windows.Forms.Padding(2);
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(224, 80);
            this.txtDescripcion.TabIndex = 53;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 100);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 52;
            this.label2.Text = "Descripción:";
            // 
            // cmbEstado
            // 
            this.cmbEstado.FormattingEnabled = true;
            this.cmbEstado.Items.AddRange(new object[] {
            "Rol Habilitado",
            "Rol Deshabilitado"});
            this.cmbEstado.Location = new System.Drawing.Point(100, 62);
            this.cmbEstado.Margin = new System.Windows.Forms.Padding(2);
            this.cmbEstado.Name = "cmbEstado";
            this.cmbEstado.Size = new System.Drawing.Size(224, 21);
            this.cmbEstado.TabIndex = 51;
            this.cmbEstado.Tag = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 64);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 50;
            this.label1.Text = "Estado del rol:";
            // 
            // btnAgregar
            // 
            this.btnAgregar.Font = new System.Drawing.Font("Arial", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregar.Location = new System.Drawing.Point(12, 182);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(2);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(98, 27);
            this.btnAgregar.TabIndex = 49;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Font = new System.Drawing.Font("Arial", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(232, 181);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(2);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(98, 28);
            this.btnEliminar.TabIndex = 47;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Font = new System.Drawing.Font("Arial", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnActualizar.Location = new System.Drawing.Point(122, 182);
            this.btnActualizar.Margin = new System.Windows.Forms.Padding(2);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(98, 28);
            this.btnActualizar.TabIndex = 48;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(100, 26);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(2);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(224, 20);
            this.txtNombre.TabIndex = 34;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(16, 28);
            this.lblNombre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(78, 13);
            this.lblNombre.TabIndex = 33;
            this.lblNombre.Text = "Nombre del rol:";
            // 
            // Mantenimiento_Roles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(700, 495);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.grbCckList);
            this.Controls.Add(this.dgvRolesPermisos);
            this.Controls.Add(this.lblEnderezado);
            this.Controls.Add(this.grbRoles);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3);
            this.MaximumSize = new System.Drawing.Size(716, 534);
            this.MinimumSize = new System.Drawing.Size(716, 534);
            this.Name = "Mantenimiento_Roles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mantenimiento_Roles";
            this.Load += new System.EventHandler(this.Mantenimiento_Roles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grbCckList.ResumeLayout(false);
            this.grbCckList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRolesPermisos)).EndInit();
            this.grbRoles.ResumeLayout(false);
            this.grbRoles.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbRoles;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblEnderezado;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.DataGridView dgvRolesPermisos;
        private System.Windows.Forms.ComboBox cmbEstado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.GroupBox grbCckList;
        private System.Windows.Forms.CheckedListBox clbPantallas;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
    }
}