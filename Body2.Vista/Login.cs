﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Body2.Controladora;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace Body2.Vista
{
    public partial class frmLogin : FormGeneral
    {
        LoginController login = new LoginController();
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void verificar()
        {
            //llama al metodo creado en la capa de controlador para verificar si existen los datos ingresados // F12 en LoginUsuario para navegar
            Dictionary<string, List<Pantalla>> resultado = login.LoginUsuario(txtUsuario.Text.Replace("-", String.Empty).Trim(), txtContrasena.Text);

            //el metodo devuelve un diccionario de strings, en el cual en la key (1era posicion) viene el rol o el error, y en el value (2da posicion) viene el nombre del usuario
            //el diccionario se recorre mediante un lambda
            if (resultado.Select(x => x.Key.Contains("Error")).FirstOrDefault())
            {
                MessageBox.Show(resultado.Select(x => x.Key).FirstOrDefault(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                List<Pantalla> pantallas = new List<Pantalla>();
                foreach (var d in resultado)
                {
                    foreach (var p in d.Value)
                    {
                        pantallas.Add(p);
                    }
                }
                MessageBox.Show(resultado.Select(x => x.Key).FirstOrDefault(), "Bienvenido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                frmPrincipal frmPrincipal = new frmPrincipal(pantallas, txtUsuario.Text.Replace("-", String.Empty));
                this.Hide();
                frmPrincipal.Show();
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            verificar();
        }

        private void timHoraFecha_Tick(object sender, EventArgs e)
        {
            lblHora.Text = DateTime.Now.ToShortTimeString();
            lblFecha.Text = DateTime.Now.ToShortDateString();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            Estilo_AgregarBoton(btnAceptar);
            Estilo_CancelarBoton(btnCancelar);
        }
    }
}