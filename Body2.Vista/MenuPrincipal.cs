﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Body2.Controladora;
using System.Diagnostics;

namespace Body2.Vista
{
    public partial class frmPrincipal : FormGeneral
    {

       
       
        private frmMantenimientoClientes addmcli;
        private frmMantenimientoEnderezado addmcot;

        private Mantenimiento_Roles addmryp;
        private frmMantenimientoUsuarios addmu;
        private frm_Mantenimiento_Productos productos;
        private Mantenimiento_Servicios servicios;
        private Mantenimiento_Piezas piezas;
        private frmAcercaDe addad;

      
        private Reportes.Acciones.AccionesReportes addrmo;
        private Reportes.Mantenimiento_Reportes reportes;
        private List<Pantalla> pantalla;
        private string idUsuario = "";
        private List<Usuario> usuarios = new List<Usuario>();
        private UsuarioHelper usuarioHelper;

        private Timer timer1;
        private int counter = 60;

        private bool thisHide = false;
        //private frmLogin addlo;
        //private Usuario user;

        //espera un parametro, todas las pantallas que tiene acceso este usuario basado en el rol.
        //en este caso solo me importa la lista de pantallas a las que se puede ingresar
        public frmPrincipal(List<Pantalla> pantalla, string idUsuario)
        {
            InitializeComponent();
            this.pantalla = pantalla;
            this.idUsuario = idUsuario;
        }

        private void Alerta() {
            MessageBox.Show("Lo sentimos, no tienes acceso a esta pantalla", "Error de ingreso", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private bool RevisionPantallas(string pantallaParam)
        {
             int resultado = pantalla.Where(x => x.NombrePantalla.Equals(pantallaParam)).Select(c => c.idPantalla).FirstOrDefault();
            if (resultado == 0)
            {
                Alerta();
                return false;
            }
            else
            {
                return true;
            }
        }

        private void tsmSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void sALIRToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void iNGRESARCONOTROUSUARIOToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmLogin fm = new frmLogin();
            this.Hide();
            fm.ShowDialog();
            this.Close();
        }

        private void timHoraFecha_Tick(object sender, EventArgs e)
        {
            lblHora.Text = DateTime.Now.ToLongTimeString();
            lblFecha.Text = DateTime.Now.ToShortDateString();
        }
        private void TimeStart()
        {
            timer1 = new Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 1000; // 1 second
            timer1.Start();
            lblHora.Text = counter.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            counter--;
            if (counter == 0)
                timer1.Stop();
            lblHora.Text = counter.ToString();
        }
        private void uSUARIOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RevisionPantallas("Usuarios"))
            {
                addmu = new frmMantenimientoUsuarios(idUsuario);
                addmu.ShowDialog();
            }
        }

        private void rOLESYPERMISOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
               
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            this.FormClosed += new FormClosedEventHandler(closeapp);
            usuarioHelper = new UsuarioHelper("");
            usuarios = usuarioHelper.ListarUsuario();
            lblLoggueado.Text = "Gracias por tu trabajo " + usuarios.Where(x => x.User.ToUpper().Equals(idUsuario)).Select(x => x.Nombre).FirstOrDefault();
        }

        private void closeapp(object sender, EventArgs e)

        {
            Application.Exit();
        }


        private void oTROUSUARIOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("¿Deseas salir del sistema?", "Salida", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                LoginController login = new LoginController();
                MessageBox.Show(login.CloseSystem(idUsuario), "Salida", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                frmLogin fm = new frmLogin();
                thisHide = true;
                this.Hide();
                fm.ShowDialog();
            }
        }

        private void sALIRToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aYUDAToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Process proceso = new Process();
            proceso.StartInfo.FileName = @"C:\Users\MOMO\Desktop\JU29 1018PM\Body2.Vista\Ayuda\MANUAL USUARIO PCTR.pdf";
            proceso.Start();
        }

        private void aCERCADEToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            addad = new frmAcercaDe();
            addad.ShowDialog();
        }

        private void cLIENTEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RevisionPantallas("Clientes"))
            {
                addmcli = new frmMantenimientoClientes(idUsuario);
                addmcli.ShowDialog();
            }
        }

        private void eNDEREZADOYPINTURAToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (RevisionPantallas("Enderezado"))
            {
                addmcot = new frmMantenimientoEnderezado(idUsuario);
                addmcot.ShowDialog();
            }

        }




        private void cLIENTEToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (RevisionPantallas("Clientes")) { 
                addmcli = new frmMantenimientoClientes(idUsuario);
                addmcli.ShowDialog();
            }
                
        }

        private void eNDEREZADOYPINTURAToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            addmcot = new frmMantenimientoEnderezado(idUsuario);
            addmcot.ShowDialog();
        }





        private void mOVIMIENTOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //addrmo = new Reportes.Acciones.AccionesReportes(idUsuario);
            addrmo.ShowDialog();
        }



        private void rOLYPERMISOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RevisionPantallas("Roles"))
            {
                addmryp = new Mantenimiento_Roles(idUsuario);
                addmryp.ShowDialog();
            }
        }

        private void uSUARIOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RevisionPantallas("Usuarios"))
            {
                addmu = new frmMantenimientoUsuarios(idUsuario);
                addmu.ShowDialog();
            }    
        }

        private void aGREGARPRODUCTOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
             
        }

        private void rEPORTEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RevisionPantallas("Reportes"))
            {
                reportes = new Reportes.Mantenimiento_Reportes(idUsuario);
                reportes.ShowDialog();
            }
        }

        private void frmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialog = MessageBox.Show("¿Deseas salir del sistema?", "Salida", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                LoginController login = new LoginController();
                MessageBox.Show(login.CloseSystem(idUsuario), "Salida", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                e.Cancel = (dialog == DialogResult.No);
            }

        }

        private void pRODUCTOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RevisionPantallas("Enderezado"))
            {
                productos = new frm_Mantenimiento_Productos(idUsuario);
                productos.ShowDialog();
            }
        }

        private void pIEZASToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RevisionPantallas("Piezas"))
            {
                piezas = new Mantenimiento_Piezas(idUsuario);
                piezas.ShowDialog();
            }
        }

        private void sERVICIOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RevisionPantallas("Servicios"))
            {
                servicios = new Mantenimiento_Servicios(idUsuario);
                servicios.ShowDialog();
            }
        }
    }
}
