﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Body2.Controladora;
using Newtonsoft.Json;

namespace Body2.Vista
{
    public partial class frmMantenimientoEnderezado : FormGeneral
    {
        PiezaController piezas;
        ClienteHelper clientes;
        CotizacionHelper cotizaciones;
        ProductosController productosController;
        //esta listaProductos es donde se va almacenar los productos para la cotizacion
        List<Servicios> listaDeServicios = new List<Servicios>();
        //esta lista sera llenada para guardar todos los productos solo una vez y usarla en la clase para no consultar a cada rato a la DB
        List<Productos> listaTodosProductos = new List<Productos>();

        private List<Piezas> listaPiezas = new List<Piezas>();
        private List<Cotizacion> listaCotizaciones = new List<Cotizacion>();
        private string idUsuario = "";

        List<Cliente> listaClientes = new List<Cliente>();

        List<Servicios> listaServicios = new List<Servicios>();
        ServiciosHelper servicios;

        //esta variable indica si ya se debe crear la cotizacion o se debe de esperar por algo mas
        private bool cotizacionLista = false;
        public frmMantenimientoEnderezado(string idUsuario)
        {
            InitializeComponent();
            this.idUsuario = idUsuario;
            productosController = new ProductosController(idUsuario);
            cotizaciones = new CotizacionHelper(idUsuario);
            piezas = new PiezaController(idUsuario);
            clientes = new ClienteHelper(idUsuario);
            servicios = new ServiciosHelper(idUsuario);
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timHoraFecha_Tick(object sender, EventArgs e)
        {
            label1.Text = DateTime.Now.ToShortTimeString();
            lblHora.Text = DateTime.Now.ToShortDateString();
        }

        private void frmMantenimientoEnderezado_Load(object sender, EventArgs e)
        {
            //todo metodo que se encuentre en el Load, se ejecutara cuando el sistema entre al form respectivo
            ListarPiezas();
            ListarClientes();
            ListarProductos();
            ListarServicios();
            ListarCotizaciones();
            txtReferencia.Text = Random();

            rdNoDescuento.Enabled = false;
            rdDescuento.Enabled = false;
            txtMontoPrecio.Enabled = false;

            Estilo_AgregarBoton(btnAgregar);
            Estilo_ModificarBoton(btnActualizar);
            Estilo_LimpiarBoton(btnLimpiar);
            Estilo_CancelarBoton(btnSalir);
        }

        private void ListarPiezas() {
            //llena la lista con las piezas de la base de datos
            if (clbPiezas.Items.Count > 0)
            {
                clbPiezas.Items.Clear();
            }
            listaPiezas = piezas.ListarPiezas();
            foreach (var i in listaPiezas)
            {
                clbPiezas.Items.Add(i.NombrePieza);
                clbPiezas.SelectedValue = i.idPieza;
            }
        }

        private void ListarServicios()
        {
            listaServicios = servicios.ListarServicios();
            listaServicios = listaServicios.OrderBy(x => x.idServicio).ToList();
            cmbServicios.DataSource = listaServicios;
            cmbServicios.DisplayMember = "Nombre";
            cmbServicios.ValueMember = "idServicio";
        }
        private void ListarClientes()
        {
            //llena la lista con los clientes de la base de datos
            
            listaClientes = clientes.ListarCliente();
            listaClientes.Add(new Cliente
            {
                Cedula = "0",
                Correo = "",
                Domicilio = "",
                Nombre = "-- Selecciona un cliente --",
                Telefono = ""
            });
            listaClientes = listaClientes.OrderBy(x => x.Cedula).ToList();
            cmbClientes.DataSource = listaClientes;
            cmbClientes.DisplayMember = "Nombre";
            cmbClientes.ValueMember = "Cedula";
        }
        private void ListarProductos()
        {
            //esta funcion toma la lista completa de productos, y selecciona aquellos productos que pertenezcan al cliente seleccionado en el comboboxClientes
            listaTodosProductos = productosController.ListarProductos();
        }
        private void ListarProductos(string cedula) {
            List<Productos> lista = listaTodosProductos;
            try
            {
                lista[lista.FindIndex(i => i.Cliente == null)].Cliente = "";
            }
            catch (Exception)
            {}

            lista = lista.Where(x => x.Cliente.Equals(cedula)).ToList();
            lista = lista.OrderBy(x => x.idProducto).ToList();
            if (cmbProductos.Items.Count > 0)
            {
                //eliminar los items en el combobox para resetearlo y que no cargue valores antiguos
                cmbProductos.DataSource = null;
                cmbProductos.Items.Clear();
                cmbProductos.Text = "";
            }
            cmbProductos.DataSource = lista;
            cmbProductos.DisplayMember = "Placa";
            cmbProductos.ValueMember = "idProducto";

            //se coloca en el label el precio que tiene dicho auto. El precio es el que se coloca en la tabla de productos en la DB
            //este precio lo coloca el usuario cuando agrega un producto nuevo. Los producto tienen el nombre, placa y precio
            string placa = "";
            if (cmbProductos.SelectedValue != null)
            {
                placa = cmbProductos.SelectedValue.ToString();
            }
            PrecioIndividual(placa, lista);
        }

        private void PrecioIndividual(string producto, List<Productos> lista) {
            //tomo todos los productos con el mismo numero de placa
            lista = lista.Where(x => x.idProducto == producto).ToList();
            //tomo el precio correspondiente a esa producto
            //lblPrecioIndividual.Text = "₡ " + lista.Where(x => x.idProducto == producto).Select(x => x.PrecioProducto).FirstOrDefault().ToString();
            //txtMontoPrecio.Text = lista.Where(x => x.idProducto == producto).Select(x => x.PrecioProducto).FirstOrDefault().ToString();
        }
        private string TomarPiezasDanadas()
        {
            string json = "";
            try
            {
                List<Piezas> piezasDanadas = new List<Piezas>();
                if (clbPiezas.CheckedItems.Count != 0)
                {
                    for (int i = 0; i < clbPiezas.CheckedIndices.Count; i++)
                    {
                        string value = clbPiezas.Items[i].ToString();
                        piezasDanadas.Add(new Piezas { 
                            NombrePieza = clbPiezas.CheckedItems[i].ToString(),
                            idPieza = listaPiezas.Where(x => x.NombrePieza.Equals(clbPiezas.CheckedItems[i].ToString())).Select(x => x.idPieza).FirstOrDefault()
                        });
                    }
                }
                json = JsonConvert.SerializeObject(piezasDanadas);
            }
            catch (Exception ex)
            {

            }
            return json;
        }
        private List<Control> controls()
        {
            List<Control> controls = new List<Control>();
            controls.Add(lblMontoIva);
            controls.Add(lblMontoTotal);
            controls.Add(txtMontoPrecio);
            controls.Add(cmbProductos);
            
            return controls;
        }

        private bool ValidarLlenarServicios()
        {
            if (dgvProductos.Rows.Count == 0)
            {
                MessageBox.Show("Error, por favor agregar al menos un servicio a la cotización", "Sin servicios", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else
            {
                return true;
            }
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (!ValidacionInputs(controls()))
            {
                MessageBox.Show("Por favor ingresa toda la información requerida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!ValidarLlenarServicios())
            {
                return;
            }

            if (cmbClientes.SelectedValue.ToString().Equals("0") && !cotizacionLista)
            {
                MessageBox.Show("Por favor selecciona un cliente para crear la cotización.", "Cuidado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                cotizacionLista = true;
            }

            if (!cotizacionLista)
            {
                MessageBox.Show("Es probable que estés creando una cotización sobre un \n número de cotización ya existente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string montoExtra = txtMontoExtra.Text;
            if (String.IsNullOrEmpty(montoExtra))
                montoExtra = "0";

            //toma la lista de todos los producto y los convierte en un formato Json
            string productosJson = JsonConvert.SerializeObject(listaDeServicios);

            string piezasDanadas = TomarPiezasDanadas();


            Cotizacion prod = new Cotizacion
            {
                Referencia = txtReferencia.Text.Trim(),
                Fecha = dtpFecha.Value,
                Observaciones = txtObservacion.Text,
                Productos = cmbProductos.Text,
                Iva = Convert.ToDecimal(lblMontoIva.Text),
                Total = Convert.ToDecimal(lblMontoTotal.Text),
                MontoExtra = Convert.ToDecimal(montoExtra),
                IdCliente = cmbClientes.SelectedValue.ToString(),
                IdUsuario = idUsuario,
                PiezasDanadas = piezasDanadas,
                Servicios = productosJson
            };
            listaTodosProductos = productosController.ListarProductos();
            string resultado = cotizaciones.RevisarPlacaCliente(listaTodosProductos, cmbClientes.SelectedValue.ToString(), cmbClientes.Text,cmbProductos.Text);
            if (resultado.Contains("¿Deseas crear este producto para") && !cmbClientes.SelectedValue.ToString().Equals("0"))
            {
                DialogResult dialog = MessageBox.Show(resultado.Replace(cmbClientes.SelectedValue.ToString(), cmbClientes.Text), "Nuevo producto", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    AgregaProductoPartialView partialView = new AgregaProductoPartialView(cmbClientes.Text, cmbProductos.Text, idUsuario);
                    partialView.ShowDialog();
                    if (!partialView.Creado)
                    {
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Lo sentimos, debes de elegir una placa existente al cliente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                
            }
            else if (resultado.Contains("Error. Esta placa ya pertenece a otro cliente"))
            {
                MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (cmbClientes.SelectedValue.ToString().Equals("0"))
            {
                MessageBox.Show("Por favor selecciona un cliente para crear la cotización.", "Cuidado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Cotizacion cotizacion = cotizaciones.crearCotizaciones(prod);
            
            if (cotizacion == null)
            {
                MessageBox.Show("Tuvimos un error en la creación de la cotización", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Cotización creada exitosamente", "Agregado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbEnderezado, txtReferencia);
            ListarCotizaciones();
            //se inhabilita de nuevo el textbox del precio
            txtMontoPrecio.Enabled = false;
            LimpiarTodosControles();
            txtReferencia.Text = Random();
            listaDeServicios = new List<Servicios>();
            txtMontoPrecio.Text = "0";
            lblMontoExtra.Text = "0";
            lblMontoIva.Text = "0";
            lblMontoTotal.Text = "0";
            lblPrecioIndividual.Text = "0";

            cmbClientes.SelectedIndex = 0;
            cmbProductos.Text = "";
        }
        private void LimpiarTodosControles()
        {
            LimpiarInputs(grbEnderezado, txtReferencia);
            for (int i = 0; i < clbPiezas.Items.Count; i++)
            {
                clbPiezas.SetItemChecked(i, false);
            }
            dgvProductos.DataSource = null;
            LimpiarInputs(grbEnderezado, txtReferencia);
            txtMontoPrecio.Text = "0";
            lblMontoExtra.Text = "0";
            lblMontoIva.Text = "0";
            lblMontoTotal.Text = "0";
            lblPrecioIndividual.Text = "0";

            cmbClientes.SelectedIndex = 0;
        }
        private void ListarCotizaciones()
        {
            listaCotizaciones = cotizaciones.ListarCotizacion();
            foreach (var c in listaCotizaciones)
            {
                c.IdCliente = listaClientes.Where(x => x.Cedula == c.IdCliente).Select(x => x.Nombre).FirstOrDefault();
            }
            if (listaCotizaciones == null)
            {
                listaCotizaciones = new List<Cotizacion>();
            }
            dgvmep.DataSource = listaCotizaciones;
            OcultaCeldas();
        }
        private void cmbClientes_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cmbClientes.SelectedValue != null)
            {
                string cedula = cmbClientes.SelectedValue.ToString();
                ListarProductos(cedula);
            }
            
            Montos();
        }
        private void Montos(bool deLista = false, List<Servicios> listaModificar = null, decimal montonDelDgv = 0) {

            string precio = "0";
            string montoExtra = txtMontoExtra.Text;
            precio = listaDeServicios.Sum(x => x.CostoBase).ToString();
            decimal precioConIva = Math.Round((Convert.ToDecimal(precio) + (Convert.ToDecimal(precio) * 0.13M)),2);
            if (String.IsNullOrEmpty(montoExtra))
            {
                montoExtra = "0";
            }

            if (montonDelDgv > 0)
            {
                decimal montoActual = montonDelDgv - Convert.ToDecimal(precioConIva);
                montoExtra = montoActual.ToString();
                if (montoActual < 0)
                {
                    //si es positivo quiere decir que es un descuento
                    rdDescuento.Checked = true;
                    montoExtra = montoExtra.ToString().Replace("-", String.Empty);
                }
                else
                {
                    rdNoDescuento.Checked = true;
                }
            }

            CalcularMontos(Convert.ToDecimal(precio), Convert.ToDecimal(montoExtra), txtMontoPrecio, rdDescuento.Checked, lblMontoTotal, lblMontoIva, lblDescuento: lblMontoExtra);
        }

        private void rdDescuento_Click(object sender, EventArgs e)
        {
            lblExtra.Text = "DESC";
            lblMontoExtra.Text = "-" + txtMontoExtra.Text;
            Montos();
        }

        private void rdNoDescuento_Click(object sender, EventArgs e)
        {
            lblExtra.Text = "EXTRA";
            lblMontoExtra.Text = txtMontoExtra.Text;
            Montos();
        }

        private void txtMontoExtra_TextChanged(object sender, EventArgs e)
        {
            lblExtra.Text = "EXTRA";
            lblMontoExtra.Text = txtMontoExtra.Text;
            if (!txtMontoExtra.Text.Equals(""))
            {
                rdNoDescuento.Enabled = true;
                rdDescuento.Enabled = true;
                rdNoDescuento.Checked = true;
            }
            else
            {
                rdNoDescuento.Enabled = false;
                rdDescuento.Enabled = false;
                rdNoDescuento.Checked = false;
                rdDescuento.Checked = false;
            }
            Montos();
        }

        private void dgvmep_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LimpiarInputs(grbEnderezado, txtReferencia);
            for (int i = 0; i < clbPiezas.Items.Count; i++)
            {
                clbPiezas.SetItemChecked(i, false);
            }
            
            Dictionary<Control, int> textBoxes = new Dictionary<Control, int>();
            textBoxes.Add(txtReferencia, 0);
            textBoxes.Add(txtObservacion, 2);
            textBoxes.Add(dgvProductos, 3);
            textBoxes.Add(cmbClientes, 7);
            textBoxes.Add(clbPiezas,9);
            textBoxes.Add(cmbProductos,3);
            //textBoxes.Add(txt);
            LlenarDataGrid(dgvmep, sender, e, textBoxes);
            int rowIndex = e.RowIndex;
            ListarServiciosModifica(rowIndex);
            txtReferencia.Enabled = false;
            cmbClientes.Enabled = false;
        }
        private void ListarServiciosModifica(int index)
        {
            string valores = dgvmep.Rows[index].Cells["Servicios"].FormattedValue.ToString();
            bool tieneVariosValores = false;
            string[] arrayValores = null;
            if (valores.Contains("-"))
            {
                arrayValores = valores.Split('-');
                tieneVariosValores = true;
            }
            if (valores.Contains("/"))
            {
                arrayValores = valores.Split('/');
                tieneVariosValores = true;
            }
            if (!tieneVariosValores)
            {
                arrayValores = new string[1];
                arrayValores[0] = valores;
            }

            //esta sera la lista que va a ser llenada con los productos que tiene la cotizacion seleccionada
            List<string> systemArray = new List<string>();
            List<Servicios> lista = new List<Servicios>();
            foreach (var v in arrayValores)
            {
                Servicios serviciosObj = new Servicios();
                serviciosObj = listaServicios.Where(x => x.Nombre.Equals(v)).FirstOrDefault();
                if (serviciosObj != null)
                {
                    lista.Add(serviciosObj);
                }
                else
                {
                    serviciosObj = listaServicios.Where(x => x.Nombre.Equals(v)).FirstOrDefault();
                    if (serviciosObj == null)
                    {
                        systemArray.Add(v);
                    }
                    else
                    {
                        lista.Add(serviciosObj);
                    }
                }
            }
            
            dgvProductos.DataSource = lista;
            List<string> nombreColumnas = new List<string>();
            nombreColumnas.Add("idProducto");
            nombreColumnas.Add("Cliente");
            nombreColumnas.Add("Color");
            nombreColumnas.Add("Marca");
            //OcultarCeldasDataGrid(dgvProductos, nombreColumnas);
            decimal totalDelDgv = Convert.ToDecimal(dgvmep.Rows[index].Cells["Total"].FormattedValue.ToString());
            listaDeServicios = lista;
            Montos(listaModificar: lista, montonDelDgv: totalDelDgv);
        }
        private void OcultaCeldas()
        {
            List<string> nombreColumnas = new List<string>();
            nombreColumnas.Add("Iva");
            nombreColumnas.Add("MontoExtra");
            nombreColumnas.Add("IdUsuario");
            OcultarCeldasDataGrid(dgvmep, nombreColumnas);
        }

        private void dgvmep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dialog = MessageBox.Show("¿Deseas eliminar este registro?", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dialog == DialogResult.No)
                {
                    return;
                }
                int index = dgvmep.SelectedCells[0].RowIndex;
                Cotizacion data = (Cotizacion) dgvmep.Rows[index].DataBoundItem;
                string resultado = cotizaciones.EliminarCotizacion(data.Referencia);
                if (resultado.Contains("Error"))
                {
                    MessageBox.Show(resultado, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show(resultado, "Modificado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                ListarCotizaciones();
            }
        }

        private void txtMontoPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPress_SoloNumeros(sender, e);
            //verifica si el usuario desea agregar una cotizacion sin un cliente que ya esta en el sistema y ademas que ya haya digitado un precio
            if (!cotizacionLista && !(sender as TextBox).Text.Equals(""))
            {
                cotizacionLista = true;
            }
            else
            {
                cotizacionLista = false;
            }
        }
        private void AgregarServicios(Servicios servicios)
        {
            //verifica si ya agrego esta placa anteriormente a esta misma cotizacion
            if (listaDeServicios.Where(x => x.idServicio.Equals(servicios.idServicio)).ToList().Count == 0)
            {
                listaDeServicios.Add(servicios);
            }
            else
            {
                MessageBox.Show("Ya tienes este producto agregado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            LlenarProductosGridView();
        }
        private void LlenarProductosGridView()
        {
            dgvProductos.DataSource = null;

            dgvProductos.DataSource = listaDeServicios;

        }
        private void btnAgregaProd_Click(object sender, EventArgs e)
        {
            string tipo = cmbServicios.Text;

            if (String.IsNullOrEmpty(tipo))
            {
                MessageBox.Show("Por favor elije un producto ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            List<Servicios> lista = servicios.ListarServicios();
            Servicios serviciosObj = lista.Where(x => x.Nombre.Equals(tipo)).FirstOrDefault();

            AgregarServicios(serviciosObj);
            Montos();
        }

        private void cmbProductos_TextUpdate(object sender, EventArgs e)
        {
            txtMontoPrecio.Text = "";
            lblPrecioIndividual.Text = "";
            txtMontoPrecio.Enabled = true;
        }

        private void cmbProductos_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Productos> lista = productosController.ListarProductos();
            string placa = "";
            if (cmbServicios.SelectedValue != null)
            {
                placa = cmbServicios.SelectedValue.ToString();
            }

            PrecioIndividual(placa, lista);
            Montos();
        }

        private void dgvProductos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dialog = MessageBox.Show("¿Deseas eliminar este producto?", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dialog == DialogResult.No)
                {
                    return;
                }
                int index = dgvProductos.SelectedCells[0].RowIndex;
                listaDeServicios.RemoveAt(index);
                LlenarProductosGridView();
                Montos(true);
                //LimpiarTodosControles();
                //txtMontoPrecio.Text = "0";
                //lblMontoExtra.Text = "0";
                //lblMontoIva.Text = "0";
                //lblMontoTotal.Text = "0";
                //lblPrecioIndividual.Text = "0";

                //cmbClientes.SelectedIndex = 0;
            }
        }
        private bool RegistroExistente()
        {
            if (listaCotizaciones.Where(x => x.Referencia.Equals(txtReferencia.Text)).FirstOrDefault() == null)
            {
                return false;
            }
            return true;
        }
        private void txtMontoExtra_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPress_SoloNumeros(sender,e);
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (!ValidacionInputs(controls()))
            {
                MessageBox.Show("Por favor ingresa toda la información requerida", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //verifica existencias en la base de datos del registro a modificar
            if (!RegistroExistente())
            {
                MessageBox.Show("No hemos podido encontrar este registro en la base de datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (cmbClientes.SelectedValue.ToString().Equals("0") && !cotizacionLista)
            {
                DialogResult dialog = MessageBox.Show("Estás apunto de crear una cotización sin cliente. ¿Deseas continuar?", "Cuidado", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialog == DialogResult.No)
                {
                    return;
                }
                else
                {
                    //habilitando el textbox de monto precio para cuando el usuario desea agregar una cotizacion para un cliente no registrado
                    txtMontoPrecio.Enabled = true;
                }
            }
            else
            {
                cotizacionLista = true;
            }


            string montoExtra = txtMontoExtra.Text;
            if (String.IsNullOrEmpty(montoExtra))
                montoExtra = "0";

            //toma la lista de todos los producto y los convierte en un formato Json
            string productosJson = JsonConvert.SerializeObject(listaDeServicios);

            string piezasDanadas = TomarPiezasDanadas();


            Cotizacion prod = new Cotizacion
            {
                Referencia = txtReferencia.Text.Trim(),
                Fecha = dtpFecha.Value,
                Observaciones = txtObservacion.Text,
                Productos = cmbProductos.Text,
                Iva = Convert.ToDecimal(lblMontoIva.Text),
                Total = Convert.ToDecimal(lblMontoTotal.Text),
                MontoExtra = Convert.ToDecimal(montoExtra),
                IdCliente = cmbClientes.SelectedValue.ToString(),
                IdUsuario = idUsuario,
                PiezasDanadas = piezasDanadas,
                Servicios = productosJson
            };
            string cotizacion = cotizaciones.ActualizarCotizacion(prod);
            if (cotizacion.Contains("Error"))
            {
                MessageBox.Show("Tuvimos un error en la creación de la cotización", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Cotización creada exitosamente", "Agregado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            LimpiarInputs(grbEnderezado, txtReferencia);
            ListarCotizaciones();
            //se inhabilita de nuevo el textbox del precio
            txtMontoPrecio.Enabled = false;
            txtReferencia.Text = Random();
            cmbClientes.Enabled = true;
            LimpiarTodosControles();
            txtMontoPrecio.Text = "0";
            lblMontoExtra.Text = "0";
            lblMontoIva.Text = "0";
            lblMontoTotal.Text = "0";
            lblPrecioIndividual.Text = "0";

            cmbClientes.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LimpiarTodosControles();
            for (int i = 0; i < clbPiezas.Items.Count; i++)
            {
                clbPiezas.SetItemChecked(i, false);
            }
            txtReferencia.Text = Random();
            cmbClientes.DataSource = null;

            cmbClientes.DataSource = listaClientes;
            cmbClientes.DisplayMember = "Nombre";
            cmbClientes.ValueMember = "Cedula";

            cmbClientes.Enabled = true;
            listaDeServicios = new List<Servicios>();

            txtMontoPrecio.Text = "0";
            lblMontoExtra.Text = "0";
            lblMontoIva.Text = "0";
            lblMontoTotal.Text = "0";
            lblPrecioIndividual.Text = "0";
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox meTxt = (TextBox)sender;
            string search = meTxt.Text.ToUpper();
            List<Cotizacion> listaParcialBusqueda = new List<Cotizacion>();
            List<Cotizacion> listaParcialBusquedaFinal = new List<Cotizacion>();
            if (meTxt.Text.Length == 0)
            {
                dgvmep.DataSource = listaCotizaciones;
            }
            if (listaCotizaciones.Count > 0)
            {
                listaParcialBusquedaFinal = new List<Cotizacion>();
                dgvmep.DataSource = listaParcialBusquedaFinal;
                listaParcialBusqueda = listaCotizaciones.Where(x => x.IdCliente.ToUpper().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaCotizaciones.Where(x => x.Total.ToString().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaCotizaciones.Where(x => x.Observaciones.ToUpper().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaCotizaciones.Where(x => x.Productos.ToUpper().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaCotizaciones.Where(x => x.PiezasDanadas.ToUpper().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusqueda = listaCotizaciones.Where(x => x.Referencia.ToUpper().Contains(search)).ToList();
                foreach (var i in listaParcialBusqueda)
                {
                    listaParcialBusquedaFinal.Add(i);
                }
                listaParcialBusquedaFinal = listaParcialBusquedaFinal.Distinct().ToList();
                dgvmep.DataSource = listaParcialBusquedaFinal;
            }
        }
    }
}
