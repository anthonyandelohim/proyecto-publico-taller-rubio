﻿using Body2.Controladora.Interface;
using Body2.Modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Body2.Controladora
{
    public class ProductosController
    {
        private int pantallaId = 0;
        private Acciones accionesObj;
        AccionesController acciones = new AccionesController();
        GeneralController<Productos> generalController = new GeneralController<Productos>();
        GeneralController<SysProductos> generalSysController = new GeneralController<SysProductos>();

        public ProductosController(string idUsuario)
        {
            accionesObj = new Acciones(idUsuario);
            PermisoHelper permiso = new PermisoHelper();
            //tomo el id de la pantalla tomando como referencia el nombre
            pantallaId = permiso.ListaPantallas().Where(x => x.NombrePantalla.Contains("Productos")).Select(x => x.idPantalla).FirstOrDefault();
        }

        public string AgregarProducto(Productos productosParam)
        {
            string resultado = "";
            bool exito = true;
            string error = "";
            try
            {
                List<Productos> listaProductos = new List<Productos>();
                listaProductos = ListarProductos();
                //aqui se revisa si ya existe un ID igual al que se coloca para el nuevo producto
                string existente = "";
                if (listaProductos != null && listaProductos.Count > 0)
                {
                    existente = listaProductos.Where(x => x.idProducto.Equals(productosParam.idProducto)).Select(y => y.idProducto).FirstOrDefault();
                    if (existente == null)
                        existente = "";
                }
                if (existente.Equals(""))
                {
                    List<Productos> lista = new List<Productos>();
                    lista.Add(productosParam);
                    
                    Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                    diccionario.Add("idProducto", SqlDbType.NVarChar);
                    diccionario.Add("NombreProducto", SqlDbType.NVarChar);
                    diccionario.Add("Color", SqlDbType.NVarChar);
                    diccionario.Add("Marca", SqlDbType.NVarChar);
                    diccionario.Add("Placa", SqlDbType.NVarChar);
                    diccionario.Add("Cliente", SqlDbType.NVarChar);
                    diccionario.Add("Modelo", SqlDbType.NVarChar);
                    resultado = generalController.AgregarModificar(lista, "SP_InsertarProductos", diccionario);
                }
                else
                {
                    exito = false;
                    resultado = "Error. Este producto ya existe. Por favor cambiar el ID";
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                if (!exito)
                {
                    error = "Error. Intento fallido por existencia";
                }
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Insert";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }

        public List<Productos> ListarProductos()
        {
            List<Productos> lista = new List<Productos>();
            try
            {
                lista = generalController.Listar("SP_ListarProductos", "Productos");
            }
            catch (Exception ex)
            {
            }
            return lista;
        }

        public string ActualizarProducto(Productos productosParam)
        {
            string resultado = "";
            string error = "";
            try
            {
                List<Productos> lista = new List<Productos>();
                lista.Add(productosParam);

                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("idProducto", SqlDbType.NVarChar);
                diccionario.Add("NombreProducto", SqlDbType.NVarChar);
                diccionario.Add("Color", SqlDbType.NVarChar);
                diccionario.Add("Marca", SqlDbType.NVarChar);
                diccionario.Add("Placa", SqlDbType.NVarChar);
                diccionario.Add("Cliente", SqlDbType.NVarChar);
                diccionario.Add("Modelo", SqlDbType.NVarChar);
                resultado = generalController.AgregarModificar(lista, "SP_ActualizarProductos", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Update";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }
        public string EliminarProducto(string idProducto)
        {
            string error = "";
            try
            {
                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("idProducto", SqlDbType.NVarChar);
                return generalController.Eliminar(idProducto, "SP_EliminarProductos", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return "Error. Lo sentimos, no hemos podido eliminar este producto";
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Delete";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }

        }

        public string EliminarSysProducto(string idProducto)
        {
            string error = "";
            try
            {
                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("producto", SqlDbType.NVarChar);
                return generalSysController.Eliminar(idProducto, "SP_EliminarSysProductos", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return "Error. Lo sentimos, no hemos podido eliminar este producto";
            }

        }

        public List<SysProductos> ListarSysProductos()
        {
            List<SysProductos> lista = new List<SysProductos>();
            try
            {
                lista = generalSysController.Listar("SP_ListarSysProductos", "SysProductos");
            }
            catch (Exception ex)
            {
            }
            return lista;
        }
        public SysProductos BuscarSysProductos(string producto)
        {
            SysProductos lista = new SysProductos();
            try
            {
                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("producto", SqlDbType.NVarChar);
                lista = generalSysController.Buscar(producto, "SP_BuscarSysRelacion", diccionario, "SysProductos");
            }
            catch (Exception ex)
            {
            }
            return lista;
        }
        public string AgregarSystemProducto(SysProductos productosParam)
        {
            string resultado = "";
            bool exito = true;
            string error = "";
            try
            {
                SysProductos sysObjeto = new SysProductos();
                sysObjeto = BuscarSysProductos(productosParam.Placa);
                //aqui se revisa si ya existe un ID igual al que se coloca para el nuevo producto
                string existente = "";
                if (sysObjeto != null && sysObjeto != null)
                {
                    if (sysObjeto.Placa.Equals(productosParam.Placa))
                    {
                        if (existente == null)
                            existente = "";
                    }
                    
                }
                if (existente.Equals(""))
                {
                    List<SysProductos> lista = new List<SysProductos>();
                    lista.Add(productosParam);

                    Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                    diccionario.Add("idCotizacion", SqlDbType.NVarChar);
                    diccionario.Add("producto", SqlDbType.NVarChar);
                    diccionario.Add("monto", SqlDbType.Decimal);
                    resultado = generalSysController.AgregarModificar(lista, "SP_AgregarSysProductos", diccionario);
                }
                else
                {
                    exito = false;
                    resultado = "Error. Este producto ya se utilizó para otro cliente. Por favor cambiar el ID";
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                if (!exito)
                {
                    error = "Error. Intento fallido por existencia";
                }
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Insert";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }

    }
}
