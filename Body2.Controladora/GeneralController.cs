﻿using Body2.Modelo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Body2.Controladora
{
    public class GeneralController<T> 
    {
        //instancia en donde se conecta a la base de datos
        readonly Datos conector = new Datos();

        /// <summary>
        /// La cantidad de objetoParam y el diccionario deben de ser iguales. Siempre
        /// </summary>
        /// <param name="objetoParam">Datos de lo que se va agregar</param>
        /// <param name="storeProcedure">Nombre del store procedure a ejecutar</param>
        /// <param name="diccionario">Aqui se coloca el nombre de los parametros y del tipo de objeto de cada uno</param>
        /// <returns></returns>
        /// 


        //Param 1 => toda la informacion que viene desde los textbox, combobox....desde el form....El T hace que cualquier objeto que le entre, lo
        //transforme en lo que yo le diga..la lista hace que se trabajen con muchos objetos a la vez, en lugar de ir uno a uno
        //Param 2 => Nombre del Procedimiento almacenado
        //Param 3 => valores a ingresar y el tipo de dato (SqlDbType)....toma el nombre en string, y el tipo de dato del parametro en SqlDbType
        public string AgregarModificar(List<T> objetoParam, string storeProcedure, Dictionary<string, SqlDbType> diccionario)
        {
            string resultado = "";
            try
            {
                List<string> listaParametros = new List<string>();
                List<Parametros> parametros = new List<Parametros>();
                
                //un ciclo para tomar los datos que vienen de objetoParam
                //al ser tipo Generico (T) puede tomar cualquier tipo de objeto. Pasamos lo que viene de la lista<t> a una lista de string
                foreach (var o in objetoParam[0].GetType().GetProperties())
                {
                    string obj = (o.GetValue(objetoParam[0], null).ToString());
                    listaParametros.Add(obj);
                }
                
                //contador que tomara la posicion de cada elemento
                //key sera el nombre del parametro y value el tipo del objeto en SQL (number, nvarchar, decimal, date, etc, etc)
                int contador = 0;
                foreach (string p in listaParametros)
                {
                    //se crea una lista de parametros en donde toma la informacion del parametro llamado diccionario, su info en
                    //las key y los values ya se explico que tiene cada una. Esta lista es que se necesita en la funcion EjecutarStoredProcedure
                    parametros.Add(new Parametros
                    {
                        Name = diccionario.ElementAt(contador).Key,
                        Value = p,
                        SqlType = diccionario.ElementAt(contador).Value
                    });

                    contador += 1;
                }
                //ejecutar store procedure y se le envia la lista de parametros
                conector.EjecutarStoredProcedure(storeProcedure, parametros, null,
                reader =>
                {
                    
                });

                resultado = "El objeto ha sido agregado exitosamente";
            }
            catch (Exception ex)
            {
                resultado = "Error. No se pudo agregar este objeto";
            }
            return resultado;
        }

        public string Eliminar(string id, string storeProcedure, Dictionary<string, SqlDbType> diccionario) {
            string resultado = "";
            try
            {
                conector.EjecutarStoredProcedure(storeProcedure, new List<Parametros>
                {
                    new Parametros {Name = diccionario.ElementAt(0).Key, Value = id, SqlType = diccionario.ElementAt(0).Value},
                }, null,
                reader => {

                });
                resultado = "El objeto ha sido eliminado exitosamente";
            }
            catch (Exception ex)
            {
                resultado = "Error. No se pudo eliminar este objeto";
            }
            return resultado;
        }
        public T Buscar(string id, string storeProcedure, Dictionary<string, SqlDbType> diccionario, string clase)
        {
            T resultado;
            try
            {
                switch (clase)
                {
                    case "SysProductos":
                        SysProductos productos = new SysProductos();
                        conector.EjecutarStoredProcedure(storeProcedure, new List<Parametros>
                        {
                            new Parametros {Name = diccionario.ElementAt(0).Key, Value = id, SqlType = diccionario.ElementAt(0).Value},
                        }, null,
                        reader => {
                            productos = (new SysProductos
                            {
                                Placa = Datos.ReadField<string>("idCliente", reader),
                                Monto = Datos.ReadField<decimal>("monto", reader)
                            });
                        });
                        return (T)Convert.ChangeType(productos, typeof(T));
                    default:
                        return (T)Convert.ChangeType(null, typeof(T));
                }
                
            }
            catch (Exception ex)
            {
                return (T)Convert.ChangeType(null, typeof(T));
            }
            
        }
        //param 1 => nombre del SP
        //param 2 => lo que deseo listar (productos, clientes, cotizaciones)
        public List<T> Listar(string storeProcedure, string tipoDato) {
            List<T> resultado = new List<T>();
            
            try
            {
                //el tener una sola funcion que lista todos los datos, se debe saber que tipo de dato debe listar
                switch (tipoDato)
                {
                    case "Servicios":
                        List<Servicios> servicios = new List<Servicios>();

                        conector.EjecutarStoredProcedure(storeProcedure, null, null,
                        reader => {
                            servicios.Add(new Servicios
                            {
                                idServicio = Datos.ReadField<string>("idServicio", reader),
                                Nombre = Datos.ReadField<string>("NombreServicio", reader),
                                CostoBase = Datos.ReadField<decimal>("CostoBase", reader)
                            });
                        });

                        //se convierte la lista en una generica. Al ser generica esta conversion funciona para retornar lo que esta funcion espera,
                        //pero en realidad se esta retornando un lista de servicios
                        return (List<T>)Convert.ChangeType(servicios, typeof(List<T>));
                    case "Productos":
                        List<Productos> productos = new List<Productos>();

                        conector.EjecutarStoredProcedure(storeProcedure, null, null,
                        reader => {
                            productos.Add(new Productos
                            {
                                idProducto = Datos.ReadField<string>("idProducto", reader),
                                NombreProducto = Datos.ReadField<string>("NombreProducto", reader),
                                Color = Datos.ReadField<string>("Color", reader),
                                Marca = Datos.ReadField<string>("Marca", reader),
                                Placa = Datos.ReadField<string>("Placa", reader),
                                Cliente = Datos.ReadField<string>("Cliente", reader),
                                Modelo = Datos.ReadField<string>("Modelo", reader)
                            });
                        });
                        
                        //se convierte la lista en una generica. Al ser generica esta conversion funciona para retornar lo que esta funcion espera,
                        //pero en realidad se esta retornando un lista de productos
                        return (List<T>)Convert.ChangeType(productos, typeof(List<T>));
                    case "SysProductos":
                        List<SysProductos> sysProductos = new List<SysProductos>();

                        conector.EjecutarStoredProcedure(storeProcedure, null, null,
                        reader => {
                            sysProductos.Add(new SysProductos
                            {
                                idCotizacion = Datos.ReadField<string>("idCotizacion", reader),
                                Placa = Datos.ReadField<string>("Producto", reader),
                                Monto = Datos.ReadField<decimal>("Monto", reader)
                            });
                        });
                        
                        //se convierte la lista en una generica. Al ser generica esta conversion funciona para retornar lo que esta funcion espera,
                        //pero en realidad se esta retornando un lista de productos
                        return (List<T>)Convert.ChangeType(sysProductos, typeof(List<T>));
                    case "Piezas":
                        List<Piezas> piezas = new List<Piezas>();
                        conector.EjecutarStoredProcedure(storeProcedure, null, null,
                        reader => {
                            piezas.Add(new Piezas
                            {
                                idPieza = Datos.ReadField<string>("idPieza", reader),
                                NombrePieza = Datos.ReadField<string>("Pieza", reader)
                            });
                        });
                        //se convierte la lista en una generica. Al ser generica esta conversion funciona para retornar lo que esta funcion espera,
                        //pero en realidad se esta retornando un lista de piezas
                        return (List<T>)Convert.ChangeType(piezas, typeof(List<T>));

                    case "Cliente":
                        List<Cliente> clientes = new List<Cliente>();
                        conector.EjecutarStoredProcedure(storeProcedure, null, null,
                        reader => {
                            clientes.Add(new Cliente
                            {
                                Cedula = Datos.ReadField<string>("cedula", reader),
                                Nombre = Datos.ReadField<string>("nombre", reader),
                                Correo = Datos.ReadField<string>("correo", reader),
                                Telefono = Datos.ReadField<string>("telefono", reader),
                                Domicilio = Datos.ReadField<string>("domicilio", reader),
                            });
                        });
                        //se convierte la lista en una generica. Al ser generica esta conversion funciona para retornar lo que esta funcion espera,
                        //pero en realidad se esta retornando un lista de clientes
                        return (List<T>)Convert.ChangeType(clientes, typeof(List<T>));

                    case "Cotizacion":
                        List<Cotizacion> cotizacion = new List<Cotizacion>();
                        conector.EjecutarStoredProcedure(storeProcedure, null, null,
                        reader => {

                            string productosJson = Datos.ReadField<string>("Servicios", reader);
                            string piezasJson = Datos.ReadField<string>("PiezasDanadas", reader);
                            //los productos vienen de la base de datos en formato Json, por lo que lo convierto a una lista de productos
                            List<Servicios> listaProducto = new List<Servicios>();
                            //verifico que productosJson no venga vacio
                            if (!String.IsNullOrEmpty(productosJson))
                            {
                                listaProducto = JsonConvert.DeserializeObject<List<Servicios>>(productosJson);
                            }
                            string productosString = "";
                            foreach (var p in listaProducto)
                            {
                                productosString = productosString + p.Nombre + "/";
                            }
                            //mismo procedimiento de los producto ahora con las piezas
                            List<Piezas> listaPiezas = new List<Piezas>();
                            if (!String.IsNullOrEmpty(piezasJson))
                            {
                                listaPiezas = JsonConvert.DeserializeObject<List<Piezas>>(piezasJson);
                            }
                            string piezasString = "";
                            foreach (var p in listaPiezas)
                            {
                                piezasString = piezasString + p.NombrePieza + "/";
                            }
                            cotizacion.Add(new Cotizacion
                            {
                                Referencia = Datos.ReadField<string>("Referencia", reader),
                                Fecha= Datos.ReadField<DateTime>("Fecha", reader),
                                Observaciones = Datos.ReadField<string>("Observaciones", reader),
                                Productos = Datos.ReadField<string>("idProducto", reader),
                                Iva = Datos.ReadField<decimal>("Iva", reader),
                                Total = Datos.ReadField<decimal>("Total", reader),
                                MontoExtra = Datos.ReadField<decimal>("MontoExtra", reader),
                                IdCliente = Datos.ReadField<string>("IdCliente", reader),
                                IdUsuario = Datos.ReadField<string>("IdUsuario", reader),
                                PiezasDanadas = piezasString.TrimEnd('/'),
                                Servicios = productosString.TrimEnd('/'),
                            });
                        });
                        //se convierte la lista en una generica. Al ser generica esta conversion funciona para retornar lo que esta funcion espera,
                        //pero en realidad se esta retornando un lista de cotizaciones
                        return (List<T>)Convert.ChangeType(cotizacion, typeof(List<T>));

                    case "Roles":
                        List<Rol> roles = new List<Rol>();
                        conector.EjecutarStoredProcedure(storeProcedure, null, null,
                        reader => {

                            string productosJson = Datos.ReadField<string>("pantallas", reader);
                            //los productos vienen de la base de datos en formato Json, por lo que lo convierto a una lista de pantallas
                            List<Pantalla> listaProducto = new List<Pantalla>();
                            //verifico que productosJson no venga vacio
                            if (!String.IsNullOrEmpty(productosJson))
                            {
                                listaProducto = JsonConvert.DeserializeObject<List<Pantalla>>(productosJson);
                            }
                             
                            string productosString = "";
                            foreach (var p in listaProducto)
                            {
                                productosString = productosString + p.NombrePantalla + " - ";
                            }
                            roles.Add(new Rol
                            {
                                idRol = Datos.ReadField<int>("codigo_rol", reader),
                                NombreRol = Datos.ReadField<string>("nombre_rol", reader),
                                Descripcion = Datos.ReadField<string>("descripcion", reader),
                                Estado = Datos.ReadField<string>("estado", reader),
                                Pantallas = productosString.TrimEnd()
                            });
                        });
                        //se convierte la lista en una generica. Al ser generica esta conversion funciona para retornar lo que esta funcion espera,
                        //pero en realidad se esta retornando un lista de roles
                        return (List<T>)Convert.ChangeType(roles, typeof(List<T>));

                    case "Usuario":
                        List<Usuario> usuarios = new List<Usuario>();
                        conector.EjecutarStoredProcedure(storeProcedure, null, null,
                        reader => {
                            usuarios.Add(new Usuario
                            {
                                Nombre = Datos.ReadField<string>("nombre", reader),
                                User = Datos.ReadField<string>("usuario", reader),
                                idRol = Datos.ReadField<string>("codigo_rol2", reader),
                                Clave = Datos.ReadField<string>("clave", reader)
                            });
                        });
                        //se convierte la lista en una generica. Al ser generica esta conversion funciona para retornar lo que esta funcion espera,
                        //pero en realidad se esta retornando un lista de usuarios
                        return (List<T>)Convert.ChangeType(usuarios, typeof(List<T>));

                    case "Pantalla":
                        List<Pantalla> pantallas = new List<Pantalla>();
                        conector.EjecutarStoredProcedure(storeProcedure, null, null,
                        reader => {
                            pantallas.Add(new Pantalla
                            {
                                idPantalla = Datos.ReadField<int>("idPantalla", reader),
                                NombrePantalla = Datos.ReadField<string>("NombrePantalla", reader),
                            });
                        });
                        //se convierte la lista en una generica. Al ser generica esta conversion funciona para retornar lo que esta funcion espera,
                        //pero en realidad se esta retornando un lista de pantallas
                        return (List<T>)Convert.ChangeType(pantallas, typeof(List<T>));

                    default:
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }
        
    }
}
