﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace Body2.Controladora
{
    public class UsuarioHelper
    {
        private int pantallaId = 0;
        private Acciones accionesObj;
        AccionesController acciones = new AccionesController();
        GeneralController<Usuario> generalController = new GeneralController<Usuario>();

        public UsuarioHelper(string idUsuario)
        {
            accionesObj = new Acciones(idUsuario);
            PermisoHelper permiso = new PermisoHelper();
            //tomo el id de la pantalla tomando como referencia el nombre
            pantallaId = permiso.ListaPantallas().Where(x => x.NombrePantalla.Contains("Usuarios")).Select(x => x.idPantalla).FirstOrDefault();
        }

        public string AgregarUsuario(Usuario UsuarioParam)
        {
            string resultado = "";
            bool exito = true;
            string error = "";
            try
            {

                UsuarioParam.User = UsuarioParam.User.Replace("-", String.Empty);
                List<Usuario> listaUsuario = new List<Usuario>();
                listaUsuario = ListarUsuario();
                //aqui se revisa si ya existe un ID igual al que se coloca para el nuevo Usuario
                string existente = "";
                if (listaUsuario != null && listaUsuario.Count > 0)
                {
                    existente = listaUsuario.Where(x => x.User.Equals(UsuarioParam.User)).Select(y => y.User).FirstOrDefault();
                    if (existente == null)
                        existente = "";
                }
                if (existente.Equals(""))
                {
                    //encriptacion
                    UsuarioParam.Clave = EncyptDescryp.Encrypt(UsuarioParam.Clave);

                    List<Usuario> lista = new List<Usuario>();
                    lista.Add(UsuarioParam);

                    Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                    diccionario.Add("Nombre", SqlDbType.NVarChar);
                    diccionario.Add("Usuario", SqlDbType.NVarChar);
                    diccionario.Add("Clave", SqlDbType.NVarChar);
                    diccionario.Add("Codigo_rol", SqlDbType.NVarChar);
                    resultado = generalController.AgregarModificar(lista, "SP_InsertarUsuario", diccionario);
                }
                else
                {
                    exito = false;
                    resultado = "Error. Esta usuario ya existe. Por favor utilice otro nombre para la opción Usuario";
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                if (!exito)
                {
                    error = "Error. Intento fallido por existencia";
                }
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Insert";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }

        public List<Usuario> ListarUsuario()
        {
            List<Usuario> lista = new List<Usuario>();
            try
            {
                lista = generalController.Listar("SP_ListarUsuario", "Usuario");
            }
            catch (Exception ex)
            {
            }
            return lista;
        }

        public string ActualizarUsuario(Usuario UsuarioParam)
        {
            string resultado = "";
            string error = "";
            try
            {

                string claveActual = UsuarioParam.Clave;
                bool cambioDeClave = false;
                if (claveActual.Length % 4 == 0)
                {
                    UsuarioParam.Clave = EncyptDescryp.Decrypt(UsuarioParam.Clave);
                    UsuarioParam.Clave = EncyptDescryp.Encrypt(UsuarioParam.Clave);
                }
                else
                {
                    cambioDeClave = true;
                    UsuarioParam.Clave = EncyptDescryp.Encrypt(UsuarioParam.Clave);
                }

                UsuarioParam.User = UsuarioParam.User.Replace("-", String.Empty);
                List<Usuario> lista = new List<Usuario>();
                lista.Add(UsuarioParam);

                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("Nombre", SqlDbType.NVarChar);
                diccionario.Add("Usuario", SqlDbType.NVarChar);
                diccionario.Add("Clave", SqlDbType.NVarChar);
                diccionario.Add("Codigo_rol", SqlDbType.NVarChar);
                resultado = generalController.AgregarModificar(lista, "SP_ActualizarUsuario", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Update";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }
        public string EliminarUsuario(string idUsuario)
        {
            string error = "";
            try
            {
                idUsuario = idUsuario.Replace("-", String.Empty);
                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("idUsuario", SqlDbType.NVarChar);
                return generalController.Eliminar(idUsuario, "SP_EliminarUsuario", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return "Error. Lo sentimos, no hemos podido eliminar este usuario";
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Delete";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }

        }

    }

}
