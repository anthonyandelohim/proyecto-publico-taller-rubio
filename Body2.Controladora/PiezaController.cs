﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Body2.Controladora
{
    public class PiezaController
    {
        private int pantallaId = 0;
        private Acciones accionesObj;
        AccionesController acciones = new AccionesController();
        GeneralController<Piezas> generalController = new GeneralController<Piezas>();

        public PiezaController(string idUsuario)
        {
            accionesObj = new Acciones(idUsuario);
            PermisoHelper permiso = new PermisoHelper();
            //tomo el id de la pantalla tomando como referencia el nombre
            pantallaId = permiso.ListaPantallas().Where(x => x.NombrePantalla.Contains("Piezas")).Select(x => x.idPantalla).FirstOrDefault();
        }

        public string AgregarPieza(Piezas PiezasParam)
        {
            string resultado = "";
            bool exito = true;
            string error = "";
            try
            {
                List<Piezas> listaPiezas = new List<Piezas>();
                listaPiezas = ListarPiezas();
                //aqui se revisa si ya existe un ID igual al que se coloca para el nuevo Pieza
                string existente = "";
                if (listaPiezas != null && listaPiezas.Count > 0)
                {
                    existente = listaPiezas.Where(x => x.idPieza.Equals(PiezasParam.idPieza)).Select(y => y.idPieza).FirstOrDefault();
                    if (existente == null)
                        existente = "";
                }
                if (existente.Equals(""))
                {
                    List<Piezas> lista = new List<Piezas>();
                    lista.Add(PiezasParam);

                    Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                    diccionario.Add("idPieza", SqlDbType.NVarChar);
                    diccionario.Add("NombrePieza", SqlDbType.NVarChar);
                    resultado = generalController.AgregarModificar(lista, "SP_InsertarPiezas", diccionario);
                }
                else
                {
                    exito = false;
                    resultado = "Error. Esta Pieza ya existe. Por favor cambiar el ID";
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                if (!exito)
                {
                    error = "Error. Intento fallido por existencia";
                }
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Insert";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }

        public List<Piezas> ListarPiezas()
        {
            List<Piezas> lista = new List<Piezas>();
            try
            {
                lista = generalController.Listar("SP_ListarPiezas", "Piezas");
            }
            catch (Exception ex)
            {
            }
            return lista;
        }

        public string ActualizarPieza(Piezas PiezasParam)
        {
            string resultado = "";
            string error = "";
            try
            {
                List<Piezas> lista = new List<Piezas>();
                lista.Add(PiezasParam);

                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("idPieza", SqlDbType.NVarChar);
                diccionario.Add("NombrePieza", SqlDbType.NVarChar);
                resultado = generalController.AgregarModificar(lista, "SP_ActualizarPiezas", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Update";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }
        public string EliminarPieza(string idPieza)
        {
            string error = "";
            try
            {
                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("idPieza", SqlDbType.NVarChar);
                return generalController.Eliminar(idPieza, "SP_EliminarPiezas", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return "Error. Lo sentimos, no hemos podido eliminar esta pieza";
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Insert";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            
        }
    }
}
