﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Body2.Controladora
{
    public class ServiciosHelper
    {
        private int pantallaId = 0;
        private Acciones accionesObj;
        AccionesController acciones = new AccionesController();
        GeneralController<Servicios> generalController = new GeneralController<Servicios>();

        public ServiciosHelper(string idServicios)
        {
            accionesObj = new Acciones(idServicios);
            PermisoHelper permiso = new PermisoHelper();
            //tomo el id de la pantalla tomando como referencia el nombre
            pantallaId = permiso.ListaPantallas().Where(x => x.NombrePantalla.Contains("Servicios")).Select(x => x.idPantalla).FirstOrDefault();
        }

        public string AgregarServicios(Servicios ServiciosParam)
        {
            string resultado = "";
            bool exito = true;
            string error = "";
            try
            {
                List<Servicios> listaServicios = new List<Servicios>();
                listaServicios = ListarServicios();
                //aqui se revisa si ya existe un ID igual al que se coloca para el nuevo Servicios
                string existente = "";
                if (listaServicios != null && listaServicios.Count > 0)
                {
                    existente = listaServicios.Where(x => x.idServicio.Replace(" ", String.Empty).Equals(ServiciosParam.idServicio.Replace(" ", String.Empty))).Select(y => y.idServicio).FirstOrDefault();
                    if (existente == null)
                        existente = "";
                }
                if (existente.Equals(""))
                {

                    List<Servicios> lista = new List<Servicios>();
                    lista.Add(ServiciosParam);

                    Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                    diccionario.Add("idServicios", SqlDbType.NVarChar);
                    diccionario.Add("NombreServicios", SqlDbType.NVarChar);
                    diccionario.Add("CostoBase", SqlDbType.Decimal);
                    resultado = generalController.AgregarModificar(lista, "SP_InsertarServicios", diccionario);
                }
                else
                {
                    exito = false;
                    resultado = "Error. Este servicio ya existe. Por favor utilice otro nombre para la opción Servicios";
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                if (!exito)
                {
                    error = "Error. Intento fallido por existencia";
                }
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Insert";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }

        public List<Servicios> ListarServicios()
        {
            List<Servicios> lista = new List<Servicios>();
            try
            {
                lista = generalController.Listar("SP_ListarServicios", "Servicios");
            }
            catch (Exception ex)
            {
            }
            return lista;
        }

        public string ActualizarServicios(Servicios ServiciosParam)
        {
            string resultado = "";
            string error = "";
            try
            {
                List<Servicios> lista = new List<Servicios>();
                lista.Add(ServiciosParam);

                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("idServicios", SqlDbType.NVarChar);
                diccionario.Add("NombreServicios", SqlDbType.NVarChar);
                diccionario.Add("CostoBase", SqlDbType.Decimal);
                resultado = generalController.AgregarModificar(lista, "SP_ActualizarServicios", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Update";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }
        public string EliminarServicios(string idServicios)
        {
            string error = "";
            try
            {
                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("idServicios", SqlDbType.NVarChar);
                return generalController.Eliminar(idServicios, "SP_EliminarServicios", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return "Error. Lo sentimos, no hemos podido eliminar este Servicios";
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Delete";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
        }
    }
}
