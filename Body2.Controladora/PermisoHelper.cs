﻿

using System;
using System.Collections.Generic;

namespace Body2.Controladora
{
    public class PermisoHelper
    {
        GeneralController<Pantalla> pantallaController = new GeneralController<Pantalla>();

        public List<Pantalla> ListaPantallas()
        {
            List<Pantalla> lista = new List<Pantalla>();
            try
            {
                lista = pantallaController.Listar("SP_ListarPantallas", "Pantalla");
            }
            catch (Exception)
            {

            }
            return lista;
        }
    }
}
