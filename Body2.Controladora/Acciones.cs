﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Body2.Controladora
{
    public class Acciones
    {
        public string idUsuario { get; set; }
        public int Pantalla { get; set; }
        public DateTime Fecha { get; set; }
        public string Accion { get; set; }
        public string Detalle { get; set; }

        public Acciones(string idUsuario)
        {
            this.idUsuario = idUsuario;
        }
    }
}
