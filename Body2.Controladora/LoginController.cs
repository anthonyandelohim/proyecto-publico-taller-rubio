﻿using Body2.Modelo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Body2.Controladora
{
    public class LoginController
    {
        readonly private int pantallaId = 0;
        readonly Datos conector = new Datos(); 
        private Acciones accionesObj;
        readonly AccionesController acciones = new AccionesController();

        public LoginController()
        {

            PermisoHelper permiso = new PermisoHelper();
            //tomo el id de la pantalla tomando como referencia el nombre
            pantallaId = permiso.ListaPantallas().Where(x => x.NombrePantalla.Contains("Login")).Select(x => x.idPantalla).FirstOrDefault();
        }

        public Dictionary<string, List<Pantalla>> LoginUsuario(string usuario, string password) {
            //este metodo es llamado para logguear al usuario, si al final encuentra al usuario y la clave, la funcion va a retornar un diccionario
            //donde Dictionary<string, List<Pantalla>> => string = sera el nombre del usuario y List<pantalla> => la lista de mis pantallas
            Dictionary<string, List<Pantalla>> resultado = new Dictionary<string, List<Pantalla>>();
            
            string error = "";
            string nombre = "";
            string userName = "";
            string estadoDeRol = "";
            List<Usuario> usuarios = new List<Usuario>();
            try
            {
                conector.EjecutarStoredProcedure("SP_Login", null, null,
                reader =>
                {
                    usuarios.Add(new Usuario{
                        idRol = Datos.ReadField<string>("codigo_rol2", reader),
                        Nombre = Datos.ReadField<string>("nombre", reader),
                        Clave = Datos.ReadField<string>("clave", reader),
                        User = Datos.ReadField<string>("usuario", reader),
                    });
                });
                //aqui toma todos los usuarios obtenido y por medio de un ciclo empieza a desencriptar las contrasenas
                foreach (var u in usuarios)
                {
                    usuarios[usuarios.FindIndex(i => i.Clave.Equals(u.Clave))].Clave = EncyptDescryp.Decrypt(u.Clave);
                }
                //verifica si el usuario y la contrasena coinciden con la informacion que vino de la base de datos ya con la clave desencriptada
                Usuario usuarioLogin = usuarios.Where(x => x.Clave.Equals(password) && x.User.Replace("-",String.Empty).Equals(usuario)).FirstOrDefault();
                //si no hay resultados devuelve un objeto nulo
                if (usuarioLogin == null)
                {
                    nombre = "Error. Por favor ingresa correctamente tus datos";
                    resultado.Add(nombre, null);
                    return resultado;
                }
                userName = usuarioLogin.User;
                nombre = usuarioLogin.Nombre;
                //verifica si el rol esta activo
                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("Usuario", SqlDbType.NVarChar);
                conector.EjecutarStoredProcedure("SP_UsuarioRolActivo", new List<Parametros>
                    {
                        new Parametros {Name = diccionario.ElementAt(0).Key, Value = userName, SqlType = diccionario.ElementAt(0).Value},
                    }, null,
                reader =>
                {
                    estadoDeRol = Datos.ReadField<string>("estado", reader);
                });

                if (estadoDeRol.Equals("0"))
                {
                    nombre = "Error. Lo sentimos, estás intentando ingresar con un rol inhabilitado, ponte en contacto con el administrador";
                    error = "Intento de ingreso con rol inactivo";
                    resultado.Add(nombre, null);
                    return resultado;
                }
                //devuelve un diccionario en donde el key (1er valor) es el idRol y el value (2do valor) es el nombre del usuario
                resultado.Add(nombre, GetPantallas(usuarioLogin.User));
                error = "Ingreso al sistema del usuario: " + usuario;

            }
            catch (Exception ex)
            {
                error = ex.Message;
                nombre = "Error. Por favor ingresa correctamente tus datos";
                resultado.Add(nombre, null);
            }
            finally
            {
                if (String.IsNullOrEmpty(error))
                {
                    error = "Intento de ingreso fallido. Usuario: " + usuario;
                }
                accionesObj = new Acciones(userName);
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Login";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }
        private List<Pantalla> GetPantallas(string Usuario) {
            List<Pantalla> pantallas = new List<Pantalla>();
            try
            {
                string pantallasJson = "";

                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("Usuario", SqlDbType.NVarChar);
                conector.EjecutarStoredProcedure("SP_LoginJoin", new List<Parametros>
                {
                    new Parametros {Name = diccionario.ElementAt(0).Key, Value = Usuario, SqlType = diccionario.ElementAt(0).Value},
                }, null,
                    reader => {
                        pantallasJson = Datos.ReadField<string>("pantallas", reader);
                    });
                if (!String.IsNullOrEmpty(pantallasJson))
                {
                    pantallas = JsonConvert.DeserializeObject<List<Pantalla>>(pantallasJson);
                }
            }
            catch (Exception)
            {

            }
            
            return pantallas;
        }
        public string CloseSystem(string userName)
        {
            try
            {
                accionesObj = new Acciones(userName);
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Exit";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = "Log Out de usuario " + userName;
                acciones.SysAcciones(accionesObj);
            }
            catch (Exception ex)
            {

            }
            return "¡Hasta pronto!";
        }
    }
}
