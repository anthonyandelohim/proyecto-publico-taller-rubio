﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Body2.Modelo;

namespace Body2.Controladora
{

    public class Cotizacion
    {
        public string Referencia { get; set; }
        public DateTime Fecha { get; set; }
        public string Observaciones { get; set; }
        public string Productos { get; set; }
        public decimal Iva { get; set; }
        public decimal Total { get; set; }
        public decimal MontoExtra { get; set; }
        public string IdCliente { get; set; }
        public string IdUsuario { get; set; }
        public string PiezasDanadas { get; set; }
        public string Servicios { get; set; }
    }
}

