﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Body2.Controladora
{
    public class Productos
    {
        public string idProducto { get; set; }
        public string NombreProducto { get; set; }
        //public decimal PrecioProducto { get; set; }
        public string Color { get; set; }
        public string Marca { get; set; }
        public string Placa { get; set; }
        public string Cliente { get; set; }
        public string Modelo { get; set; }
    }

    public class SysProductos
    {
        public string idCotizacion { get; set; }
        public string Placa { get; set; }
        public decimal Monto { get; set; }
    }
}
