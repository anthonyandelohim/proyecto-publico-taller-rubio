﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Body2.Modelo;

namespace Body2.Controladora
{
    public class ClienteHelper
    {
        readonly private int pantallaId = 0;
        readonly private Acciones accionesObj;
        readonly AccionesController acciones = new AccionesController();
        readonly GeneralController<Cliente> generalController = new GeneralController<Cliente>();

        public ClienteHelper(string idUsuario)
        {
            accionesObj = new Acciones(idUsuario);
            PermisoHelper permiso = new PermisoHelper();
            //tomo el id de la pantalla tomando como referencia el nombre
            pantallaId = permiso.ListaPantallas().Where(x => x.NombrePantalla.Contains("Cliente")).Select(x => x.idPantalla).FirstOrDefault();
        }

        public string AgregarCliente(Cliente ClienteParam)
        {
            string resultado = "";
            string error = "";
            bool exito = true;
            try
            {
                List<Cliente> listaCliente = new List<Cliente>();
                listaCliente = ListarCliente();
                //aqui se revisa si ya existe un ID igual al que se coloca para el nuevo Pieza
                string existente = "";
                if (listaCliente != null && listaCliente.Count > 0)
                {
                    foreach (var c in listaCliente)
                    {
                        if (c.Cedula.Equals(ClienteParam.Cedula))
                        {
                            existente = c.Cedula;
                        }
                    }
                    if (existente == null)
                        existente = "";
                }
                if (existente.Equals(""))
                {
                    List<Cliente> lista = new List<Cliente>
                    {
                        ClienteParam
                    };

                    Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>
                    {
                        //estos son los parametros que espera el store procedure
                        { "Cedula", SqlDbType.NVarChar },
                        { "NombreCliente", SqlDbType.NVarChar },
                        { "Telefono", SqlDbType.NVarChar },
                        { "Correo", SqlDbType.NVarChar },
                        { "Domicilio", SqlDbType.NVarChar }
                    };
                    resultado = generalController.AgregarModificar(lista, "SP_InsertarCliente", diccionario);
                }
                else
                {
                    exito = false;
                    resultado = "Error. Este cliente ya existe. Por favor cambiar el ID";
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                if (!exito)
                {
                    error = "Error. Intento fallido por existencia";
                }
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Insert";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }

        public List<Cliente> ListarCliente()
        {
            List<Cliente> lista = new List<Cliente>();
            try
            {
                //como se llama mi SP  y que quiero que liste
                lista = generalController.Listar("SP_ListarCliente", "Cliente");
            }
            catch (Exception ex)
            {
                return lista;
            }
            return lista;
        }

        public string ActualizarCliente(Cliente ClienteParam)
        {
            string resultado = "";
            string error = "";
            try
            {
                List<Cliente> lista = new List<Cliente>
                {
                    ClienteParam
                };

                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>
                {
                    //este orden va con relacion al orden en el que viene la lista de cliente
                    //si el nombre viene de primero, el primero parametro sera el nombre y asi sucesivamente
                    { "NombreCliente", SqlDbType.NVarChar },
                    { "Cedula", SqlDbType.NVarChar },
                    { "Telefono", SqlDbType.NVarChar },
                    { "Correo", SqlDbType.NVarChar },
                    { "Domicilio", SqlDbType.NVarChar }
                };
                resultado = generalController.AgregarModificar(lista, "SP_ActualizarCliente", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Update";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }
        public string EliminarCliente(string cedula)
        {

            string error = "";
            try
            {
                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("Cedula", SqlDbType.NVarChar);
                return generalController.Eliminar(cedula, "SP_EliminarCliente", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return "Error. Lo sentimos, no hemos podido eliminar este cliente";
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Delete";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            
        }
    }
}