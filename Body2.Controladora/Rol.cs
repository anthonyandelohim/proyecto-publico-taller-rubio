﻿

namespace Body2.Controladora
{
    public class Rol
    {
        public int idRol { get; set; }
        public string NombreRol { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public string Pantallas { get; set; }

    }
}
