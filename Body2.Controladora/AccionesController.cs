﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Body2.Controladora
{
    public class AccionesController
    {
        GeneralController<Acciones> generalController = new GeneralController<Acciones>();

        public void SysAcciones(Acciones acciones)
        {
            try
            {

                if (acciones.idUsuario != null)
                {
                    acciones.idUsuario = acciones.idUsuario.Replace("-", String.Empty);
                }

                List<Acciones> lista = new List<Acciones>();
                lista.Add(acciones);

                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("idUsuario", SqlDbType.NVarChar);
                diccionario.Add("Pantalla", SqlDbType.Int);
                diccionario.Add("Fecha", SqlDbType.DateTime);
                diccionario.Add("Accion", SqlDbType.NVarChar);
                diccionario.Add("Detalle", SqlDbType.NVarChar);
                generalController.AgregarModificar(lista, "SP_InsertarAcciones", diccionario);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
