﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Body2.Controladora
{
    public class RolHelper
    {
        private int pantallaId = 0;
        private Acciones accionesObj;
        AccionesController acciones = new AccionesController();
        GeneralController<Rol> generalController = new GeneralController<Rol>();

        public RolHelper(string idUsuario)
        {
            accionesObj = new Acciones(idUsuario);
            PermisoHelper permiso = new PermisoHelper();
            //tomo el id de la pantalla tomando como referencia el nombre
            pantallaId = permiso.ListaPantallas().Where(x => x.NombrePantalla.Contains("Roles")).Select(x => x.idPantalla).FirstOrDefault();
        }

        public string AgregarRol(Rol RolParam)
        {
            string resultado = "";
            bool exito = true;
            string error = "";
            try
            {
                List<Rol> listaRol = new List<Rol>();
                listaRol = ListarRol();
                //aqui se revisa si ya existe un ID igual al que se coloca para el nuevo Rol
                string existente = "";
                if (listaRol != null && listaRol.Count > 0)
                {
                    existente = listaRol.Where(x => x.NombreRol.Equals(RolParam.NombreRol)).Select(y => y.NombreRol).FirstOrDefault();
                    if (existente == null)
                        existente = "";
                }
                if (existente.Equals(""))
                {
                    List<Rol> lista = new List<Rol>();
                    lista.Add(RolParam);

                    Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                    diccionario.Add("CodigoRol", SqlDbType.Int);
                    diccionario.Add("Nombre_rol", SqlDbType.NVarChar);
                    diccionario.Add("Descripcion", SqlDbType.NVarChar);
                    diccionario.Add("Estado", SqlDbType.NVarChar);
                    diccionario.Add("Pantallas", SqlDbType.NVarChar);
                    resultado = generalController.AgregarModificar(lista, "SP_InsertarRol", diccionario);
                }
                else
                {
                    exito = false;
                    resultado = "Error. Este rol ya existe. Por favor utilice otro nombre para este rol";
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                if (!exito)
                {
                    error = "Error. Intento fallido por existencia";
                }
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Insert";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }

        public List<Rol> ListarRol()
        {
            List<Rol> lista = new List<Rol>();
            try
            {
                lista = generalController.Listar("SP_ListarRol", "Roles");
            }
            catch (Exception ex)
            {
            }
            return lista;
        }

        public string ActualizarRol(Rol RolParam)
        {
            string resultado = "";
            string error = "";
            try
            {
                List<Rol> lista = new List<Rol>();
                lista.Add(RolParam);

                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("CodigoRol", SqlDbType.Int);
                diccionario.Add("Nombre_rol", SqlDbType.NVarChar);
                diccionario.Add("Descripcion", SqlDbType.NVarChar);
                diccionario.Add("Estado", SqlDbType.NVarChar);
                diccionario.Add("Pantallas", SqlDbType.NVarChar);
                resultado = generalController.AgregarModificar(lista, "SP_ActualizarRol", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Update";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }
        public string EliminarRol(string Rol, List<Usuario> usuarios)
        {
            string error = "";
            try
            {
                List<Rol> listaRoles = new List<Rol>();
                listaRoles = ListarRol();

                string idRol = listaRoles.Where(x => x.NombreRol.Equals(Rol)).Select(x => x.idRol).FirstOrDefault().ToString();
                string rolOcupado = usuarios.Where(x => x.idRol.Equals(idRol)).Select(x => x.Nombre).FirstOrDefault();
                if (!String.IsNullOrEmpty(rolOcupado))
                {
                    return "Error. Este rol es imposbiel de eliminar porque se encuentra ocupado por un usuario.";
                }

                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                int idRolInt = Convert.ToInt32(idRol);
                diccionario.Add("idRol", SqlDbType.Int);
                return generalController.Eliminar(idRol, "SP_EliminarRol", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return "Error. Lo sentimos, no hemos podido eliminar este rol";
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Delete";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            
        }
    }
}
