﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Body2.Controladora.Fabrica
{
    public interface IFabricacion
    {
        Cotizacion crearCotizaciones(Cotizacion lista);
    }
}
