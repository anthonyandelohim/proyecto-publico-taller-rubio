﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Body2.Controladora
{
    public class Servicios
    {
        public string idServicio { get; set; }
        public string Nombre { get; set; }
        public decimal CostoBase { get; set; }
    }
}
