﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Body2.Controladora
{
    public class ReportesController
    {
        private string accion = "";
        private int pantallaId = 0;
        private Acciones accionesObj;
        private string idUsuario = "";
        AccionesController acciones = new AccionesController();
        public ReportesController(string idUsuario, string accion)
        {
            this.accion = accion;
            this.idUsuario = idUsuario;
            accionesObj = new Acciones(idUsuario);
            ReportesAcciones();
        }
        private void ReportesAcciones()
        {
            PermisoHelper permiso = new PermisoHelper();
            //tomo el id de la pantalla tomando como referencia el nombre
            pantallaId = permiso.ListaPantallas().Where(x => x.NombrePantalla.Contains("Reportes")).Select(x => x.idPantalla).FirstOrDefault();
            accionesObj.Pantalla = pantallaId;
            accionesObj.Accion = "Insert";
            accionesObj.Fecha = DateTime.Now;
            accionesObj.Detalle = accion;
            acciones.SysAcciones(accionesObj);
        }
    }
}
