﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Body2.Modelo;

namespace Body2.Controladora
{
    public class Usuario
    {
        public string Nombre { get; set; }
        public string User { get; set; }
        public string Clave { get; set; }
        public string idRol { get; set; }
    }
}
