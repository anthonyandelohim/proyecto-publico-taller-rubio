﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Body2.Controladora.Fabrica;
using Body2.Modelo;

namespace Body2.Controladora
{
    public class CotizacionHelper: ConcretarFabricacion
    {
        GeneralController<Cotizacion> generalController = new GeneralController<Cotizacion>();
        AccionesController acciones = new AccionesController();
        private Acciones accionesObj;
        private int pantallaId = 0;


        //esta es una implementacion de una clase abstracta. Mi clase abstracta es ConcretarFabricacion
        //Tiene el principio de Facroty Pattern https://refactoring.guru/design-patterns/factory-method

        public CotizacionHelper(string idUsuario)
        {
            accionesObj = new Acciones(idUsuario);
            PermisoHelper permiso = new PermisoHelper();
            pantallaId = permiso.ListaPantallas().Where(x => x.NombrePantalla.Contains("Enderezado")).Select(x => x.idPantalla).FirstOrDefault();
        }

        public string RevisarPlacaCliente(List<Productos> productos, string cedula, string cliente, string placa)
        {
            try
            {
                Productos placaExiste = productos.Where(x => x.Placa.ToUpper().Equals(placa.ToUpper())).FirstOrDefault();
                if (placaExiste != null)
                {
                    if (placaExiste.Cliente.Replace("-", String.Empty) == cedula)
                    {
                        return "";
                    }
                    else
                    {
                        return "Error. Esta placa ya pertenece a otro cliente";
                    }
                }
                else
                {
                    return "¿Deseas crear este producto para " + cliente + "?";
                }

                
            }
            catch (Exception ex)
            {
                return "Error system";
            }
        }
        public override Cotizacion crearCotizaciones(Cotizacion cotizacionParam)
        {
            bool exito = true;
            string error = "";
            try
            {
                List<Cotizacion> listaCotizacion = new List<Cotizacion>();
                listaCotizacion = ListarCotizacion();
                //aqui se revisa si ya existe un ID igual al que se coloca para el nuevo Cotizacion
                string existente = "";

                if (listaCotizacion != null && listaCotizacion.Count > 0)
                {
                    existente = listaCotizacion.Where(x => x.Referencia.Equals(cotizacionParam.Referencia)).Select(y => y.Referencia).FirstOrDefault();
                    if (existente == null)
                        existente = "";
                }
                if (existente.Equals(""))
                {
                    List<Cotizacion> lista = new List<Cotizacion>();
                    lista.Add(cotizacionParam);

                    Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                    diccionario.Add("Referencia", SqlDbType.NVarChar);
                    diccionario.Add("Fecha", SqlDbType.DateTime);
                    diccionario.Add("Observaciones", SqlDbType.NVarChar);
                    diccionario.Add("IdProducto", SqlDbType.NVarChar);
                    diccionario.Add("Iva", SqlDbType.Decimal);
                    diccionario.Add("Total", SqlDbType.Decimal);
                    diccionario.Add("MontoExtra", SqlDbType.Decimal);
                    diccionario.Add("IdCliente", SqlDbType.NVarChar);
                    diccionario.Add("idUsuario", SqlDbType.NVarChar);
                    diccionario.Add("PiezasDanadas", SqlDbType.NVarChar);
                    diccionario.Add("Servicios", SqlDbType.NVarChar);
                    string resultado = generalController.AgregarModificar(lista, "SP_InsertarCotizacion", diccionario);

                    return cotizacionParam;
                }
                else
                {
                    exito = false;
                    return null;
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return null;
            }
            finally
            {
                //guarda todas las acciones del sistema en la base de datos. Tabla Acciones
                if (!exito)
                {
                    error = "Error. Intento fallido por existencia";
                }
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Insert";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
        }
        public List<Cotizacion> ListarCotizacion()
        {
            List<Cotizacion> lista = new List<Cotizacion>();
            try
            {
                lista = generalController.Listar("SP_ListarCotizacion", "Cotizacion");
            }
            catch (Exception ex)
            {
            }
            return lista;
        }

        public string ActualizarCotizacion(Cotizacion CotizacionParam)
        {
            string resultado = "";
            string error = "";
            try
            {
                List<Cotizacion> lista = new List<Cotizacion>();
                lista.Add(CotizacionParam);

                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("Referencia", SqlDbType.NVarChar);
                diccionario.Add("Fecha", SqlDbType.DateTime);
                diccionario.Add("Observaciones", SqlDbType.NVarChar);
                diccionario.Add("IdProducto", SqlDbType.NVarChar);
                diccionario.Add("Iva", SqlDbType.Decimal);
                diccionario.Add("Total", SqlDbType.Decimal);
                diccionario.Add("MontoExtra", SqlDbType.Decimal);
                diccionario.Add("IdCliente", SqlDbType.NVarChar);
                diccionario.Add("idUsuario", SqlDbType.NVarChar);
                diccionario.Add("PiezasDanadas", SqlDbType.NVarChar);
                diccionario.Add("Servicios", SqlDbType.NVarChar);
                resultado = generalController.AgregarModificar(lista, "SP_ActualizarCotizacion", diccionario);
            }
            catch (Exception ex)
            {
                resultado = "Error. Lo sentimos, no pudimos modificar esta cotización.";
                error = ex.Message;
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Update";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            return resultado;
        }
        public string EliminarCotizacion(string idCotizacion)
        {
            string error = "";
            try
            {
                Dictionary<string, SqlDbType> diccionario = new Dictionary<string, SqlDbType>();
                diccionario.Add("Referencia", SqlDbType.NVarChar);
                return generalController.Eliminar(idCotizacion, "SP_EliminarCotizacion", diccionario);
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return "Error. Lo sentimos, no hemos podido eliminar esta cotización";
            }
            finally
            {
                accionesObj.Pantalla = pantallaId;
                accionesObj.Accion = "Delete";
                accionesObj.Fecha = DateTime.Now;
                accionesObj.Detalle = error;
                acciones.SysAcciones(accionesObj);
            }
            
        }
    }
}
