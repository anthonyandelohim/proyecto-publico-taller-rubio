﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Body2.Modelo
{
    public class Parametros
    {
        #region Properties

        /// <summary>
        /// Name that SQL procedure expect from the parameter
        /// </summary>
        public string Name;

        /// <summary>
        /// Type of the parameter
        /// </summary>
        public SqlDbType SqlType;

        /// <summary>
        /// Value of the parameter
        /// </summary>
        public object Value;

        /// <summary>
        /// Specify the behavior of the parameter
        /// </summary>
        public bool IsInput;

        public enum CommandType
        {
            StoredProcedure = 1,
            Function = 2
        }

        #endregion

        #region Builder

        public Parametros()
        {
            IsInput = true;
        }

        #endregion
    }
}
