﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

//Agregar 
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Body2.Modelo
{
    public class Datos
    {
        #region Declaracion de Variables

        SqlConnection cnnConexion = null;
        SqlCommand cmdComando = null;
        SqlDataAdapter daAdaptador = null;
        DataTable Dtt = null;
        readonly string strCadenaConexion = string.Empty;

        #endregion

        #region Constructor

        public Datos()
        {
            //strCadenaConexion = @"Data Source= DESKTOP-3HI025C\SQLEXPRESS;  Initial Catalog = PCTR;  user id = sa; password = 123";
            strCadenaConexion = @"Data Source=DESKTOP-DUR7MB9;  Initial Catalog = PCTR; Integrated Security=SSPI";


        }

        #endregion

        #region Metodos a Ejecutar
        public delegate void Reader(SqlDataReader reader);
        public delegate void ExceptionHandler(Exception reader);

        //esta funcion ejecuta todos los store procedures de la base de datos. Es generica, funciona para todos
        //Select, delete, insert, update
        //Param 1 => nombre dels SP
        //Param 2 => valores a ingresar. del store procedure...es decir, lo que esta esperando recibir
        //..SP_InsertarRol(codigoRol, nombre_rol, descr, estado, pantallas)
        //Param 3 => guarda el error si hay uno..siempre cuando es igual a algo es opcional
        //Param 4 => los valores que van a ser guardados, o modificados, o eliminados  que viene desde el form
        public void EjecutarStoredProcedure(string procedureName, IEnumerable<Parametros> parameters, ExceptionHandler exeptionHandler = null, params Reader[] readers)
        {
            try
            {
                using (var connection = new SqlConnection(strCadenaConexion))
                using (var command = connection.CreateCommand())
                {
                    connection.Open();
                    command.CommandText = procedureName;
                    command.CommandType = CommandType.StoredProcedure;

                    if (parameters != null)
                    {
                        foreach (var parameter in parameters)
                        {
                            var sqlParameter = new SqlParameter(parameter.Name, parameter.SqlType)
                            {
                                Value = parameter.Value,
                                Direction = parameter.IsInput ? ParameterDirection.Input : ParameterDirection.Output
                            };
                            command.Parameters.Add(sqlParameter);
                        }
                    }

                    if (readers != null && !readers.Any())
                    {
                        command.ExecuteNonQuery();
                    }
                    else
                    {
                        using (var sqlReader = command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            //guarda la informacion
                            if (readers != null)
                            {
                                foreach (var reader in readers)
                                {
                                    while (sqlReader.Read())
                                    {
                                        reader(sqlReader);
                                    }

                                    sqlReader.NextResult();
                                }
                            }
                        }
                    }
                }
            }
            catch (NotImplementedException e)
            {
                if (exeptionHandler != null)
                {
                    exeptionHandler(e);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
        }
        public static T ReadField<T>(string name, SqlDataReader reader) where T : IConvertible
        {
            try
            {
                var value = reader[name];

                if (value == DBNull.Value)
                {
                    return default(T);
                }

                var type = value.GetType();
                var converter = TypeDescriptor.GetConverter(typeof(T));

                if (converter.CanConvertFrom(type))
                {
                    return (T)converter.ConvertFrom(value);
                }

                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch (Exception)
            {
                throw new NotImplementedException();
            }
        }

        public void EjecutarSP(SqlParameter[] parParametros, string parSPName)
        {
            // dtDatos = new DataSet();

            try
            {
                //Instanciamos el objeto conexion con la cadena de conexion.
                cnnConexion = new SqlConnection(strCadenaConexion);
                //Instanciamos el objeto comando con el TSQL y conexion a utilizar.
                cmdComando = new SqlCommand();//(parTSQL, cnnConexion);
                cmdComando.Connection = cnnConexion;
                //Abrimos la conexion
                cnnConexion.Open();
                //Asignamos el tipo de comando a ejecutar.
                cmdComando.CommandType = CommandType.StoredProcedure;
                //Agregamos el nombre del Srore procedure.
                cmdComando.CommandText = parSPName;
                //Agregmos los parametros a ejecutar
                cmdComando.Parameters.AddRange(parParametros);
                //Ejecutamos el TSQL(Transaction SQL) en el servidor.
                cmdComando.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            finally
            {
                cnnConexion.Dispose();
                cmdComando.Dispose();

            }

        }

        public DataTable RetornaTabla(SqlParameter[] parParametros, string parTSQL)
        {
            Dtt = null;
            try
            {
                Dtt = new DataTable();
                //Instanciamos el objeto conexion con la cadena de conexion.
                cnnConexion = new SqlConnection(strCadenaConexion);
                //Instanciamos el objeto comando con el TSQL
                cmdComando = new SqlCommand();//(parTSQL, cnnConexion);
                cmdComando.Connection = cnnConexion;
                //Asignamos el tipo de comando a ejecutar.
                cmdComando.CommandType = CommandType.StoredProcedure;
                //Agregamos el nombre del Srore procedure.
                cmdComando.CommandText = parTSQL;
                //Agregmos los parametros a ejecutar
                cmdComando.Parameters.AddRange(parParametros);
                //Instanciamos el objeto Adaptador con el comando a utilizar
                daAdaptador = new SqlDataAdapter(cmdComando);
                //Llenamos el Dataset con el adaptador de datos.
                daAdaptador.Fill(Dtt);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnnConexion.Dispose();
                cmdComando.Dispose();
                daAdaptador.Dispose();
            }
            return Dtt;
        }


        #endregion


    }
}